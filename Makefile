SHELL=/bin/bash
.SHELLFLAGS=-o pipefail -c

NAME=ska-mid-dish-ds-manager
VERSION=$(shell grep -e "^version = s*" pyproject.toml | cut -d = -f 2 | xargs)
IMAGE=$(CAR_OCI_REGISTRY_HOST)/$(NAME)
DOCKER_BUILD_CONTEXT=.
DOCKER_FILE_PATH=Dockerfile
MINIKUBE = true ## Minikube or not
TANGO_HOST = tango-databaseds:10000  ## TANGO_HOST connection to the Tango DS
CLUSTER_DOMAIN ?= cluster.local ## Domain used for naming Tango DS
SKA_TANGO_OPERATOR = true

-include .make/base.mk

########################################################################
# DOCS
########################################################################
-include .make/docs.mk


########################################################################
# PYTHON
########################################################################
PYTHON_LINE_LENGTH = 99
PYTHON_SWITCHES_FOR_BLACK = --line-length 99
PYTHON_SWITCHES_FOR_ISORT = -w 99
PYTHON_SWITCHES_FOR_FLAKE8 = --max-line-length=99
PYTHON_VARS_BEFORE_PYTEST ?= PYTHONPATH=.:./src \
							 TANGO_HOST=$(TANGO_HOST)
PYTHON_VARS_AFTER_PYTEST ?= -m '$(MARK)' --forked --json-report --json-report-file=build/report.json --junitxml=build/report.xml --ds-host="ds-opcua-server-simulator-001-svc.${KUBE_NAMESPACE}"

# Set the specific environment variables required for pytest
python-test: MARK = unit
k8s-test-runner: MARK = acceptance
k8s-test-runner: TANGO_HOST = tango-databaseds.$(KUBE_NAMESPACE).svc.$(CLUSTER_DOMAIN):10000

-include .make/python.mk

########################################################################
# OCI, HELM, K8S
########################################################################
OCI_TAG = $(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)

CI_REGISTRY ?= registry.gitlab.com

# Use the previously built image when running in the pipeline
ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set dishstructuremanager.image.image=$(NAME) \
	--set dishstructuremanager.image.registry=$(CI_REGISTRY)/ska-telescope/$(NAME) \
	--set dishstructuremanager.image.tag=$(OCI_TAG) \
	--set ska-mid-dish-simulators.dsOpcuaSimulator.enabled=true \
	--set ska-tango-base.enabled=true \
	--set global.dishes="{001,111}"
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/$(NAME)/$(NAME):$(OCI_TAG)
K8S_TIMEOUT=600s
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.operator=$(SKA_TANGO_OPERATOR) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	$(CUSTOM_VALUES)

HELM_CHARTS_TO_PUBLISH = ska-mid-dish-ds-manager

-include .make/oci.mk
-include .make/helm.mk
-include .make/k8s.mk

########################################################################
# PRIVATE OVERRIDES
########################################################################

-include PrivateRules.mak
