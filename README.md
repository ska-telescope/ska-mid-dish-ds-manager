# SKA MID Dish Structure Manager

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-mid-dish-ds-manager/badge/?version=latest)](https://ska-telescope-ska-mid-dish-ds-manager.readthedocs.io/en/latest/?badge=latest)

This device exposes a tango interface to control and monitor the Dish Structure Simulator
which implements an OPCUA server. Both constitute the Dish LMC devices with the DSManager
controlled by the master controller (DishManager). 

**Dish LMC context**

Dish Manager (Tango) -> Dish Structure Manager (Tango) -> Dish Struture (OPC-UA)

![DSManager in DishLMC](./docs/src/images/ds_manager_in_lmc.png)

## Installation

This project's requirement is managed with poetry and can be installed using a package manager or from source.

### From source

- Clone the repo

```bash
git clone git@gitlab.com:ska-telescope/ska-mid-dish-ds-manager.git
```

- Install poetry

```bash
pip install poetry
```

Install the dependencies and the package.

```bash
poetry install
```

## Testing

- Run the unit tests

```bash
make python-test
```

- Lint

```bash
make python-lint
```

## Development
### Deploy DS Manager with simulator

- Deploy DSManager with OPCUA server from ska-mid-dish-simulators

```bash
$ helm upgrade --install dev . -n ds-manager \
--set global.minikube=true \
--set global.operator=true \
--set ska-mid-dish-simulators.enabled=true \
--set ska-mid-dish-simulators.dsOpcuaSimulator.enabled=true
```

`ska-tango-base` is not deployed by default, to deploy it add the `--set` below:

```bash
--set ska-tango-base.enabled=true
```

### Deploy DS Manager without simulator

- Deploy DSManager with OPCUA server and specify address to running OPCUA server
- If you choose to manually set the DSC FQDNs note that the length of dishstructuremanager.dsc.fqdns must match global.dishes

```bash
$ helm upgrade --install dev . -n ds-manager \
--set global.minikube=true \
--set global.operator=true \
--set global.dishes="{001, 111}" \
--set ska-mid-dish-simulators.enabled=false \
--set dishstructuremanager.dsc.fqdns="{opc.tcp://127.0.0.1:1234/OPCUA/SimulationServer, opc.tcp://127.0.0.1:5678/OPCUA/SimulationServer}"
```
### Run DS Manager using file as DB 
- Run DS Manager on it own, using either nodb flag and/or a file as its DB. 
follow guide from [RTD](https://developer.skao.int/projects/ska-mid-dish-ds-manager/en/latest/deployment.html) 

## Configuration

### Attribute configuration
DSManager converts and exposes a subset the OPC-UA interface from the Dish Structure Controller into Tango. A property on the tango device, `DSCAttributes`, has been created to assist with any extra attributes to be exposed.

This property is list of strings structured in pairs as follows: `[nodePath0, description0, nodePath1, description1, ...]`. Where nodePath is the OPC-UA node path of the variable node relative from the PLC_PRG node.

The default configuration for this property can be found in the `DEFAULT_DSC_ATTRIBUTES` constant defined in `src/ska_mid_dish_ds_manager/models/dsc_attribute_config.py`.

Any attributes present in this list when the device command `ExposeDynamicDSCAttributes` is invoked will have their values exposed on a tango attribute along with change and archive events available.

Attribute names are generated from the node subpaths by concatenating "dsc" with the last two subpaths, removing any underscores, and converting the string to camel case. For example, if the dish structure controller state attribute `Management.Status.DscState` is added to the list, it will be exposed as `dscStatusDscState` on the Tango interface.

## Writing documentation

The documentation for this project can be found in the docs folder. For local builds,
plantuml is required to render the UML diagrams. Download `plantuml.jar` from the [plantuml website](https://plantuml.com/download), move it to `./docs/src/utils` and run `export LOCAL_BUILD=True`. Then 
run the command below and browse the docs from `docs/build/html/index.html`.

```bash
make docs-build html
```
