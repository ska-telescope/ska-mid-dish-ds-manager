================
 Design Overview
================

The DSManager design is based on the `BaseDevice`_ in ska-tango-base which uses a component manager
to monitor and control its component. The DSManager receives its control commands from the
DishManager which then issues a mapped command to the component it controls, i.e. DS Simulator.
The DS Simulator has an OPCUA interface which the component manager (in DS Manager) maintains a client
connection to. All updates from the component are monitored using callbacks and bubbled up to the
DishManager to inform the aggregate health/status of the dish.

Image below summarises design implementation:

.. image:: ../images/ds_manager_design.png
    :width: 100%
    :alt: DS Manager Design

.. _BaseDevice: https://gitlab.com/ska-telescope/ska-tango-base/-/blob/0.18.1/src/ska_tango_base/base/base_device.py?ref_type=tags
