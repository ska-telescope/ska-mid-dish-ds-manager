===============
Developer Guide
===============

.. toctree::
  :maxdepth: 1

   Design<ds_manager_design>
   Testing<testing>
