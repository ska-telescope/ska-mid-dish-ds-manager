=========================
SKA DS Manager Deployment
=========================

Overview
--------

Local Deployment
----------------
Ensure that the OPCUA server is running first to start the DS Manager device server 

.. code-block:: rst

    # Clone the ska mid-dish simulators repo
    `git clone https://gitlab.com/ska-telescope/ska-mid-dish-simulators.git` 
    # Run opcua server 
    $ poetry install && python3 /src/ska_mid_dish_simulators/devices/ds_opcua_server.py

DS Manager can be deployed and ran in isolation using the following commands:

.. code-block:: rst 

        # Pass in connection details for the OPCUA server by using DB file 
        $ python3 src/ska_mid_dish_ds_manager/DSManager.py DSManager mid-dish/ds-manager/ska001 -file=database/DSManagerDBFile.db -host localhost -port 45678

        # Connecting to the DS Manager using iTango
        $ itango3
        In [1]: dp = DeviceProxy("tango://localhost:45687/mid-dish/ds-manager/ska001#dbase=no")
           [2]: dp.ping() 

.. tip:: DS Manager can also be deployed with nodb flag:
    Guide to `deploying DS Manager with nodb`_

.. _deploying DS Manager with nodb: https://developer.skao.int/projects/ska-mid-dish-ds-manager/en/latest/developer_guide/testing.html