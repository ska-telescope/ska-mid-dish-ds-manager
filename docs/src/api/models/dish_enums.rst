==========
Dish Enums
==========

.. automodule:: ska_mid_dish_ds_manager.models.dish_enums
   :members:
