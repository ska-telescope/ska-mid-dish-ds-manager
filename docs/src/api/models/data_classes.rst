============
Data Classes
============

.. automodule:: ska_mid_dish_ds_manager.models.data_classes
   :members:
