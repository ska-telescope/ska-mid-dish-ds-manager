======================
SKA DS Manager Session
======================

Dish Structure Controller Session Management
--------------------------------------------
The Dish Structure Controller (DSC) has an arbitration mechanism to allow only one client to have authority and 
execute commands successfully. The `DscCmdAuthority` variable on the server indicates which client has authority to command the DSC. LMC has the lowest priority. 
The priority above LMC is the EGUI client, and above that is the Hand Held Panel (HHP) client. This means that either EGUI or HHP can take control of the DSC 
when LMC has authority. LMC can take authority from another LMC client or if there is no authority being held. The validity of the session
is determined by the session id. Each command sent to the DSC includes the session id for validation.

On Initialisation
-----------------
On initialisation, DSM (Dish Structure Manager) subscribes to the `DscCmdAuthority` variable, if authority is not held or 
another LMC has authority, DSM will attempt to take authority. This sequence is shown below:

.. uml:: session_id_init.uml

On Command Execution
--------------------

When executing commands a pre-check is done by inspecting `DscCmdAuthority` variable. If LMC has authority,
the command will be sent. If the session id is invalid while LMC has authority, a new session id is requested and the command is 
executed with the new session id. If no client has authority, DSM will attempt to take authority and 
proceed with the command execution. Commands will be rejected if a higher priority client has authority. 
This sequence is shown below:

.. uml:: session_id_cmd_exec.uml

.. _Session management on Dish Structure Controller: https://developer.skao.int/projects/ska-mid-dish-ds-manager/en/latest/session_managment.html