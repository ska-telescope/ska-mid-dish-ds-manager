============================================
SKA Mid Dish Structure Manager Documentation
============================================

Description
-----------

This device exposes an tango interface to control and monitor the Dish Structure Simulator
which implements an OPCUA server. Both constitute the Dish LMC devices with the DSManager
controlled by the master controller (DishManager). Image details where DSManager fits in 
the DishLMC context.

.. image:: images/ds_manager_in_lmc.png
  :alt: DSManager in DishLMC Context

.. toctree::
   :maxdepth: 1

   Developer Guide<developer_guide/index>
   Deployment<deployment>
   Session Management<user_guide/session_management>
   API<api/index>
   CHANGELOG<CHANGELOG>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
