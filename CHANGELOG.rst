##########
Change Log
##########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Unreleased
**********

Version 3.1.1
*************
- Cleaned up log message

Version 3.1.0
*************
- Added Band0PointingModelParams
- Bump ska-tango-util to v0.4.14
- Bump ska-tango-base to v0.4.14
- Bumped simulators version to 4.2.3
- Updated dockerfile to use new base images and improved docker image build
- Add SubscriptionManager and OPCUAControl (sculib) modules
- Swapped the OPCUAComponentManager with the SCUlib OPCUAComponentManager
- Removed redundant code (Due to the inclusion of sculib)

Version 3.0.1
*************
- Added `trackTableCurrentIndex` and `trackTableEndIndex` attributes that reflect track table indexes

Version 3.0.0
*************
- Bump ska-tango-util to v0.4.13
- Bump ska-tango-base to v0.4.13
- Bumped simulators version to 4.2.1

Version 2.2.0
*************
- Added in Read/Write dscPowerLimitKw attribute to be used when FP, LP and Slew are invoked.
- Bumped simulators version to 4.2.0

Version 2.1.2
*************
- Updated error message string to contain the error from OPCUA

Version 2.1.1
*************
- Bumped simulators chart version to 4.1.2

Version 2.1.0
*************
- Updated ska-mid-dish-simulators to v4.1.0 that uses servo loops for dish movement.
- Added SetPowerMode tango command.
- DSC states updated to align with PLC in ITF.
- Changed TrackLoadTable to a FastCommand.

Version 2.0.1
*************
- Added B5a and B5b pointing model param attributes
- Pointing update interval was reverted to 50ms due to reduced overhead from recent log changes.
- Application and System nodes cached - reduce time.
- Updated the ApplicationState helper function to default to nodeID since the ITF PLC has an issue with the BrowseName.
- Casted pointing model values in update_pointing_model_params.
- Makes us of TiltOnType enum value as specified in ICD.

Version 2.0.0
*************
- Changed OPCUA method call exception handling to handle all UA related exceptions
- Updated to align with dish structure controller ICD v0.0.10
- Migrated remaining OPCUA method calls to use updated argument signature which requires session id
- HealthState mapping triggered by CurrentMode and Application State nodes
- Removed achievedPointingAz and achievedPointingEl attributes.
- Added currentPointing attribute.
- Updated the logic to use a subset of currentPointing for the achievedPointing attribute.

Version 1.5.0
*************
- Moved dynamic attribute creation to Tango command
- Added connectionState attribute
- Moved act_staticOffset_value_Xel and act_staticOffset_value_El from dynamic attributes to static
- Reworked the STOW command to bypass the LRC queue
- Updated to align with dish structure controller ICD v0.0.7
- Implemented session ID management
- Resolved SKB-443

  - Changed DS Manager healthState to report OK when init is complete
  - Changed DS Manager State to report RUNNING when init is complete

Version 1.4.1
*************
- Added per command logging
- Updated to use SKA epoch for TAI timestamps

Version 1.4.0
*************
- Updated ska-tango-base to v1.0.0
- Updated PyTango to v9.5.0

Version 1.3.1
*************
- Update TAI conversion in source timestamp conversion and track start time

Version 1.3.0
*************
- Update docs to demonstrate running devices as nodb
- Added attributes `azimuthSpeed` and `elevationSpeed` used in slew command
- Removed explicity call to activate and deactivate axes on STANDBY_LP and STANDBY_FP
- Added TrackProgramMode attribute to be used for selecting TABLE or POLYNOMIAL tracking
- Refactored DS manager OPC-UA subscriptions callback
- Reduced OPC-UA subscriptions from over 20 to 3
- Grouped subscriptions into 3: Pointing, Static and Dynamic
- Updated ska-tango-util to 0.4.11
- Updated ska-tango-base to 0.4.10
- Updated ska-mid-dish-simulators to 2.0.2
- Added tango attributes `achievedTargetLock` and `configureTargetLock`


Version 1.2.7
*************
- Implemented device monitor - Solution to SKB-293
- Updated Simulators to 1.6.6

Version 1.2.6
*************
- Change ds-manager device name to conform to ADR-9
- Bumped up dish simulators version to 1.6.5
- Set DSC start_time parameter for Track command to 1 second in the future from when Track is called
- Expose achievedPointingAz and achievedPointingEl attributes
- Expose trackInterpolationMode attribute and add it to the TrackStart method call

Version 1.2.5
*************
- Expose DSC variables from OPC-UA server as dynamic attributes on DS Manager

Version 1.2.4
*************
- Rename tango attribute (DSSimFqdn to DSCFqdn)
- Expose helm attribute for list of dscFqdns for each dish id device
- Updated TrackStop command on DSManager
- Update simulator version 1.6.3
- Update tango-base version 0.4.8
- Update tango-util version 0.4.10

Version 1.2.3
*************
- Use client certificates for CETC54 OPC-UA namespace
- Enable and push archive events for attributes
- Fix device naming for dish IDs 100 and more
- Update to simulators version v1.6.2

Version 1.2.2
*************
- Use ska-ser-sphinx-theme for documentation
- Explicitly convert dish IDs to strings in template

Version 1.2.1
*************
- Resolve bug in device init method
- Remove write to OPC-UA nodes on desiredPointing write

Version 1.2.0
*************
- Update TrackLoadTable to accept DevFloat SPECTRUM

Version 1.1.2
*************
- Update to simulators version v1.4.3

Version 1.1.1
*************
- Update to simulators version v1.4.2 which includes band2PointingModelParams changes
- Updated ska-tango-base in charts to be disabled by default

Version 1.1.0
*************
- Exposing the desiredPointing
- Exposing the band2PointingModelParams from DS

Version 1.0.0
*************
- Exposing the pointing coordinates on DS Manager from DS

Version 0.0.2
*************
- Updated helm chart to make the OPC-UA server name configurable

Version 0.0.1
*************
- Added DSManager tango device
- Added OPC-UA Component Manager
- Implemented interface between DSManager and the component manager
- Implemented interface between the component manager and the OPC-UA server
- Use ska-mid-dish-simulators v1.3.0 which includes the OPC-UA server simulator
