# pylint: disable=unused-argument

"""This module has some helper methods to manage subscriptions."""
import logging
from dataclasses import dataclass, field
from functools import partial
from itertools import product
from queue import Queue
from typing import Callable, Optional, Tuple

from asyncua import ua
from ska_control_model import HealthState
from ska_mid_dish_steering_control.sculib import NodeDict

from ska_mid_dish_ds_manager.models.constants import (
    POINTING_PUBLISH_PERIOD_MS,
    STATIC_PUBLISH_PERIOD_MS,
)
from ska_mid_dish_ds_manager.models.dish_enums import (
    ApplicationStateType,
    BandType,
    CurrentModeType,
    DscCmdAuthType,
    DSCState,
    DSOperatingMode,
    DSPowerState,
    IndexerPosition,
    PointingState,
)
from ska_mid_dish_ds_manager.opcua_control import OPCUAControl
from ska_mid_dish_ds_manager.utils import SubscriptionEvent, get_tai_timestamp_from_datetime


@dataclass
class MonitoredNode:
    """Dataclass to store node subscription information."""

    node_path: str  # Dot seperated name, E.g Management.Commands.SetPowerMode
    component_state_name: str  # Key for the component state
    # Method to do a conversion E.g 1 -> DSCState.STANDBY
    convert_to_component_state_val: Optional[Callable] = None
    # Methods for side effects
    calculate_updates: Tuple = field(default_factory=tuple)


@dataclass
class MonitoredNodeGroup:
    """Dataclass to store groups of nodes and their subscription rates."""

    subscription_rate: int = STATIC_PUBLISH_PERIOD_MS
    nodes: dict[str, MonitoredNode] = field(default_factory=dict)


class SubscriptionManager:
    """Utility class for subscriptions."""

    def __init__(self, logger: logging.Logger) -> None:
        """Init SubscriptionManager.

        :param logger: logger
        :type logger: logging.Logger
        """
        self.logger = logger
        self.monitored_node_groups: list[MonitoredNodeGroup] = []
        self._dsc_static_pointing_model_params_order = (
            "IA",
            "CA",
            "NPAE",
            "AN",
            "AN0",
            "AW",
            "AW0",
            "ACEC",
            "ACES",
            "ABA",
            "ABphi",
            "IE",
            "ECEC",
            "ECES",
            "HECE4",
            "HESE4",
            "HECE8",
            "HESE8",
        )
        self._dsc_static_pointing_model_bands = (0, 1, 2, 3, 4, 5, 6, 7)

    @staticmethod
    def _compute_band_type_to_indexer_position(
        subs_event: SubscriptionEvent, component_state: dict
    ) -> dict:
        """Calculate indexerposition.

        :param subs_event: The event
        :type subs_event: SubscriptionEvent
        :return: The component state values to be updated.
        :rtype: dict
        """
        # Map the OPC-UA BandType enum to the IndexerPosition enum
        # <Field Name="Optical" Value="0"/>
        # <Field Name="Band_1" Value="1"/>
        # <Field Name="Band_2" Value="2"/>
        # <Field Name="Band_3" Value="3"/>
        # <Field Name="Band_4" Value="4"/>
        # <Field Name="Band_5a" Value="5"/>
        # <Field Name="Band_5b" Value="6"/>
        # <Field Name="Band_6" Value="7"/>
        # <Field Name="Unknown" Value="8"/>
        mapping = {
            ua.BandType.Optical: IndexerPosition.UNKNOWN,
            ua.BandType.Band_1: IndexerPosition.B1,
            ua.BandType.Band_2: IndexerPosition.B2,
            ua.BandType.Band_3: IndexerPosition.B3,
            ua.BandType.Band_4: IndexerPosition.B4,
            ua.BandType.Band_5a: IndexerPosition.B5,
            ua.BandType.Band_5b: IndexerPosition.UNKNOWN,
            ua.BandType.Band_6: IndexerPosition.UNKNOWN,
        }
        updates = {"indexerposition": mapping.get(subs_event.value, IndexerPosition.UNKNOWN)}
        return updates

    @staticmethod
    def _compute_health_state(_subs_event: SubscriptionEvent, component_state: dict) -> dict:
        """Calculate health_state.

        :param _subs_event: The event, not used here.
        :type _subs_event: SubscriptionEvent
        :param component_state: The current component state.
        :type component_state: dict
        :return: The component state values to be updated.
        :rtype: dict
        """
        new_health_state = HealthState.FAILED
        if (
            component_state["currentmode"] == CurrentModeType.SYSTEMMODE_BB
            and component_state["applicationstate"] == ApplicationStateType.Run
        ):
            new_health_state = HealthState.OK
        return {"healthstate": new_health_state}

    @staticmethod
    def _compute_pointing_updates(subs_event: SubscriptionEvent, component_state: dict) -> dict:
        """Calculate `currentpointing` and `achievedpointing` updates.

        :param subs_event: The event
        :type subs_event: SubscriptionEvent
        :param component_state: The current component state.
        :type component_state: dict
        :return: The component state values to be updated.
        :rtype: dict
        """
        updates = {}
        stored_current_pointing = component_state["currentpointing"]
        if (
            subs_event.value[1] != stored_current_pointing[1]
            or subs_event.value[2] != stored_current_pointing[2]
        ):
            updates["currentpointing"] = subs_event.value
            new_achieved_pointing = [subs_event.value[0], subs_event.value[1], subs_event.value[2]]
            updates["achievedpointing"] = new_achieved_pointing
        return updates

    @staticmethod
    def _compute_pointing_state(subs_event: SubscriptionEvent, component_state: dict) -> dict:
        """Calculate pointingstate.

        :param subs_event: The event, not used
        :type subs_event: SubscriptionEvent
        :param component_state: The current component state.
        :type component_state: dict
        :return: The component state values to be updated.
        :rtype: dict
        """
        updates = {}
        pointing_states = {
            DSCState.STANDSTILL: PointingState.READY,
            DSCState.STANDBY: PointingState.READY,
            DSCState.SLEW: PointingState.SLEW,
            DSCState.TRACK: PointingState.TRACK,
        }
        if component_state["dscstate"] in pointing_states:
            updates["pointingstate"] = pointing_states[component_state["dscstate"]]
        else:
            updates["pointingstate"] = PointingState.UNKNOWN
        return updates

    def _compute_operating_mode(
        self, subs_event: SubscriptionEvent, component_state: dict
    ) -> dict:
        """Compute the operating mode.

        :param subs_event: The event, not used
        :type subs_event: SubscriptionEvent
        :param component_state: The current component state.
        :type component_state: dict
        :return: Values to update the component state with.
        :rtype: dict
        """
        if component_state.get("hhpconnected", False):
            return {"operatingmode": DSOperatingMode.MAINTENANCE}

        dsc_state: DSCState = component_state.get("dscstate", DSCState.UNKNOWN)
        power_state: DSPowerState = component_state.get("powerstate", DSPowerState.UNKNOWN)

        if dsc_state == DSCState.STANDBY:
            if power_state == DSPowerState.LOW_POWER:
                return {"operatingmode": DSOperatingMode.STANDBY_LP}
            if power_state == DSPowerState.FULL_POWER:
                return {"operatingmode": DSOperatingMode.STANDBY_FP}
            self.logger.error(
                "With powerstate [%s] we cannot determine STANDBY_LP or STANDBY_FP",
                power_state,
            )
            return {"operatingmode": DSOperatingMode.UNKNOWN}

        mapping = {
            DSCState.STARTUP: DSOperatingMode.STARTUP,
            # DSCState.STANDBY: Special case above
            DSCState.LOCKED: DSOperatingMode.ESTOP,
            DSCState.STOWED: DSOperatingMode.STOW,
            DSCState.LOCKED_STOWED: DSOperatingMode.ESTOP,
            DSCState.ACTIVATING: DSOperatingMode.UNKNOWN,
            DSCState.DEACTIVATING: DSOperatingMode.UNKNOWN,
            DSCState.STANDSTILL: DSOperatingMode.POINT,
            DSCState.STOPPING: DSOperatingMode.POINT,
            DSCState.SLEW: DSOperatingMode.POINT,
            DSCState.JOG: DSOperatingMode.MAINTENANCE,
            DSCState.TRACK: DSOperatingMode.POINT,
        }

        return {"operatingmode": mapping.get(dsc_state, DSOperatingMode.UNKNOWN)}

    def _compute_configure_target_lock(
        self, subs_event: SubscriptionEvent, component_state: dict
    ) -> dict:
        """Compute the configureTargetLock attribute.

        :param subs_event: The event, not used
        :type subs_event: SubscriptionEvent
        :param component_state: The current component state.
        :type component_state: dict
        :return: Values to update the component state with.
        :rtype: dict
        """
        updated_configure_target_lock = [
            component_state.get("onsourcethreshold", 0.0),
            component_state.get("thresholdtimeperiod", 0.0),
        ]
        return {"configuretargetlock": updated_configure_target_lock}

    def _fi_axis_moving_changed(
        self, subs_event: SubscriptionEvent, component_state: dict
    ) -> dict:
        """Compute the IndexerPosition.

        :param subs_event: The event, not used
        :type subs_event: SubscriptionEvent
        :param component_state: The current component state.
        :type component_state: dict
        :return: Values to update the component state with.
        :rtype: dict
        """
        if subs_event.value:
            return {"indexerposition": IndexerPosition.MOVING}

        component_state_indexer_position = component_state.get(
            "indexerposition", IndexerPosition.UNKNOWN
        )
        if component_state_indexer_position == IndexerPosition.MOVING:
            return {"indexerposition": IndexerPosition.UNKNOWN}
        return {}

    def _compute_band_update(
        self,
        component_state_name: str,
        sub_coeff: str,
        subs_event: SubscriptionEvent,
        component_state: dict,
    ) -> dict:
        """
        Update the specified component state's coefficient value based on a subscription event.

        :param component_state_name: The key in `component_state` whose values need to be updated.
        :param sub_coeff: The coefficient to be updated, identified by its name.
        :param subs_event (SubscriptionEvent): An event containing the new # noqa: RST206
        value to update the coefficient with.
        :param component_state: A dictionary containing the current state of the components, where
        the key is the component name and the value is a list of coefficient values.

        :return: Updated component state
        """
        updated_values = list(component_state[component_state_name])

        if len(updated_values) < len(self._dsc_static_pointing_model_params_order):
            updated_values = [0.0] * len(self._dsc_static_pointing_model_params_order)

        for i, coeff in enumerate(self._dsc_static_pointing_model_params_order):
            if sub_coeff == coeff:
                updated_values[i] = subs_event.value
                break

        return {component_state_name: updated_values}

    # pylint: disable=too-many-locals
    def build_monitored_nodes(self, available_nodes: NodeDict) -> None:
        """Build up the information used for subscriptions.

        :param available_nodes: The list of available nodes from the sculib
        :type available_nodes: NodeDict

        Returns:
        {'Management.Status.DscState': MonitoredNode, ....}

        E.g

        node_names["Management.Status.DscState"] = MonitoredNode(
            The dictionary key.

        "Management.Status.DscState",
            The path of the node.

        "dscstate",
            The component state name.

        lambda sub_event: DSCState(sub_event.value),
            A method that gets an event value. Used to do simple conversions like int to Enum

        (self._compute_pointing_state, self._compute_operating_mode),
            Handle any side effects. E.g if the DSCState changes then the pointing state and
            operatingmode needs to change.)

        :return: Return the information needed for subscriptions.
        :rtype: list[dict[str, MonitoredNode], int]
        """
        expected_static_nodes = {
            "Management.Status.DscState": MonitoredNode(
                "Management.Status.DscState",
                "dscstate",
                lambda sub_event: DSCState(sub_event.value),
                (self._compute_pointing_state, self._compute_operating_mode),
            ),
            "Management.PowerStatus.LowPowerActive": MonitoredNode(
                "Management.PowerStatus.LowPowerActive",
                "powerstate",
                lambda sub_event: (
                    DSPowerState.LOW_POWER if sub_event.value else DSPowerState.FULL_POWER
                ),
                (self._compute_operating_mode,),
            ),
            "FeedIndexer.Status.AxisMoving": MonitoredNode(
                "FeedIndexer.Status.AxisMoving",
                "indexerposition",
                None,
                (self._fi_axis_moving_changed,),
            ),
            "System.CurrentMode": MonitoredNode(
                "System.CurrentMode",
                "currentmode",
                lambda sub_event: CurrentModeType(sub_event.value),
                (self._compute_health_state,),
            ),
            "Logic.Application.ApplicationState": MonitoredNode(
                "Logic.Application.ApplicationState",
                "applicationstate",
                lambda sub_event: ApplicationStateType(sub_event.value),
                (self._compute_health_state,),
            ),
            "CommandArbiter.Status.DscCmdAuthority": MonitoredNode(
                "CommandArbiter.Status.DscCmdAuthority",
                "dsccmdauth",
                lambda sub_event: DscCmdAuthType(sub_event.value),
                (),
            ),
            "Management.Status.OnSource": MonitoredNode(
                "Management.Status.OnSource",
                "achievedtargetlock",
                lambda sub_event: sub_event.value,
                (self._compute_health_state,),
            ),
            "Management.Status.FiPos": MonitoredNode(
                "Management.Status.FiPos",
                "indexerposition",
                None,
                (self._compute_band_type_to_indexer_position,),
            ),
            "Management.Status.HHP_Cncted": MonitoredNode(
                "Management.Status.HHP_Cncted",
                "hhpconnected",
                lambda sub_event: sub_event.value,
                (self._compute_operating_mode,),
            ),
            "Tracking.Status.act_statOffset_value_El": MonitoredNode(
                "Tracking.Status.act_statOffset_value_El",
                "actstaticoffsetvalueel",
                lambda sub_event: sub_event.value,
                (self._compute_operating_mode,),
            ),
            "Tracking.Status.act_statOffset_value_Xel": MonitoredNode(
                "Tracking.Status.act_statOffset_value_Xel",
                "actstaticoffsetvaluexel",
                lambda sub_event: sub_event.value,
                (self._compute_operating_mode,),
            ),
            "Tracking.Status.OnSourceThreshold": MonitoredNode(
                "Tracking.Status.OnSourceThreshold",
                "onsourcethreshold",
                lambda sub_event: sub_event.value,
                (self._compute_configure_target_lock,),
            ),
            "Tracking.Status.ThresholdTimePeriod": MonitoredNode(
                "Tracking.Status.ThresholdTimePeriod",
                "thresholdtimeperiod",
                lambda sub_event: sub_event.value,
                (self._compute_configure_target_lock,),
            ),
        }

        expected_pointing_nodes = {
            "Pointing.Status.CurrentPointing": MonitoredNode(
                "Pointing.Status.CurrentPointing",
                "currentpointing",
                None,
                (self._compute_pointing_updates,),
            ),
            "Tracking.Status.p_desired_Az": MonitoredNode(
                "Tracking.Status.p_desired_Az",
                "desiredpointingaz",
                lambda sub_event: (
                    get_tai_timestamp_from_datetime(sub_event.source_timestamp),
                    sub_event.value,
                ),
                (),
            ),
            "Tracking.Status.p_desired_El": MonitoredNode(
                "Tracking.Status.p_desired_El",
                "desiredpointingel",
                lambda sub_event: (
                    get_tai_timestamp_from_datetime(sub_event.source_timestamp),
                    sub_event.value,
                ),
                (),
            ),
            "Tracking.TableStatus.act_pt_act_index": MonitoredNode(
                "Tracking.TableStatus.act_pt_act_index",
                "actptactindex",
                lambda sub_event: sub_event.value,
                (),
            ),
            "Tracking.TableStatus.act_pt_end_index": MonitoredNode(
                "Tracking.TableStatus.act_pt_end_index",
                "actptendindex",
                lambda sub_event: sub_event.value,
                (),
            ),
        }

        # Add expected static pointing model parameters to expected_static_nodes
        spm_param_index_style = None

        if "Pointing.StaticPmParameters.StaticPmParameters[0].IA" in available_nodes:
            spm_param_index_style = "[%s]"
        elif "Pointing.StaticPmParameters.StaticPmParameters_0.IA" in available_nodes:
            spm_param_index_style = "_%s"
        else:
            self.logger.warning("Could not find StaticPmParameters in available nodes.")

        if spm_param_index_style:
            # Add all the band pointing model parameter nodes
            for bandtype_index, coeff in product(
                self._dsc_static_pointing_model_bands, self._dsc_static_pointing_model_params_order
            ):
                band_param = BandType(bandtype_index)

                if band_param == BandType.OPTICAL:
                    band_param_name = "0"
                else:
                    band_param_name = band_param.name.lower()[1:]

                band_comp_state_name = f"band{band_param_name}pointingmodelparams"

                spm_param_index = spm_param_index_style % bandtype_index

                path = f"Pointing.StaticPmParameters.StaticPmParameters{spm_param_index}.{coeff}"

                expected_static_nodes[path] = MonitoredNode(
                    path,
                    band_comp_state_name,
                    None,
                    (
                        partial(
                            self._compute_band_update,
                            band_comp_state_name,
                            coeff,
                        ),
                    ),
                )

        monitored_node_groups: list[MonitoredNodeGroup] = [
            MonitoredNodeGroup(STATIC_PUBLISH_PERIOD_MS, expected_static_nodes),
            MonitoredNodeGroup(POINTING_PUBLISH_PERIOD_MS, expected_pointing_nodes),
        ]

        # Ensure that the expected nodes are in the list of available nodes
        for group_index, mon_node_group in enumerate(monitored_node_groups):
            found_nodes_in_group = {}
            for mon_node_key, mon_node in mon_node_group.nodes.items():
                if mon_node.node_path in available_nodes:
                    found_nodes_in_group[mon_node_key] = mon_node
                else:
                    self.logger.warning(
                        "Expected node %s not found in list of available nodes.",
                        mon_node.node_path,
                    )
            monitored_node_groups[group_index].nodes = found_nodes_in_group

        self.monitored_node_groups = monitored_node_groups

    def get_monitored_node(self, node_path: str) -> MonitoredNode | None:
        """Get a monitored node based on its node path.

        :param node_path: The node path of the desired node
        :type node_path: str
        :return: The found MonitoredNode | None.
        :rtype: MonitoredNode | None
        """
        for mon_node_group in self.monitored_node_groups:
            if node_path in mon_node_group.nodes:
                return mon_node_group.nodes[node_path]
        return None

    def setup_subscriptions(
        self, scu: OPCUAControl, events_queue: Queue, lost_connection_cb: Callable | None = None
    ) -> None:
        """Get a monitored node based on its node path.

        :param scu: The SCULib
        :type scu: OPCUAControl
        :param events_queue: The queue for datachange events
        :type events_queue: Queue
        :param lost_connection_cb: The optional callback for lost connection
        :type lost_connection_cb: Callable | None
        """
        self.build_monitored_nodes(scu.nodes)

        added_lost_connection_cb = False
        for mon_node_group in self.monitored_node_groups:
            node_paths = [mon_node.node_path for mon_node in mon_node_group.nodes.values()]
            if len(node_paths) > 0:
                if lost_connection_cb and not added_lost_connection_cb:
                    scu.subscribe(
                        node_paths,
                        mon_node_group.subscription_rate,
                        events_queue,
                        lost_connection_cb,
                    )
                    added_lost_connection_cb = True
                else:
                    scu.subscribe(node_paths, mon_node_group.subscription_rate, events_queue)
