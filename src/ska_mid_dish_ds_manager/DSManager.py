"""Tango Device for Dish Structure with monitoring and control to OPCUA server."""

# pylint: disable=invalid-name,unused-argument,too-many-public-methods
# pylint: disable=attribute-defined-outside-init
# pylint: disable=protected-access
# pylint: disable=too-many-lines

import logging
import weakref
from functools import reduce
from typing import Any, List, Optional, Tuple
from urllib.parse import urlparse

from ska_control_model import CommunicationStatus, HealthState, ResultCode, TaskStatus
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import FastCommand, SubmittedSlowCommand
from tango import AttrWriteType, DevState
from tango.server import attribute, command, device_property, run

from ska_mid_dish_ds_manager.models.command_class import ImmediateSlowCommand
from ska_mid_dish_ds_manager.models.constants import (
    BAND_POINTING_MODEL_PARAMS_LENGTH,
    DEFAULT_AZIMUTH_SPEED_DPS,
    DEFAULT_ELEVATION_SPEED_DPS,
    DSC_MAX_POWER_LIMIT_KW,
    DSC_MIN_POWER_LIMIT_KW,
    MAX_AZIMUTH_SPEED_DPS,
    MAX_ELEVATION_SPEED_DPS,
)
from ska_mid_dish_ds_manager.models.dish_enums import (
    BandType,
    DSCState,
    DSOperatingMode,
    DSPowerState,
    IndexerPosition,
    PointingState,
    TrackInterpolationMode,
    TrackProgramMode,
)
from ska_mid_dish_ds_manager.models.dsc_attribute_config import DEFAULT_DSC_ATTRIBUTES
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager
from ska_mid_dish_ds_manager.utils import BaseInfoIt, generate_tango_name

DevVarLongStringArrayType = Tuple[List[ResultCode], List[Optional[str]]]


def change_case(attr_name: str) -> str:
    """Convert camel case string to snake case.

    The snake case output is prefixed by an underscore to
    match the naming convention of the attribute variables.

    Example:
    dishMode > _dish_mode
    capture > _capture
    b3CapabilityState > _b3_capability_state

    Source: https://www.geeksforgeeks.org/
    python-program-to-convert-camel-case-string-to-snake-case/
    """
    # pylint: disable=line-too-long
    return f"_{reduce(lambda x, y: x + ('_' if y.isupper() else '') + y, attr_name).lower()}"  # noqa: E501


class DSManager(SKABaseDevice):  # type: ignore
    """The Dish Structure Manager."""

    # Access instances for debugging and testing
    instances = weakref.WeakValueDictionary()  # type: ignore

    # -----------------
    # Device Properties
    # -----------------
    opcua_server_addr = "opc.tcp://ds-opcua-server-simulator-001-svc:4840/dish-structure/server/"
    DSCFqdn = device_property(dtype=str, default_value=opcua_server_addr)
    opcua_namespace = "http://skao.int/DS_ICD/"
    DSOPCUANamespace = device_property(dtype=str, default_value=opcua_namespace)
    DSCAttributes = device_property(dtype=(str,), default_value=DEFAULT_DSC_ATTRIBUTES)

    class InitCommand(SKABaseDevice.InitCommand):
        """Initializes the attributes of the DSManager."""

        def do(
            self: SKABaseDevice.InitCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Initialise DSManager.

            :param args: positional arguments
            :param kwargs: keyword arguments

            :return: A tuple containing a return code and a string
            """
            self._device._connection_state = CommunicationStatus.NOT_ESTABLISHED

            self._device._component_state_attr_map = {
                "powerstate": "powerState",
                "pointingstate": "pointingState",
                "operatingmode": "operatingMode",
                "indexerposition": "indexerPosition",
                "achievedpointing": "achievedPointing",
                "desiredpointingaz": "desiredPointingAz",
                "desiredpointingel": "desiredPointingEl",
                "currentpointing": "currentPointing",
                "healthstate": "healthState",
                "buildstate": "buildState",
                "band0pointingmodelparams": "band0PointingModelParams",
                "band1pointingmodelparams": "band1PointingModelParams",
                "band2pointingmodelparams": "band2PointingModelParams",
                "band3pointingmodelparams": "band3PointingModelParams",
                "band4pointingmodelparams": "band4PointingModelParams",
                "band5apointingmodelparams": "band5aPointingModelParams",
                "band5bpointingmodelparams": "band5bPointingModelParams",
                "dscstate": "dscState",
                "trackinterpolationmode": "trackInterpolationMode",
                "azimuthspeed": "azimuthSpeed",
                "elevationspeed": "elevationSpeed",
                "configuretargetlock": "configureTargetLock",
                "achievedtargetlock": "achievedTargetLock",
                "actstaticoffsetvaluexel": "actStaticOffsetValueXel",
                "actstaticoffsetvalueel": "actStaticOffsetValueEl",
                "trackprogrammode": "trackProgramMode",
                "dscpowerlimitkw": "dscPowerLimitKw",
                "actptactindex": "trackTableCurrentIndex",
                "actptendindex": "trackTableEndIndex",
            }
            for attr in self._device._component_state_attr_map.values():
                self._device.set_change_event(attr, True, False)
                self._device.set_archive_event(attr, True, False)

            self._device.set_change_event("connectionState", True, False)
            self._device.set_archive_event("connectionState", True, False)
            self._device.push_change_event("healthState", HealthState.UNKNOWN)
            self._device.push_archive_event("healthState", HealthState.UNKNOWN)
            self._device.set_state(DevState.RUNNING)
            self._device.component_manager.start_communicating()

            # Save the device instance
            # This can be used during unit testing to access the running device instance and its
            # component manager to manually change values and trigger callbacks.
            self._device.instances[self._device.get_name()] = self._device
            (result_code, message) = super().do()  # type: ignore
            return (ResultCode(result_code), message)

    def create_component_manager(
        self: "DSManager",
    ) -> OPCUAComponentManager:
        """
        Create a component manager.

        :return: the component manager
        """
        parsed_fqdn = urlparse(self.DSCFqdn)
        return OPCUAComponentManager(
            ds_host=parsed_fqdn.hostname,
            ds_port=parsed_fqdn.port,
            ds_endpoint=parsed_fqdn.path,
            ds_opcua_namespace=self.DSOPCUANamespace,
            logger=self.logger,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed,
        )

    def init_command_objects(self) -> None:
        """Initialise the command handlers."""
        super().init_command_objects()

        for command_name, method_name in [
            ("SetStandbyLPMode", "set_standby_lp_mode"),
            ("SetStandbyFPMode", "set_standby_fp_mode"),
            ("SetPointMode", "set_point_mode"),
            ("SetMaintenanceMode", "set_maintenance_mode"),
            ("SetIndexPosition", "set_index_position"),
            ("Slew", "slew"),
            ("Track", "track"),
            ("TrackStop", "track_stop"),
            ("TrackLoadStaticOff", "track_load_static_off"),
            ("SetPowerMode", "set_power_mode"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    callback=None,
                    logger=self.logger,
                ),
            )

        self.register_command_object(
            "Stow",
            ImmediateSlowCommand(
                "Stow",
                self._command_tracker,
                self.component_manager,
                "stow",
                callback=None,
                logger=self.logger,
            ),
        )

        self.register_command_object(
            "TrackLoadTable",
            self.TrackLoadTableCommand(self.component_manager, self.logger),
        )

    def _communication_state_changed(self, communication_state: CommunicationStatus) -> None:
        """Push and archive events on communication state change."""
        self.push_change_event("connectionState", communication_state)
        self.push_archive_event("connectionState", communication_state)

    def _component_state_changed(self, *args: Any, **kwargs: Any) -> None:
        if not hasattr(self, "_component_state_attr_map"):
            self.logger.warning("Init not completed, but state is being updated [%s]", kwargs)
            return

        for comp_state_name, comp_state_value in kwargs.items():
            attribute_name = self._component_state_attr_map.get(comp_state_name, comp_state_name)
            attr_variable = change_case(attribute_name)
            setattr(self, attr_variable, comp_state_value)
            self.push_change_event(attribute_name, comp_state_value)
            self.push_archive_event(attribute_name, comp_state_value)

    def _create_tango_dynamic_attributes(self) -> Tuple[ResultCode, str]:
        """Create Tango attributes and set their read method."""
        self.logger.debug("Setting up attributes dynamically")
        for node_name, _ in self.component_manager.scu.attributes.items():
            tango_name = generate_tango_name(node_name)
            attr_name = f"dsc{change_case(tango_name)}"
            tango_attr = attribute(
                name=attr_name,
                dtype=str,
                doc=f"Read the node {node_name} on the dish structure controller",
                fget=self.component_manager.read_node_value,
                fset=None,
            )
            # map attributes this way {tango_attr : opcua_node_path }
            self.component_manager.tango_opcua_attr_mapper[attr_name] = node_name
            self.add_attribute(tango_attr)
        return ResultCode.OK, "dynamic attributes setup completed"

    # -----------
    # Attributes
    # -----------

    @attribute(dtype=str)
    def buildState(self) -> str:
        """Get DS manager and DSC version structure."""
        return self.component_manager.component_state.get("buildstate")

    @attribute(
        dtype=CommunicationStatus,
        access=AttrWriteType.READ,
        doc="Displays connection status to DSC device",
    )
    def connectionState(self) -> CommunicationStatus:
        """Indicate connection state of device."""
        return self.component_manager.communication_state

    @attribute(
        dtype=DSPowerState,
        access=AttrWriteType.READ,
    )
    def powerState(self: "DSManager") -> DSPowerState:
        """Indicate power state of device."""
        return self.component_manager.component_state.get("powerstate", DSPowerState.UNKNOWN)

    @attribute(
        dtype=DSOperatingMode,
        access=AttrWriteType.READ,
    )
    def operatingMode(self: "DSManager") -> DSOperatingMode:
        """Indicate operating mode of device."""
        return self.component_manager.component_state.get("operatingmode", DSOperatingMode.UNKNOWN)

    @attribute(
        dtype=IndexerPosition,
        access=AttrWriteType.READ,
    )
    def indexerPosition(self: "DSManager") -> IndexerPosition:
        """Indicate operating mode of device."""
        return self.component_manager.component_state.get(
            "indexerposition", IndexerPosition.UNKNOWN
        )

    @attribute(
        dtype=PointingState,
        access=AttrWriteType.READ,
    )
    def pointingState(self: "DSManager") -> PointingState:
        """Indicate operating mode of device."""
        return self.component_manager.component_state.get("pointingstate", None)

    @attribute(
        dtype=bool,
        access=AttrWriteType.READ,
    )
    def achievedTargetLock(self) -> bool:
        """Indicate Actual “On Sky” rms tracking error of device."""
        return self.component_manager.component_state.get("achievedtargetlock", False)

    @attribute(
        dtype=bool,
        doc="Indicates whether the hand held pendant is connected",
        access=AttrWriteType.READ,
    )
    def hhpConnected(self) -> bool:
        """Indicate whether the hand held pendant is connected."""
        return self.component_manager.component_state.get("hhpconnected", False)

    @attribute(
        max_dim_x=3,
        dtype=(float,),
        doc="[0] Timestamp\n[1] Azimuth\n[2] Elevation",
        access=AttrWriteType.READ,
    )
    def achievedPointing(self: "DSManager") -> list[float]:
        """Indicate achieved pointing of dish axis."""
        return self.component_manager.component_state.get("achievedpointing", [0.0, 0.0, 0.0])

    @attribute(
        max_dim_x=7,
        dtype=(float,),
        doc=(
            "List consisting of pointing information:"
            "\n[\n[0] TAIoffset,\n[1] Azimuth.p_Act,\n[2] Elevation.p_Act,"
            "\n[3] Azimuth.p_Enc,\n[4] Elevation.p_Enc,"
            "\n[5] TiltCorrVal_Az,\n[6] TiltCorrVal_El\n]"
        ),
        access=AttrWriteType.READ,
    )
    def currentPointing(self: "DSManager") -> list[float]:
        """Display pointing feedback."""
        return self.component_manager.component_state.get(
            "currentpointing", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        )

    @attribute(
        max_dim_x=2,
        dtype=(float,),
        doc="[0] Timestamp\n[1] Azimuth",
        access=AttrWriteType.READ,
    )
    def desiredPointingAz(self: "DSManager") -> list[float]:
        """Indicate desired pointing of device azimuth axis."""
        return self.component_manager.component_state.get("desiredpointingaz", [0.0, 0.0, 0.0])

    @attribute(
        max_dim_x=2,
        dtype=(float,),
        doc="[0] Timestamp\n[1] Elevation",
        access=AttrWriteType.READ,
    )
    def desiredPointingEl(self: "DSManager") -> list[float]:
        """Indicate desired pointing of device elevation axis."""
        return self.component_manager.component_state.get("desiredpointingel", [0.0, 0.0, 0.0])

    @attribute(
        dtype=float,
        doc="Actual cross-elevation static offset (arcsec)",
        access=AttrWriteType.READ,
    )
    def actStaticOffsetValueXel(self: "DSManager") -> float:
        """Indicate actual cross-elevation static offset in arcsec."""
        return self.component_manager.component_state.get("actstaticoffsetvaluexel", 0.0)

    @attribute(
        dtype=float,
        doc="Actual elevation static offset (arcsec)",
        access=AttrWriteType.READ,
    )
    def actStaticOffsetValueEl(self: "DSManager") -> float:
        """Indicate actual elevation static offset in arcsec."""
        return self.component_manager.component_state.get("actstaticoffsetvalueel", 0.0)

    @attribute(
        max_dim_x=1,
        dtype=HealthState,
        doc="Returns the current Health State",
        access=AttrWriteType.READ,
    )
    def healthState(self: "DSManager") -> Any:
        """Indicate the Health State."""
        return self.component_manager.component_state.get("healthstate", HealthState.UNKNOWN)

    @attribute(
        dtype=int,
        doc="Actual used index in the track table",
        access=AttrWriteType.READ,
    )
    def trackTableCurrentIndex(self) -> int:
        """Index of current point being tracked in the track table."""
        return self.component_manager.component_state.get("actptactindex", 0)

    @attribute(
        dtype=int,
        doc="End index in the track table",
        access=AttrWriteType.READ,
    )
    def trackTableEndIndex(self) -> int:
        """Index of last point in the track table."""
        return self.component_manager.component_state.get("actptendindex", 0)

    @attribute(
        dtype=(float,),
        max_dim_x=BAND_POINTING_MODEL_PARAMS_LENGTH,
        doc="""
            Parameters for (local) Band 0 pointing models used by Dish to do pointing corrections.

            When writing to this attribute, the selected band for correction will be set to B1.

            Band pointing model parameters are:
            [0] IA, [1] CA, [2] NPAE, [3] AN, [4] AN0, [5] AW, [6] AW0, [7] ACEC, [8] ACES,
            [9] ABA, [10] ABphi, [11] IE, [12] ECEC, [13] ECES, [14] HECE4,
            [15] HESE4, [16] HECE8, [17] HESE8
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def band0PointingModelParams(self: "DSManager"):
        """Indicate band 0 static pointing model parameters of device."""
        return self.component_manager.component_state.get("band0pointingmodelparams", [])

    @band0PointingModelParams.write  # type: ignore
    def band0PointingModelParams(self: "DSManager", value):
        """Update band 0 static pointing model parameters."""
        self.logger.debug("Updating band0PointingModelParams %s", value)

        task_status, message = self.component_manager.update_pointing_model_params(
            BandType.OPTICAL,
            value,
        )

        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=(float,),
        max_dim_x=BAND_POINTING_MODEL_PARAMS_LENGTH,
        doc="""
            Parameters for (local) Band 1 pointing models used by Dish to do pointing corrections.

            When writing to this attribute, the selected band for correction will be set to B1.

            Band pointing model parameters are:
            [0] IA, [1] CA, [2] NPAE, [3] AN, [4] AN0, [5] AW, [6] AW0, [7] ACEC, [8] ACES,
            [9] ABA, [10] ABphi, [11] IE, [12] ECEC, [13] ECES, [14] HECE4,
            [15] HESE4, [16] HECE8, [17] HESE8
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def band1PointingModelParams(self: "DSManager"):
        """Indicate band 1 static pointing model parameters of device."""
        return self.component_manager.component_state.get("band1pointingmodelparams", [])

    @band1PointingModelParams.write  # type: ignore
    def band1PointingModelParams(self: "DSManager", value):
        """Update band 1 static pointing model parameters."""
        self.logger.debug("Updating band1PointingModelParams %s", value)

        task_status, message = self.component_manager.update_pointing_model_params(
            BandType.B1,
            value,
        )

        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=(float,),
        max_dim_x=BAND_POINTING_MODEL_PARAMS_LENGTH,
        doc="""
            Parameters for (local) Band 2 pointing models used by Dish to do pointing corrections.

            When writing to this attribute, the selected band for correction will be set to B2.

            Band pointing model parameters are:
            [0] IA, [1] CA, [2] NPAE, [3] AN, [4] AN0, [5] AW, [6] AW0, [7] ACEC, [8] ACES,
            [9] ABA, [10] ABphi, [11] IE, [12] ECEC, [13] ECES, [14] HECE4,
            [15] HESE4, [16] HECE8, [17] HESE8
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def band2PointingModelParams(self: "DSManager"):
        """Indicate band 2 static pointing model parameters of device."""
        return self.component_manager.component_state.get("band2pointingmodelparams", [])

    @band2PointingModelParams.write  # type: ignore
    def band2PointingModelParams(self: "DSManager", value):
        """Update band 2 static pointing model parameters."""
        self.logger.debug("Updating band2PointingModelParams %s", value)

        task_status, message = self.component_manager.update_pointing_model_params(
            BandType.B2,
            value,
        )

        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=(float,),
        max_dim_x=BAND_POINTING_MODEL_PARAMS_LENGTH,
        doc="""
            Parameters for (local) Band 3 pointing models used by Dish to do pointing corrections.

            When writing to this attribute, the selected band for correction will be set to B3.

            Band pointing model parameters are:
            [0] IA, [1] CA, [2] NPAE, [3] AN, [4] AN0, [5] AW, [6] AW0, [7] ACEC, [8] ACES,
            [9] ABA, [10] ABphi, [11] IE, [12] ECEC, [13] ECES, [14] HECE4,
            [15] HESE4, [16] HECE8, [17] HESE8
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def band3PointingModelParams(self: "DSManager"):
        """Indicate band 3 static pointing model parameters of device."""
        return self.component_manager.component_state.get("band3pointingmodelparams", [])

    @band3PointingModelParams.write  # type: ignore
    def band3PointingModelParams(self: "DSManager", value):
        """Update band 3 static pointing model parameters."""
        self.logger.debug("Updating band3PointingModelParams %s", value)

        task_status, message = self.component_manager.update_pointing_model_params(
            BandType.B3,
            value,
        )

        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=(float,),
        max_dim_x=BAND_POINTING_MODEL_PARAMS_LENGTH,
        doc="""
            Parameters for (local) Band 4 pointing models used by Dish to do pointing corrections.

            When writing to this attribute, the selected band for correction will be set to B4.

            Band pointing model parameters are:
            [0] IA, [1] CA, [2] NPAE, [3] AN, [4] AN0, [5] AW, [6] AW0, [7] ACEC, [8] ACES,
            [9] ABA, [10] ABphi, [11] IE, [12] ECEC, [13] ECES, [14] HECE4,
            [15] HESE4, [16] HECE8, [17] HESE8
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def band4PointingModelParams(self: "DSManager"):
        """Indicate band 4 static pointing model parameters of device."""
        return self.component_manager.component_state.get("band4pointingmodelparams", [])

    @band4PointingModelParams.write  # type: ignore
    def band4PointingModelParams(self: "DSManager", value):
        """Update band 4 static pointing model parameters."""
        self.logger.debug("Updating band4PointingModelParams %s", value)

        task_status, message = self.component_manager.update_pointing_model_params(
            BandType.B4,
            value,
        )

        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=(float,),
        max_dim_x=BAND_POINTING_MODEL_PARAMS_LENGTH,
        doc="""
            Parameters for (local) Band 5a pointing models used by Dish to do pointing corrections.

            When writing to this attribute, the selected band for correction will be set to B5a.

            Band pointing model parameters are:
            [0] IA, [1] CA, [2] NPAE, [3] AN, [4] AN0, [5] AW, [6] AW0, [7] ACEC, [8] ACES,
            [9] ABA, [10] ABphi, [11] IE, [12] ECEC, [13] ECES, [14] HECE4,
            [15] HESE4, [16] HECE8, [17] HESE8
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def band5aPointingModelParams(self: "DSManager"):
        """Indicate band 5a static pointing model parameters of device."""
        return self.component_manager.component_state.get("band5apointingmodelparams", [])

    @band5aPointingModelParams.write  # type: ignore
    def band5aPointingModelParams(self: "DSManager", value):
        """Update band 5a static pointing model parameters."""
        self.logger.debug("Updating band5aPointingModelParams %s", value)

        task_status, message = self.component_manager.update_pointing_model_params(
            BandType.B5a,
            value,
        )

        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=(float,),
        max_dim_x=BAND_POINTING_MODEL_PARAMS_LENGTH,
        doc="""
            Parameters for (local) Band 5b pointing models used by Dish to do pointing corrections.

            When writing to this attribute, the selected band for correction will be set to B5b.

            Band pointing model parameters are:
            [0] IA, [1] CA, [2] NPAE, [3] AN, [4] AN0, [5] AW, [6] AW0, [7] ACEC, [8] ACES,
            [9] ABA, [10] ABphi, [11] IE, [12] ECEC, [13] ECES, [14] HECE4,
            [15] HESE4, [16] HECE8, [17] HESE8
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def band5bPointingModelParams(self: "DSManager"):
        """Indicate band 5b static pointing model parameters of device."""
        return self.component_manager.component_state.get("band5bpointingmodelparams", [])

    @band5bPointingModelParams.write  # type: ignore
    def band5bPointingModelParams(self: "DSManager", value):
        """Update band 5b static pointing model parameters."""
        self.logger.debug("Updating band5bPointingModelParams %s", value)

        task_status, message = self.component_manager.update_pointing_model_params(
            BandType.B5b,
            value,
        )

        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=DSCState,
        doc="Dish Structure Controller State",
    )
    def dscState(self: "DSManager"):  # type: ignore
        """Indicate dish structure controller state."""
        return self.component_manager.component_state.get("dscstate", None)

    @attribute(
        dtype=TrackInterpolationMode,
        access=AttrWriteType.READ_WRITE,
        doc="Selects the type of interpolation to be used in program tracking.",
    )
    def trackInterpolationMode(self):
        """Indicate track interpolation mode of device."""
        return self.component_manager.component_state.get(
            "trackinterpolationmode", TrackInterpolationMode.SPLINE
        )

    @trackInterpolationMode.write  # type: ignore
    def trackInterpolationMode(self, value):
        """Set the trackInterpolationMode."""
        self.component_manager._update_component_state(trackinterpolationmode=value)
        self.push_change_event("trackInterpolationMode", value)
        self.push_archive_event("trackInterpolationMode", value)

    @attribute(
        dtype=float,
        access=AttrWriteType.READ_WRITE,
        doc="Sets the Azimuth rotation speed during slew in degrees per second.",
    )
    def azimuthSpeed(self):
        """Indicate azimuth speed for slew moves."""
        return self.component_manager.component_state.get(
            "azimuthspeed", DEFAULT_AZIMUTH_SPEED_DPS
        )

    @azimuthSpeed.write  # type: ignore
    def azimuthSpeed(self, value):
        """Set the azimuth speed."""
        if value <= 0 or value > MAX_AZIMUTH_SPEED_DPS:
            raise ValueError(
                f"Invalid value for Azimuth speed, valid range is (0, {MAX_AZIMUTH_SPEED_DPS}]."
            )
        self.component_manager._update_component_state(azimuthspeed=value)

    @attribute(
        dtype=float,
        access=AttrWriteType.READ_WRITE,
        doc="Sets the Elevation rotation speed during slew in degrees per second.",
    )
    def elevationSpeed(self):
        """Indicate elevation speed for slew moves."""
        return self.component_manager.component_state.get(
            "elevationspeed", DEFAULT_ELEVATION_SPEED_DPS
        )

    @elevationSpeed.write  # type: ignore
    def elevationSpeed(self, value):
        """Set the elevation speed."""
        if value <= 0 or value > MAX_ELEVATION_SPEED_DPS:
            raise ValueError(
                "Invalid value for Elevation speed,"
                f" valid range is (0, {MAX_ELEVATION_SPEED_DPS}]."
            )
        self.component_manager._update_component_state(elevationspeed=value)

    @attribute(
        dtype=TrackProgramMode,
        access=AttrWriteType.READ_WRITE,
        doc="Selects the type of tracking mode to be used.",
    )
    def trackProgramMode(self: "DSManager") -> TrackProgramMode:
        """Indicate track mode selected."""
        return self.component_manager.component_state.get(
            "trackprogrammode", TrackProgramMode.TABLE
        )

    @trackProgramMode.write  # type: ignore
    def trackProgramMode(self: "DSManager", value: TrackProgramMode):
        """Set the trackProgramMode."""
        self.component_manager._update_component_state(trackprogrammode=value)

    @attribute(
        max_dim_x=2,
        dtype=(float,),
        access=AttrWriteType.READ_WRITE,
        doc="Threshold level the rms on-sky vector error between the desired"
        "Azimuth & Elevation and actual  Azimuth & Elevation.",
    )
    def configureTargetLock(self: "DSManager") -> list[float]:
        """Indicate the configuretargetlock."""
        return self.component_manager.component_state.get("configuretargetlock", [0.0, 0.0])

    @configureTargetLock.write  # type: ignore
    def configureTargetLock(self, value: list[float]) -> None:
        """Set the configureTargetLock."""
        value = list(value)  # numpy array to list
        self.logger.debug("onSourceThreshold write method called with threshold %s", value)
        task_status, message = self.component_manager.update_threshold_levels(value)
        if task_status != TaskStatus.COMPLETED:
            raise ValueError(message)

    @attribute(
        dtype=float,
        doc="""
            DSC Power Limit (kW). Note that this attribute can also be set by calling
            SetPowerMode. This value does not reflect the power limit in reality because
            the current PowerLimit(kW) is not reported as it cannot be read from the DSC.
        """,
        access=AttrWriteType.READ_WRITE,
    )
    def dscPowerLimitKw(self: "DSManager"):
        """Represent the DSC Power Limit (kW)."""
        return self.component_manager.component_state.get(
            "dscpowerlimitkw", DSC_MIN_POWER_LIMIT_KW
        )

    @dscPowerLimitKw.write  # type: ignore
    def dscPowerLimitKw(self: "DSManager", value):
        """Set the DSC Power Limit (kW)."""
        self.logger.debug("Updating DSC Power Limit Mode  %skW", value)
        if DSC_MIN_POWER_LIMIT_KW <= value <= DSC_MAX_POWER_LIMIT_KW:
            self.component_manager._update_component_state(dscpowerlimitkw=value)
            self.push_change_event("dscpowerlimitkw", value)
            self.push_archive_event("dscpowerlimitkw", value)
        else:
            raise ValueError(
                f"Invalid value, {value} for DSC Power Limit (kW),"
                f" valid range is [{DSC_MIN_POWER_LIMIT_KW}, {DSC_MAX_POWER_LIMIT_KW}]."
            )

    # -----------
    # Commands
    # -----------

    @command(  # type: ignore[misc]
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def SetStandbyLPMode(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Triggers the Dish to transition to the STANDBY-LP mode.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("SetStandbyLPMode")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def SetStandbyFPMode(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Triggers the Dish to transition to the STANDBY-FP mode.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("SetStandbyFPMode")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def SetPointMode(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Triggers the Dish to transition to the POINT mode.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("SetPointMode")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def Stow(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Transition the dish to STOW immediately bypassing the LRC queue.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("Stow")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def SetMaintenanceMode(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Triggers the Dish to transition to the MAINTENANCE mode.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("SetMaintenanceMode")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_in=int,
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def SetIndexPosition(self: "DSManager", band: int) -> DevVarLongStringArrayType:
        """
        Triggers the Dish to transition the feed indexer to the `argin` position.

        :param band: The band which the dish is instructed to move the feed indexer to

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("SetIndexPosition")
        result_code, unique_id = handler(band)
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_in=(float,),
        dtype_out="DevVarLongStringArray",
        doc_in="[0]: Azimuth\n[1]: Elevation",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def Slew(self: "DSManager", values: list[float]) -> DevVarLongStringArrayType:
        """
        Trigger the Dish to start moving to the commanded (Az,El) position.

        :param values: float values for the az position, el position

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("Slew")
        result_code, unique_id = handler(
            values,  # type: ignore[has-type]
            self.component_manager.component_state.get("azimuthspeed", DEFAULT_AZIMUTH_SPEED_DPS),
            self.component_manager.component_state.get(
                "elevationspeed", DEFAULT_ELEVATION_SPEED_DPS
            ),
        )
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def Track(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Triggers the Dish to start tracking.

        Dish will start tracking the positions present in the track table using the set
        interpolation mode.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("Track")
        inter_mode = self.component_manager.component_state.get(
            "trackinterpolationmode", TrackInterpolationMode.SPLINE
        )
        result_code, unique_id = handler(inter_mode)  # type: ignore[has-type]
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_out="DevVarLongStringArray",
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def TrackStop(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Triggers the dish to stop tracking but will not apply brakes.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("TrackStop")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    @command(  # type: ignore[misc]
        dtype_in=(float,),
        dtype_out="DevVarLongStringArray",
        doc_in="""
            Load (global) static tracking offsets.

            The offset is loaded immediately and is not cancelled
            between tracks. The static offset introduces a positional adjustment to facilitate
            reference pointing and the five-point calibration. The static offsets are added the
            output of the interpolator before the correction of the static pointing model.

            Note: If the static pointing correction is switched off, the static offsets remain as
            an offset to the Azimuth and Elevation positions and need to be set to zero manually.

            Static offset parameters are:
            [0] Off_Xel, [1] Off_El
        """,
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def TrackLoadStaticOff(self: "DSManager", values: list[float]) -> DevVarLongStringArrayType:
        """
        Load the given static pointing model offsets.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("TrackLoadStaticOff")
        result_code, unique_id = handler(values)
        return ([result_code], [unique_id])

    class TrackLoadTableCommand(FastCommand):
        """Class for handling the TrackLoadTable command."""

        def __init__(
            self,
            component_manager: OPCUAComponentManager,
            logger: Optional[logging.Logger] = None,
        ) -> None:
            """
            Initialise a new TrackLoadTableCommand instance.

            :param component_manager: the device to which this command belongs.
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement TrackLoadTable command functionality.

            :param args: table.
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            return self._component_manager.command_manager.track_load_table(*args)

    @command(  # type: ignore[misc]
        dtype_in=(float,),  # needs to be double for the timestamps
        dtype_out="DevVarLongStringArray",
        doc_in="""
            [0]: LoadMode\n
            [1]: SequenceLength,\n
            [2 - end]: TrackTable, length should be a multiple of 3 values:\n
            (timestamp, azimuth coordinate, elevation coordinate)
        """,
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def TrackLoadTable(self: "DSManager", values: list[float]) -> DevVarLongStringArrayType:
        """
        Load the track table with the given load mode.

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("TrackLoadTable")
        result_code, unique_id = handler(values)
        return ([result_code], [unique_id])

    @command(
        dtype_out="DevVarLongStringArray",
        doc_in="""
            Setup Tango attributes to reflect attributes from the Dish Structure Controller.

            These attributes will have archive and change events enabled. The list of attributes to
            setup are defined in the DSManager property DSCAttributes.
        """,
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def ExposeDynamicDSCAttributes(self: "DSManager") -> DevVarLongStringArrayType:
        """
        Initialize all the dynamic attributes.

        Dynamic attributes are specified in the DSCAttributes tango property. This property is
        a list of strings in the form:
        - [nodepath0, description0, dtype0, ..., nodepathN, descriptionN, dtypeN]
        Where nodepath is the OPC-UA node path relative from the PLC_PRG node.

        Attribute names are generated from the node subpaths by concatenating "dsc" with the last
        two subpaths, removing any underscores, and converting the string to camel case.
        """
        if not self.component_manager.scu:
            raise RuntimeError("Connection to the DSC has not completed.")
        result_code, message = self._create_tango_dynamic_attributes()
        return ([result_code], [message])

    @command(  # type: ignore[misc]
        dtype_in=(float,),
        dtype_out="DevVarLongStringArray",
        doc_in="""
            Sets mode to Low or Full Power.

            The current low or full power active is reported by the powerState attribute.
            The current PowerLimit (kW) is not reported as it cannot be read from the DSC.
            LowPowerOn represented by a boolean where True(1.0) sets LowPowerOn to
            True (STANDBY_LP mode), and False(0.0) sets LowPowerOn to False (STANDBY_FP mode).
            [0] LowPowerOn (Boolean): True (1.0), False (0.0)\n
            [1] Power limit(kW): Maximum power consumption allowed.
            """,
    )
    @BaseInfoIt(show_args=True, show_kwargs=True, show_ret=True)
    def SetPowerMode(self: "DSManager", values: list[float]) -> DevVarLongStringArrayType:
        """
        Trigger the Dish to transition to SetPowerMode.

        :param values: float values for LowPowerOn(boolean), Power limit(kW)

        :return: A tuple containing a return code and a string
            message indicating status.
        """
        handler = self.get_command_object("SetPowerMode")
        result_code, unique_id = handler(values)
        return ([result_code], [unique_id])


def main(args: Any = None, **kwargs: Any) -> None:
    """Launch a DSManager device."""
    return run((DSManager,), args=args, **kwargs)


if __name__ == "__main__":
    main()
