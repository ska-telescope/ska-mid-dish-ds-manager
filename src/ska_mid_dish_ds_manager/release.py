# pylint: disable=invalid-name
"""Release information for Dish structure manager package."""

from importlib.metadata import PackageNotFoundError, version

name = "DS Manager"
DS_MANAGER_PACKAGE_NAME = "ska_mid_dish_ds_manager"


def get_ds_manager_release_version() -> str:
    """Get release version of package."""
    try:
        release_version = version(DS_MANAGER_PACKAGE_NAME)
    except PackageNotFoundError:
        release_version = f"ERR: parsing {DS_MANAGER_PACKAGE_NAME} version."
    return release_version
