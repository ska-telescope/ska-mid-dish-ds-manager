"""
Copied from enums defined in ska-mid-dish-manager.

See ska_mid_dish_manager/models/dish_enums.py.
"""

import enum


class DSOperatingMode(enum.IntEnum):
    """DS operating mode enums."""

    UNKNOWN = 0
    STARTUP = 1
    STANDBY_LP = 2
    STANDBY_FP = 3
    MAINTENANCE = 4
    STOW = 5
    ESTOP = 6
    POINT = 7


class PointingState(enum.IntEnum):
    """Pointing state enums."""

    READY = 0
    SLEW = 1
    TRACK = 2
    SCAN = 3
    UNKNOWN = 4


class IndexerPosition(enum.IntEnum):
    """Indexer position enums."""

    UNKNOWN = 0
    B1 = 1
    B2 = 2
    B3 = 3
    B4 = 4
    B5 = 5
    MOVING = 6
    ERROR = 7


class BandType(enum.IntEnum):
    """Band type enums."""

    OPTICAL = 0
    B1 = 1
    B2 = 2
    B3 = 3
    B4 = 4
    # pylint: disable=invalid-name
    B5a = 5
    B5b = 6
    B6 = 7
    UNKNOWN = 8


class DSPowerState(enum.IntEnum):
    """Power state enums."""

    OFF = 0
    UPS = 1
    FULL_POWER = 2
    LOW_POWER = 3
    UNKNOWN = 4


class TrackTableLoadMode(enum.IntEnum):
    """Track table load mode enums."""

    NEW = 0
    APPEND = 1
    RESET = 2


class DSCState(enum.IntEnum):
    """Dish structure controller state enums."""

    STARTUP = 0
    STANDBY = 1
    LOCKED = 2
    STOWED = 3
    LOCKED_STOWED = 4
    ACTIVATING = 5
    DEACTIVATING = 6
    STANDSTILL = 7
    STOPPING = 8
    SLEW = 9
    JOG = 10
    TRACK = 11
    UNKNOWN = 12


class TrackInterpolationMode(enum.IntEnum):
    """Track interpolation mode enums."""

    NEWTON = 0
    SPLINE = 1


class TrackProgramMode(enum.IntEnum):
    """Track program mode enums."""

    TABLE = 0
    POLY = 1


class DscCmdAuthType(enum.IntEnum):
    """Dish structure command authority enums."""

    NO_AUTHORITY = 0
    LMC = 1
    HHP = 2
    EGUI = 3


class CurrentModeType(enum.IntEnum):
    """Dish structure current mode enums."""

    SYSTEMMODE_P0 = 0
    SYSTEMMODE_P1 = 1
    SYSTEMMODE_P2 = 2
    SYSTEMMODE_P3 = 3
    SYSTEMMODE_BB = 4


# pylint: disable=invalid-name
class ApplicationStateType(enum.Enum):
    """Dish structure current mode enums."""

    Unknown = 0
    Stop = 1
    Run = 2
    Download = 3
    OnlineChange = 4
    Debug = 3
    Exchange = 4
