"""Module containing DM and DSC buildState data classes."""

import dataclasses


@dataclasses.dataclass
class DscBuildStateDataclass:
    """Dataclass to format DSC buildState data."""

    device: str
    dish_id: str
    dsc_serial_number: str
    dsc_software_version: str
    dsc_icd_version: str


@dataclasses.dataclass
class DsmBuildStateDataclass:
    """Dataclass to format DS Manager buildState data."""

    device: str
    ds_manager_version: str
    dsc_address: str
    dsc_build_state: DscBuildStateDataclass
