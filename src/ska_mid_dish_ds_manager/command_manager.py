# pylint: disable=no-member,too-many-arguments,unused-argument
# pylint: disable=too-many-locals, too-many-lines, too-many-branches
# mypy: disable-error-code="union-attr"
# mypy: disable-error-code="no-redef"
# mypy: disable-error-code="index"
# flake8: noqa:C901
"""Module that contains CommandManager where commands to DSC are managed."""
import enum
import logging
import sys
from typing import Any, Callable, Optional, Tuple, Union

from asyncua import ua
from asyncua.ua.uaerrors import UaError
from ska_control_model import ResultCode, TaskStatus
from ska_mid_dish_steering_control import CmdReturn
from ska_mid_dish_steering_control import ResultCode as SCUResultCode
from ska_tango_base.executor import TaskExecutorComponentManager

from ska_mid_dish_ds_manager.models.constants import (
    AZ_AXIS_STATE_OPCUA_PATH,
    BAND_POINTING_MODEL_PARAMS_LENGTH,
    DSC_MAX_POWER_LIMIT_KW,
    DSC_MIN_POWER_LIMIT_KW,
    DSC_STATE_OPCUA_PATH,
    EL_AXIS_STATE_OPCUA_PATH,
)
from ska_mid_dish_ds_manager.models.dish_enums import (
    BandType,
    DscCmdAuthType,
    DSPowerState,
    IndexerPosition,
    TrackInterpolationMode,
    TrackTableLoadMode,
)
from ska_mid_dish_ds_manager.opcua_control import OPCUAControl
from ska_mid_dish_ds_manager.utils import (
    MAX_AZIMUTH,
    MAX_ELEVATION,
    MAX_ELEVATION_SCIENCE,
    MIN_AZIMUTH,
    MIN_ELEVATION,
    MIN_ELEVATION_SCIENCE,
    are_points_within_dish_limits,
    get_current_tai_timestamp,
    is_point_within_dish_limits,
)


class CommandNotAuthorized(UaError):
    """Exception for incorrect command authorization."""


class CommandManager:
    """Wrap all the commands to DSC."""

    def __init__(
        self,
        logger: logging.Logger,
        scu: OPCUAControl | None,
        submit_task: Callable,
    ) -> None:
        """Wrap all the commands to DSC.

        :param logger: logger
        :type logger: logging.Logger
        :param scu: OPCUAControl instance, None if it is not established yet.
        :type scu: OPCUAControl
        :param submit_task: Submit a task to the task executor
        :type submit_task: Callable
        """
        self.logger = logger
        self.scu = scu
        self.submit_task = submit_task

    def set_standby_lp_mode(
        self,
        component_state: dict,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to STANDBY_LP mode.

        :param component_state: the current component state
        :type component_state: dict
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self.submit_task(
            self._set_standby_lp_mode,
            args=[
                component_state,
            ],
            task_callback=task_callback,
        )
        return status, response

    def _set_standby_lp_mode(
        self,
        component_state: dict,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,
    ) -> None:
        """Transition the dish to STANDBY_LP mode.

        :param component_state: the current component state
        :type component_state: dict
        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called SetStandbyLPMode")

        if not self.scu:
            raise RuntimeError("Communication with the DSC not established")

        current_power_limit = component_state.get("dscpowerlimitkw", DSC_MIN_POWER_LIMIT_KW)
        if task_callback:
            task_callback(
                status=TaskStatus.IN_PROGRESS,
                progress=f"Low Power Mode called using DSC Power Limit: {current_power_limit}kW",
            )

        try:
            dsc_state = self.scu.attributes[DSC_STATE_OPCUA_PATH].value  # type: ignore[union-attr]
            if dsc_state in [ua.DscStateType.Stowed.value, ua.DscStateType.Locked_Stowed.value]:
                # The dish needs to be in full power mode in order to call Stow(False)
                self._call_opc_ua_method(
                    task_callback,
                    "Management.Commands.SetPowerMode",
                    "SetStandbyLPMode",
                    False,
                    current_power_limit,
                )

                self._call_opc_ua_method(
                    task_callback, "Management.Commands.Stow", "SetStandbyLPMode", False
                )

            az_axis_state = self.scu.attributes[AZ_AXIS_STATE_OPCUA_PATH].value
            el_axis_state = self.scu.attributes[EL_AXIS_STATE_OPCUA_PATH].value

            if (
                az_axis_state != ua.AxisStateType.Standby
                or el_axis_state != ua.AxisStateType.Standby
            ):
                self._call_opc_ua_method(
                    task_callback,
                    "Management.Commands.DeActivate",
                    "SetStandbyLPMode",
                    ua.UInt16(ua.AxisSelectType.AzEl),
                )

            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.SetPowerMode",
                "SetStandbyLPMode",
                True,
                current_power_limit,
            )

        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="SetStandbyLPMode Called",
            )

    def set_standby_fp_mode(
        self,
        component_state: dict,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to STANDBY_FP mode.

        :param component_state: the current component state
        :type component_state: dict
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self.submit_task(
            self._set_standby_fp_mode,
            args=[
                component_state,
            ],
            task_callback=task_callback,
        )
        return status, response

    def _set_standby_fp_mode(
        self,
        component_state: dict,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> None:
        """Transition the dish to STANDBY_FP mode.

        :param component_state: the current component state
        :type component_state: dict
        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called SetStandbyFPMode")

        if not self.scu:
            raise RuntimeError("Communication with the DSC not established")

        current_power_limit = component_state.get("dscpowerlimitkw", DSC_MIN_POWER_LIMIT_KW)
        if task_callback:
            task_callback(
                status=TaskStatus.IN_PROGRESS,
                progress=f"Full Power Mode called using DSC Power Limit: {current_power_limit}kW",
            )

        try:
            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.SetPowerMode",
                "SetStandbyFPMode",
                False,
                current_power_limit,
            )

            dsc_state = self.scu.attributes[DSC_STATE_OPCUA_PATH].value  # type: ignore[union-attr]
            if dsc_state in [ua.DscStateType.Stowed.value, ua.DscStateType.Locked_Stowed.value]:
                self._call_opc_ua_method(
                    task_callback, "Management.Commands.Stow", "SetStandbyFPMode", False
                )

            az_axis_state = self.scu.attributes[AZ_AXIS_STATE_OPCUA_PATH].value
            el_axis_state = self.scu.attributes[EL_AXIS_STATE_OPCUA_PATH].value

            if (
                az_axis_state != ua.AxisStateType.Standby
                or el_axis_state != ua.AxisStateType.Standby
            ):
                self._call_opc_ua_method(
                    task_callback,
                    "Management.Commands.DeActivate",
                    "SetStandbyFPMode",
                    ua.UInt16(ua.AxisSelectType.AzEl),
                )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="SetStandbyFPMode Called",
            )

    def set_power_mode(
        self,
        component_state_callback: Callable,
        values: list[float],
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to SetPowerMode.

        :param values: (0.0 or 1.0) and limit(kW)] with 1.0 representing True
        :type values: list(float)
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        if len(values) != 2:
            return (
                TaskStatus.REJECTED,
                f"Expected 2 arguments [(0.0 or 1.0) and limit(kW)] with 1.0 representing True "
                f"and 0.0 representing False. Got {len(values)} arguments.",
            )

        # Check if the boolean number is 0.0 or 1.0
        boolean_value = values[0]
        if boolean_value not in [0.0, 1.0]:
            return (
                TaskStatus.REJECTED,
                f"Invalid boolean number: {boolean_value}. Expected 0.0 or 1.0.",
            )

        boolean_arg = bool(values[0])
        limit = values[1]
        if limit > DSC_MAX_POWER_LIMIT_KW or limit < DSC_MIN_POWER_LIMIT_KW:
            return (
                TaskStatus.REJECTED,
                f"Invalid dsc power limit: {limit}. Expected a value in this range"
                f" [{DSC_MIN_POWER_LIMIT_KW}, {DSC_MAX_POWER_LIMIT_KW}].",
            )
        status, response = self.submit_task(
            self._set_power_mode,
            args=[component_state_callback, boolean_arg, limit],
            task_callback=task_callback,
        )
        return status, response

    def _set_power_mode(
        self,
        component_state_callback: Callable,
        boolean_arg: bool,
        limit: float,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,
    ) -> None:
        """Transition the dish to SetPowerMode.

        :param boolean_arg: Low power active or not
        :type boolean_arg: bool
        :param limit: The power limit in Kw
        :type limit: float
        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called SetPowerMode with args [%s, %s]", boolean_arg, limit)

        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        try:
            component_state_callback(dscpowerlimitkw=limit)
            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.SetPowerMode",
                "SetPowerMode",
                boolean_arg,
                limit,
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="SetPowerMode Called",
            )

    def set_point_mode(
        self,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to POINT mode.

        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self.submit_task(
            self._set_point_mode, args=[], task_callback=task_callback
        )
        return status, response

    def _set_point_mode(
        self,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> None:
        """Transition the dish to POINT mode.

        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called SetPointMode")
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        try:
            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.Activate",
                "SetPointMode",
                ua.UInt16(ua.AxisSelectType.AzEl),
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="SetPointMode Called",
            )

    def stow(
        self,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to STOW immediately bypassing the LRC queue.

        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self._stow(task_abort_event=None, task_callback=task_callback)
        return status, response

    def _stow(
        self,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to STOW immediately bypassing the LRC queue.

        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        self.logger.info("Called Stow")

        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)
        try:
            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.Stow",
                "Stow",
                True,
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return (
                TaskStatus.FAILED,
                "Stow command not successful on the Dish Structure Controller",
            )
        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="Stow Called",
            )
        return (
            TaskStatus.COMPLETED,
            "The Stow command has been called on the Dish Structure Controller",
        )

    def set_maintenance_mode(
        self,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to MAINTENANCE mode.

        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self.submit_task(
            self._set_maintenance_mode, args=[], task_callback=task_callback
        )
        return status, response

    def _set_maintenance_mode(
        self,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> None:
        """Transition the dish to MAINTENANCE mode.

        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="SetMaintenanceMode Called",
            )

    def set_index_position(
        self,
        band: IndexerPosition,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Set the indexer position on the dish.

        :param band: The indexer position to move to.
        :type band: IndexerPosition
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self.submit_task(
            self._set_index_position, args=[band], task_callback=task_callback
        )
        return status, response

    def _set_index_position(
        self,
        band: IndexerPosition,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> None:
        """Set the indexer position on the dish.

        :param band: The band to select.
        :type band: IndexerPosition
        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called SetIndexPosition")

        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        ua_band = ua.UInt16(self.indexer_position_to_band_type(band))

        if ua_band is None:
            if task_callback:
                task_callback(
                    status=TaskStatus.REJECTED,
                    result=f"SetIndexPosition Rejected. Invalid band given ({str(band)})",
                )
            return

        try:
            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.Move2Band",
                "SetIndexPosition",
                ua_band,
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="SetIndexPosition Called",
            )

    def slew(
        self,
        component_state: dict,
        values: list[float],
        speed_az: float,
        speed_el: float,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to SLEW.

        :param values: The slew coordinates
        :type values: list[float]
        :param speed_az: Az axis speed
        :type speed_az: float
        :param speed_el: El Axis speed
        :type speed_el: float
        :param component_state: the current component state
        :type component_state: dict
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        if len(values) != 2:
            return (
                TaskStatus.REJECTED,
                f"Expected array of length 2 ([pos_az, pos_el])"
                f" but got array of length {len(values)}.",
            )

        pos_az = values[0]
        pos_el = values[1]
        if not is_point_within_dish_limits(pos_az, pos_el, science_limits=False):
            return (
                TaskStatus.REJECTED,
                f"Target point (Az: {pos_az}, El: {pos_el}) is not within the dishes limits"
                f" (Az: [{MIN_AZIMUTH}, {MAX_AZIMUTH}], El: [{MIN_ELEVATION}, {MAX_ELEVATION}]))",
            )

        status, response = self.submit_task(
            self._slew,
            args=[pos_az, pos_el, speed_az, speed_el, component_state],
            task_callback=task_callback,
        )
        return status, response

    def _slew(
        self,
        pos_az: float,
        pos_el: float,
        speed_az: float,
        speed_el: float,
        component_state: dict,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> None:
        """Transition the dish to SLEW.

        :param pos_az: Az coordinate
        :type pos_az: float
        :param pos_el: El coordinate
        :type pos_el: float
        :param speed_az: Az speed
        :type speed_az: float
        :param speed_el: El speed
        :type speed_el: float
        :param component_state: the current component state
        :type component_state: dict
        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info(
            "Called Slew with args [%s, %s, %s, %s]", pos_az, pos_el, speed_az, speed_el
        )

        current_power_limit = component_state.get("dscpowerlimitkw", DSC_MIN_POWER_LIMIT_KW)
        if task_callback:
            task_callback(
                status=TaskStatus.IN_PROGRESS,
                progress=f"Slew called with Azimuth speed: {speed_az} deg/s, "
                f"Elevation speed: {speed_el} deg/s and DSC Power Limit: {current_power_limit}kW",
            )

        try:
            if component_state.get("powerstate", DSPowerState.LOW_POWER) == DSPowerState.LOW_POWER:
                self._call_opc_ua_method(
                    task_callback,
                    "Management.Commands.SetPowerMode",
                    "Slew",
                    False,
                    current_power_limit,
                )
            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.Slew2AbsAzEl",
                "Slew",
                float(pos_az),
                float(pos_el),
                float(speed_az),
                float(speed_el),
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="Slew Called",
            )

    def track(
        self,
        interpolation_mode: TrackInterpolationMode,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Transition the dish to TRACK.

        :param interpolation_mode: The interpolation mode
        :type interpolation_mode: TrackInterpolationMode
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self.submit_task(
            self._track, args=[interpolation_mode], task_callback=task_callback
        )
        return status, response

    def _track(
        self,
        interpolation_mode: TrackInterpolationMode,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> None:
        """Transition the dish to TRACK.

        :param interpolation_mode: The DSC interpolation mode
        :type interpolation_mode: TrackInterpolationMode
        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called track")

        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if interpolation_mode == TrackInterpolationMode.NEWTON:
            interpol = ua.InterpolType.Newton
        else:
            interpol = ua.InterpolType.Spline

        start_time_tai = get_current_tai_timestamp() + 1

        try:
            self._call_opc_ua_method(
                task_callback,
                "Tracking.Commands.TrackStart",
                "Track",
                ua.UInt16(interpol),
                start_time_tai,
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="Track Called",
            )

    def track_stop(
        self,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Stop tracking."""
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        status, response = self.submit_task(self._track_stop, args=[], task_callback=task_callback)
        return status, response

    def _track_stop(
        self,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> None:
        """Stop tracking.

        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called TrackStop")

        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        try:
            self._call_opc_ua_method(
                task_callback,
                "Management.Commands.Stop",
                "TrackStop",
                ua.UInt16(ua.AxisSelectType.AzEl),
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="TrackStop Called",
            )

    def track_load_static_off(
        self,
        values: list[float],
        task_callback: Optional[Callable] = None,  # type: ignore
    ) -> Tuple[TaskStatus, str]:
        """Load the static pointing model offsets.

        :param values: The static offset values
        :type values: List
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        :return: Task status
        :rtype: Tuple[TaskStatus, str]
        """
        not_connected_resp = "Not connected to the Dish structure controller."
        if not self.scu:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=not_connected_resp,
                )
            return TaskStatus.FAILED, not_connected_resp

        if len(values) != 2:
            return (
                TaskStatus.REJECTED,
                f"Expected 2 arguments (off_xel, off_el) but got {len(values)} arg(s).",
            )

        off_xel = values[0]
        off_el = values[1]
        status, response = self.submit_task(
            self._track_load_static_off, args=[off_xel, off_el], task_callback=task_callback
        )
        return status, response

    def _track_load_static_off(
        self,
        off_xel: float,
        off_el: float,
        task_abort_event: Any,
        task_callback: Optional[Callable] = None,
    ) -> None:
        """Load the static pointing model offsets.

        :param off_xel: The XEl offset
        :type off_xel: float
        :param off_el: The El offset
        :type off_el: float
        :param task_abort_event: Event to indicate that the method in progress should exit.
        :type task_abort_event: threading.Event
        :param task_callback: Update LRC status, defaults to None
        :type task_callback: Optional[Callable], optional
        """
        self.logger.info("Called TrackLoadStaticOff")

        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        try:
            self._call_opc_ua_method(
                task_callback,
                "Tracking.Commands.TrackLoadStaticOff",
                "TrackLoadStaticOff",
                ua.Double(off_xel),
                ua.Double(off_el),
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return

        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED,
                result="TrackLoadStaticOff Called",
            )

        # https://confluence.skatelescope.org/display/SWSI/MID+Dish+Structure+Control

    def indexer_position_to_band_type(self, band_type: IndexerPosition) -> Union[None, enum.Enum]:
        """Map the OPC-UA BandType enum to the IndexerPosition enum."""
        # <Field Name="Band_1" Value="0"/>
        # <Field Name="Band_2" Value="1"/>
        # <Field Name="Band_3" Value="2"/>
        # <Field Name="Band_4" Value="3"/>
        # <Field Name="Band_5a" Value="4"/>
        # <Field Name="Band_5b" Value="5"/>
        # <Field Name="Band_6" Value="6"/>
        # <Field Name="Optical" Value="7"/>
        mapping = {
            IndexerPosition.B1: ua.BandType.Band_1,
            IndexerPosition.B2: ua.BandType.Band_2,
            IndexerPosition.B3: ua.BandType.Band_3,
            IndexerPosition.B4: ua.BandType.Band_4,
            IndexerPosition.B5: ua.BandType.Band_5a,
        }

        return mapping.get(band_type, None)

    def track_load_table(
        self,
        values: list[float],
    ) -> Tuple[ResultCode, str]:
        """Load the track table."""
        # perform validation checks
        if len(values) < 5:
            return (
                ResultCode.REJECTED,
                f"Length of argument ({len(values)}) is not as expected (>4).",
            )

        load_mode = TrackTableLoadMode(int(values[0]))
        sequence_length = int(values[1])
        track_table = values[2:]

        # Check that the track_table length is a multiple of 3
        if len(track_table) % 3 != 0:
            return (
                ResultCode.REJECTED,
                f"Length of table ({len(track_table)}) is not a multiple of 3 "
                "(timestamp, azimuth coordinate, elevation coordinate) as expected.",
            )

        # Check the given sequence length matches the given track table
        sequences_in_table = len(track_table) // 3
        if sequences_in_table != sequence_length:
            return (
                ResultCode.REJECTED,
                f"Sequence length ({sequence_length}) does not match "
                f"number of sequences in table ({sequences_in_table})",
            )

        tai_offsets = []
        azimuths = []
        elevations = []

        for i in range(sequence_length):
            timestamp_index = i * 3
            az_index = i * 3 + 1
            el_index = i * 3 + 2
            tai_offsets.append(track_table[timestamp_index])
            azimuths.append(track_table[az_index])
            elevations.append(track_table[el_index])

        if not are_points_within_dish_limits(list(zip(azimuths, elevations)), science_limits=True):
            return (
                ResultCode.REJECTED,
                "Track Table point(s) are not within the dishes limits"
                f" (Az: [{MIN_AZIMUTH}, {MAX_AZIMUTH}],"
                f" El: [{MIN_ELEVATION_SCIENCE}, {MAX_ELEVATION_SCIENCE}])",
            )

        # send the request to the opcua server
        self.logger.info("Called TrackLoadTable")

        try:
            if sequence_length < 1000:
                padding_required = 1000 - sequence_length
                tai_offsets.extend([0] * padding_required)
                azimuths.extend([0] * padding_required)
                elevations.extend([0] * padding_required)

            cast_tai = [ua.Double(val) for val in tai_offsets]
            cast_az = [ua.Double(val) for val in azimuths]
            cast_el = [ua.Double(val) for val in elevations]

            self._call_opc_ua_method(
                None,
                "Tracking.Commands.TrackLoadTable",
                "TrackLoadTable",
                ua.UInt16(load_mode),
                ua.UInt16(sequence_length),
                cast_tai,
                cast_az,
                cast_el,
            )

        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return (
                ResultCode.FAILED,
                f"TrackLoadTable call on the DS Controller failed with message {ua_error}",
            )

        return ResultCode.OK, "TrackLoadTable successfully triggered the DS Controller"

    def update_pointing_model_params(
        self,
        band: BandType,
        values: list[float],
    ) -> tuple:
        """Update the threshold levels for AZ/EL pointing error between actual and desired values.

        :param band: The Band type for which the pointing model parameters will be applied
        :type band: BandType

        :param values:
            A list containing exactly two elements:
            - `on_source_threshold` (float): The threshold value for the pointing error.
            - `threshold_time_period` (float): The time period over which the threshold is applied.
        :type values: list[float]

        :return: Task status
        :rtype: Tuple[TaskStatus, str]

        :raises ValueError: If the input list does not contain exactly two elements.
        """
        # The argument value is a list of 18 floats
        if len(values) != BAND_POINTING_MODEL_PARAMS_LENGTH:
            return (
                TaskStatus.FAILED,
                f"Expected {BAND_POINTING_MODEL_PARAMS_LENGTH} arguments but got"
                f" {len(values)} arg(s).",
            )

        try:
            band = ua.UInt16(band)

            cast_values = []
            for value in values:
                cast_values.append(ua.Double(value))

            args = [True, ua.UInt16(ua.TiltOnType.TiltmeterOne), True, band]
            self.logger.info("Calling PmCorrOnOff with args %s", args)
            self._call_opc_ua_method(None, "Pointing.Commands.PmCorrOnOff", "PmCorrOnOff", *args)

            args = [band, *cast_values]
            self.logger.info("Calling StaticPmSetup with args %s", args)
            self._call_opc_ua_method(
                None, "Pointing.Commands.StaticPmSetup", "StaticPmSetup", *args
            )
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return TaskStatus.FAILED, "Failed to update static pointing model parameters."

        return TaskStatus.COMPLETED, "Successfully updated static pointing model parameters."

    def update_threshold_levels(self, values: list[float]) -> tuple:
        """Update the threshold levels for AZ/EL pointing error between actual and desired values.

        :param values:
            A list containing exactly two elements:
            - `on_source_threshold` (float): The threshold value for the pointing error.
            - `threshold_time_period` (float): The time period over which the threshold is applied.
        :type values: list[float]

        :return: Task status
        :rtype: Tuple[TaskStatus, str]

        :return:TaskStatus
        :rtype: tuple[bool, str]

        :raises ValueError: If the input list does not contain exactly two elements.
        """
        if len(values) != 2:
            return (
                TaskStatus.FAILED,
                "Expected 2 arguments (on_source_threshold, threshold_time_period)"
                f" but got {len(values)} arg(s).",
            )

        on_source_threshold = values[0]
        threshold_time_period = values[1]

        try:
            self._call_opc_ua_method(
                None,
                "Management.Commands.SetOnSourceThreshold",
                "SetOnSourceThreshold",
                on_source_threshold,
                threshold_time_period,
            )
            return TaskStatus.COMPLETED, "Successfully updated the threshold level."
        except (CommandNotAuthorized, RuntimeError) as ua_error:
            self.logger.exception(ua_error)
            return TaskStatus.FAILED, "Updating the threshold level was unsuccessful."

    def _call_opc_ua_method(
        self, task_callback: Any, node_path: str, command_name: str, *args: Any
    ) -> CmdReturn:  # type: ignore
        """Execute a OPCUA method.

        :param task_callback: Callback to update the LRC
        :type task_callback: Any
        :param node_path: The OPCUA node path
        :type node_path: str
        :param command_name: The originating command name
        :type command_name: str
        :raises RuntimeError: Something went wrong
        :raises CommandNotAuthorized: Could not get auth
        :return: The result
        :rtype: CmdReturn
        """
        if not self.scu:
            raise RuntimeError("Communication with the DSC not established")
        if task_callback:
            task_callback(
                progress=(
                    f"Path [{node_path}] called with args [{args}] from [{command_name}] command"
                )
            )

        dsccmdauth_path = "CommandArbiter.Status.DscCmdAuthority"
        dsccmdauth = DscCmdAuthType(self.scu.attributes[dsccmdauth_path].value)

        command_result = None  # type: ignore[no-redef]
        if dsccmdauth == DscCmdAuthType.LMC:
            command_result = self.scu.commands[node_path](*args)
            self.logger.debug("Command result is [%s]", command_result)

            # handle case when session id is expired or
            # taking over authority from another LMC client
            if command_result[0] == SCUResultCode.NO_CMD_AUTH:
                code, msg = self.scu.take_authority(DscCmdAuthType.LMC)
                self.logger.debug("Taking authority result result is [%s] [%s]", code, msg)

                # Cannot take auth
                if code != SCUResultCode.COMMAND_DONE:
                    if task_callback:
                        task_callback(
                            status=TaskStatus.FAILED,
                            result=f"Could not get device DSC authorization [{msg}]",
                        )
                    raise CommandNotAuthorized("Could not get auth")

                command_result = self.scu.commands[node_path](*args)
                self.logger.debug("Command result is [%s]", command_result)

        elif dsccmdauth == DscCmdAuthType.NO_AUTHORITY:
            code, msg = self.scu.take_authority(DscCmdAuthType.LMC)
            self.logger.debug("Taking authority result result is [%s] [%s]", code, msg)

            # Cannot take auth
            if code != SCUResultCode.COMMAND_DONE:
                if task_callback:
                    task_callback(
                        status=TaskStatus.FAILED,
                        result=f"Could not get device DSC authorization [{msg}]",
                    )
                raise CommandNotAuthorized("Could not get auth")

            command_result = self.scu.commands[node_path](*args)
            self.logger.debug("Command result is [%s]", command_result)

        # Someone higher has auth
        else:
            if task_callback:
                task_callback(
                    status=TaskStatus.FAILED,
                    result=(
                        "Command not executed because DscCmdAuthority "
                        "is held by a higher priority client."
                    ),
                )
            raise CommandNotAuthorized(
                "Command not executed because DscCmdAuthority "
                "is held by a higher priority client."
            )

        if command_result:
            result_code: SCUResultCode = command_result[0]
            if result_code in (SCUResultCode.COMMAND_DONE, SCUResultCode.COMMAND_ACTIVATED):
                if task_callback:
                    task_callback(
                        status=TaskStatus.COMPLETED,
                        result=f"Result for [{node_path}] is [{command_result}]",
                    )
                return command_result

        if task_callback:
            task_callback(
                status=TaskStatus.FAILED,
                result=f"[{node_path}] failed with result [{command_result}]",
            )
        raise RuntimeError(f"[{node_path}] failed with result [{command_result}]")


def main() -> None:
    """For local testing."""
    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler(sys.stdout))

    scu = OPCUAControl(
        "127.0.0.1", 4840, endpoint="/dish-structure/server/", namespace="http://skao.int/DS_ICD/"
    )
    scu.connect_and_setup()
    scu.take_authority("LMC")
    task_executor_cm = TaskExecutorComponentManager(logger=logger)
    com_manager = CommandManager(logger, scu, task_executor_cm.submit_task)
    com_manager.set_standby_lp_mode({"dscpowerlimitkw": 10.0})


if __name__ == "__main__":
    main()
