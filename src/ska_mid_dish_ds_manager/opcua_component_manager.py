# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-instance-attributes
# pylint: disable=abstract-method,broad-exception-caught
# mypy: disable-error-code="attr-defined"
# mypy: disable-error-code="no-redef"
"""A Component Manager for an OPCUA device."""
import dataclasses
import json
import logging
import sys
import time
from queue import Empty, Queue
from threading import Event, Thread
from typing import Any, Callable, Tuple

from ska_control_model import CommunicationStatus, HealthState, TaskStatus
from ska_tango_base.executor import TaskExecutorComponentManager

from ska_mid_dish_ds_manager import release
from ska_mid_dish_ds_manager.command_manager import CommandManager
from ska_mid_dish_ds_manager.models.constants import (
    DEFAULT_AZIMUTH_SPEED_DPS,
    DEFAULT_ELEVATION_SPEED_DPS,
    DSC_MIN_POWER_LIMIT_KW,
)
from ska_mid_dish_ds_manager.models.data_classes import (
    DscBuildStateDataclass,
    DsmBuildStateDataclass,
)
from ska_mid_dish_ds_manager.models.dish_enums import (
    BandType,
    TrackInterpolationMode,
    TrackProgramMode,
)
from ska_mid_dish_ds_manager.opcua_control import OPCUAControl
from ska_mid_dish_ds_manager.subscription_manager import SubscriptionManager
from ska_mid_dish_ds_manager.utils import SubscriptionEvent

logging.getLogger("asyncua").setLevel(logging.WARN)
logging.getLogger("ska_mid_dish_steering_control").setLevel(logging.WARN)
logging.getLogger("ska-mid-ds-scu").setLevel(logging.WARN)


class OPCUAComponentManager(TaskExecutorComponentManager):
    """Component manager for an OPCUA device."""

    def __init__(
        self: "OPCUAComponentManager",
        ds_host: str,
        ds_port: int,
        ds_endpoint: str,
        ds_opcua_namespace: str,
        logger: logging.Logger,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """Init the component manager.

        You need to call `start_communication` to connect to
        the OPCUA server.
        This wll:
        - Set up the subscriptions
        - Create the attributes and commands that map to the OPCUA device
        - Start a thread that will consume the subscription events and update the component state.
        We only store what's needed for LMC in the component state.

        `stop_communication` will:
        - Disconnect from the OPCUA server
        - Stop the subscription thread and delete the Thread instance
        - Delete the SCU instance

        :param ds_host: The host for the OPCUA server
        :type ds_host: str
        :param ds_host: The port for the OPCUA server
        :type ds_host: int
        :param ds_endpoint: The OPCUA server connection endpoint
        :type ds_endpoint: str
        :param ds_opcua_namespace: The connection namespace
        :type ds_opcua_namespace: str
        :param logger: logger
        :type logger: logging.Logger
        """
        self.ds_host = ds_host
        self.ds_port = ds_port
        self.ds_endpoint = ds_endpoint

        self.ds_opcua_namespace = ds_opcua_namespace
        self.logger = logger
        self.tango_opcua_attr_mapper: dict = {}

        # Event handling
        self._consuming_thread: Thread | None = None
        self._stop_consumer_event: Event = Event()
        self._events_queue: Queue = Queue()

        # Connection handling
        self._connect_thread: Thread | None = None
        self._stop_connect_event = Event()

        self.tango_opcua_attr_mapper = {}
        self.scu: OPCUAControl | None = None
        self.subscription_manager = SubscriptionManager(self.logger)

        self.command_manager: CommandManager = CommandManager(
            logger=self.logger,
            scu=self.scu,
            submit_task=self.submit_task,
        )

        super().__init__(
            logger,
            *args,
            # Subscribed on OPCUA
            powerstate=None,
            dscstate=None,
            dsccmdauth=None,
            pointingstate=None,
            operatingmode=None,
            indexerposition=None,
            desiredpointingaz=None,
            desiredpointingel=None,
            achievedpointing=None,
            currentpointing=[0, 0, 0, 0, 0, 0, 0],
            currentmode=None,
            applicationstate=None,
            healthstate=HealthState.OK,
            actptactindex=0,
            actptendindex=0,
            achievedtargetlock=None,
            actstaticoffsetvaluexel=None,
            actstaticoffsetvalueel=None,
            hhpconnected=None,
            band0pointingmodelparams=[],
            band1pointingmodelparams=[],
            band2pointingmodelparams=[],
            band3pointingmodelparams=[],
            band4pointingmodelparams=[],
            band5apointingmodelparams=[],
            band5bpointingmodelparams=[],
            band6pointingmodelparams=[],
            configuretargetlock=[0.0, 0.0],
            onsourcethreshold=0.0,
            thresholdtimeperiod=0.0,
            # Read off the OPCUA as needed
            buildstate="",
            # State managed separately from OPCUA
            trackinterpolationmode=TrackInterpolationMode.SPLINE,
            azimuthspeed=DEFAULT_AZIMUTH_SPEED_DPS,
            elevationspeed=DEFAULT_ELEVATION_SPEED_DPS,
            dscpowerlimitkw=DSC_MIN_POWER_LIMIT_KW,
            trackprogrammode=TrackProgramMode.TABLE,
            **kwargs,
        )

    def _update_component_state(self, **kwargs: Any) -> None:
        """Log the new component state."""
        if not any(
            attr.lower()
            in [
                "desiredpointingaz",
                "desiredpointingel",
                "currentpointing",
                "achievedpointing",
                "dscstatuspdesiredaz",
                "dscstatuspdesiredel",
                "dscstatusonsourcedev",
                "actptactindex",
                "actptendindex",
            ]
            for attr in kwargs
        ):
            self.logger.debug("Updating ds manager component state with [%s]", kwargs)
        super()._update_component_state(**kwargs)

    def setup_subscriptions(self) -> None:
        """Set up the subscriptions.

        Should be called after connection has been established.

        `DscState` sub is used to react to loss of connectivity to the device.
        """
        if not self.scu:
            raise RuntimeError("SCU has not been initialised")

        self.subscription_manager.setup_subscriptions(
            self.scu, self._events_queue, self._lost_connection
        )

        # Update the communication state to established
        self._update_communication_state(CommunicationStatus.ESTABLISHED)

    def _lost_connection(self, *args: Any, **kwargs: Any) -> None:
        """Retries the connection when connection is lost.

        Stop the communication and then try and connect again.
        """
        self.logger.info("Lost connection. args: %s, kwargs: %s", args, kwargs)
        self.scu = None  # There is no connection so cannot disconnect
        self.stop_communicating()
        self.start_communicating()

    def event_handler_cb(
        self, scu: OPCUAControl | None = None, sub_event: dict | None = None
    ) -> None:
        """React to the events from the subscriptions _or_ the connection attempt.

        Sub event example:
        {'name': 'Management.Status.DscState',
        'node':
        Node(NodeId(Identifier=6188,
        NamespaceIndex=2,
        NodeIdType=<NodeIdType.Numeric: 2>)),
        'value': 1,
        'source_timestamp': datetime.datetime(2024, 11, 1, 11, 14, 47, 148242,
        tzinfo=datetime.timezone.utc),
        'server_timestamp': 1730459687.148249,
        'data': DataChangeNotification
        <asyncua.common.subscription.SubscriptionItemData object at 0x11544a2c0>,
        MonitoredItemNotification(ClientHandle=201,
        Value=DataValue(Value=Variant(Value=1, VariantType=<VariantType.Int32: 6>,
        Dimensions=None, is_array=False),StatusCode_=StatusCode(value=0),
        SourceTimestamp=datetime.datetime(2024, 11, 1, 11, 14, 47, 148242,
        tzinfo=datetime.timezone.utc),
        ServerTimestamp=datetime.datetime(2024, 11, 1, 11, 14, 47, 148249,
        tzinfo=datetime.timezone.utc), SourcePicoseconds=None,
        ServerPicoseconds=None)))}}

        :param scu: OPCUAControl instance, defaults to None
        :type scu: OPCUAControl | None, optional
        :param sub_event: OPCUA event, defaults to None | dict
        :type sub_event: OPCUA event, optional
        """
        if sub_event:
            # Convert the event to `SubscriptionEvent` for better ergonomics
            # May remove in future to do less processing per event
            subs_event = SubscriptionEvent(**sub_event)

            mon_node = self.subscription_manager.get_monitored_node(subs_event.name)

            if mon_node is None:
                self.logger.warning(
                    "Got event callback for node not in monitored nodes. %s.", subs_event.name
                )
                return

            # Convert the val if needed. E.g to an Enum.
            if mon_node.convert_to_component_state_val:
                converted_value = mon_node.convert_to_component_state_val(subs_event)
                component_state_name = mon_node.component_state_name
                self._update_component_state(**{component_state_name: converted_value})
            # Do any additional calculations and update
            for state_calc in mon_node.calculate_updates:
                update_vals = state_calc(subs_event, self.component_state)
                if update_vals:
                    self._update_component_state(**update_vals)

        if scu:
            self.scu = scu
            self.command_manager.scu = self.scu
            self.setup_subscriptions()
            self._update_build_state_info()

    @classmethod
    def _attempt_connection(
        cls,
        ds_host: str,
        ds_port: int,
        ds_endpoint: str,
        ds_opcua_namespace: str,
        stop_connect_event: Event,
        events_queue: Queue,
        logger: logging.Logger,
    ) -> None:
        """Attempt connection to the OPCUA until successful.

        :param ds_host: OPCUA server hostname
        :type ds_host: str
        :param ds_port: OPCUA server port
        :type ds_port: int
        :param ds_endpoint: The connection endpoint
        :type ds_endpoint: str
        :param ds_opcua_namespace: The connection namespace
        :type ds_opcua_namespace: str
        :param stop_connect_event: Event instance to indicate if we need to exit this thread
        :type stop_connect_event: Event
        :param events_queue: When connection succeeds we indicate it by putting the connection
            instance onto the queue, to be handled by the queue consumer.
        :type events_queue: Queue
        :param logger: logger
        :type logger: logging.Logger
        """
        scu = None
        while (not stop_connect_event.is_set()) or (not scu):
            try:
                scu = OPCUAControl(
                    host=ds_host,
                    port=ds_port,
                    endpoint=ds_endpoint,
                    namespace=ds_opcua_namespace,
                    timeout=1.0,
                )
                scu.connect_and_setup()
                events_queue.put(scu)
                break
            except Exception:
                # If anything goes wrong we need to continue
                scu = None
                logger.exception(
                    (
                        "Could not connect to OPCUA on host [%s] port [%s]"
                        "endpoint [%s] namespace [%s], retrying in 1 sec"
                    ),
                    ds_host,
                    ds_port,
                    ds_endpoint,
                    ds_opcua_namespace,
                )
                stop_connect_event.wait(timeout=1)

    def _stop_connection_thread(self) -> None:
        """Stop the connection attempt thread if in progress."""
        if self._connect_thread and self._connect_thread.is_alive():
            self._stop_connect_event.set()
            self._connect_thread.join()
        self._stop_connect_event.clear()
        self._connect_thread = None

    def _stop_consumer_thread(self) -> None:
        """Stop the consuming thread if in progress."""
        if self._consuming_thread and self._consuming_thread.is_alive():
            self._stop_consumer_event.set()
            self._consuming_thread.join()
        self._stop_consumer_event.clear()
        self._consuming_thread = None

    def start_communicating(self: "OPCUAComponentManager") -> None:
        """Start communication with the OPCUA device.

        Retry until successful in a thread so as not to block the Tango device
        """
        self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

        self._stop_connection_thread()

        # Start the connection attempt thread
        self._connect_thread = Thread(
            target=self._attempt_connection,
            args=[
                self.ds_host,
                self.ds_port,
                self.ds_endpoint,
                self.ds_opcua_namespace,
                self._stop_connect_event,
                self._events_queue,
                self.logger,
            ],
        )
        self._connect_thread.start()

        self._stop_consumer_thread()

        # Start the consuming thread if in progress
        self._consuming_thread = Thread(
            target=self._consume_events,
            args=[
                self._events_queue,
                self._stop_consumer_event,
                self.event_handler_cb,
                self.logger,
            ],
        )
        self._consuming_thread.start()

    def stop_communicating(self: "OPCUAComponentManager") -> None:
        """Stop communication."""
        self._update_communication_state(CommunicationStatus.DISABLED)

        self._stop_connection_thread()
        self._stop_consumer_thread()

        if self.scu:
            self.scu.disconnect_and_cleanup()
        self.scu = None

    @classmethod
    def _consume_events(
        cls,
        events_queue: Queue,
        stop_consumer_event: Event,
        event_handler_cb: Callable,
        logger: logging.Logger,
    ) -> None:
        """Consume thread events.

        These events are generated by subscriptions as well the connection operation.

        The callback is called with the appropriate method set.

        :param events_queue: The Queue where events are pulled from
        :type events_queue: Queue
        :param stop_consumer_event: The threading event to indicate that we should exit
        :type stop_consumer_event: Event
        :param event_handler_cb: The callback to call when we got something off the queue
        :type event_handler_cb: callable
        :param logger: logger
        :type logger: logging.Logger
        """
        while not stop_consumer_event.is_set():
            try:
                item = events_queue.get(timeout=0.5)
                if isinstance(item, OPCUAControl):
                    event_handler_cb(scu=item)
                else:
                    event_handler_cb(sub_event=item)
            except Empty:
                stop_consumer_event.wait(0.5)
            except Exception:
                # If anything goes wrong we need to continue
                logger.exception("Handling the event failed")
                stop_consumer_event.wait(0.5)

    def _update_build_state_info(self) -> None:
        """Update the combined Dish manager and DSC buildState structure."""
        if not self.scu:
            self.logger.warning("Could not update build state info. SCU instance is None.")
            return

        try:
            # Get buildState data nodes
            dish_id = self.scu.attributes["Management.NamePlate.DishId"].value
            dsc_ser_num = self.scu.attributes["Management.NamePlate.DishStructureSerialNo"].value
            dsc_software_ver = self.scu.attributes["Management.NamePlate.DscSoftwareVersion"].value
            dsc_icd_ver = self.scu.attributes["Management.NamePlate.IcdVersion"].value

            # Construct DSC build state info struct
            dsc_build_state = DscBuildStateDataclass(
                "Dish Structure Controller",
                dish_id,
                dsc_ser_num,
                dsc_software_ver,
                dsc_icd_ver,
            )
        except KeyError:
            self.logger.exception("No build state info")
            dsc_build_state = DscBuildStateDataclass(
                "Failed to retrieve buildState data for Dish structure controller",
                "",
                "",
                "",
                "",
            )

        # Construct dish manager version info
        dm_build_state = DsmBuildStateDataclass(
            release.name,
            release.get_ds_manager_release_version(),
            f"opc.tcp://{self.ds_host}:{self.ds_port}{self.ds_endpoint}",
            dsc_build_state,
        )

        dsc_build_state_json = json.dumps(dataclasses.asdict(dm_build_state), indent=4)
        self._update_component_state(buildstate=dsc_build_state_json)

    def set_standby_lp_mode(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call set_standby_lp_mode on the DSC."""
        return self.command_manager.set_standby_lp_mode(self._component_state, *args, **kwargs)

    def set_standby_fp_mode(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call set_standby_fp_mode on the DSC."""
        return self.command_manager.set_standby_fp_mode(self._component_state, *args, **kwargs)

    def set_point_mode(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call set_point_mode on the DSC."""
        return self.command_manager.set_point_mode(*args, **kwargs)

    def set_maintenance_mode(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call set_maintenance_mode on the DSC."""
        return self.command_manager.set_maintenance_mode(*args, **kwargs)

    def set_index_position(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call set_index_position on the DSC."""
        return self.command_manager.set_index_position(*args, **kwargs)

    def slew(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call slew on the DSC."""
        return self.command_manager.slew(self._component_state, *args, **kwargs)

    def track(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call track on the DSC."""
        return self.command_manager.track(*args, **kwargs)

    def track_stop(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call track_stop on the DSC."""
        return self.command_manager.track_stop(*args, **kwargs)

    def track_load_static_off(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call track_load_static_off on the DSC."""
        return self.command_manager.track_load_static_off(*args, **kwargs)

    def set_power_mode(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call set_power_mode on the DSC."""
        return self.command_manager.set_power_mode(self._update_component_state, *args, **kwargs)

    def stow(self, *args: Any, **kwargs: Any) -> Tuple[TaskStatus, str]:
        """Call stow on the DSC."""
        return self.command_manager.stow(*args, **kwargs)

    def update_pointing_model_params(
        self,
        band: BandType,
        values: list[float],
    ) -> tuple:
        """Update the static pointing model parameters on the Dish Structure."""
        return self.command_manager.update_pointing_model_params(band, values)

    def update_threshold_levels(self, values: list[float]) -> tuple:
        """Update the threshold level between actual and desire AZ/EL pointing error."""
        return self.command_manager.update_threshold_levels(values)

    def read_node_value(
        self,
        _: Any,
        tango_attr: Any,
    ) -> str:
        """Read a node value from the OPCUA server.

        :param _: instance of the DSManager
        :type _: DSManager
        :param tango_attr: method to push change events, defaults to None
        :type tango_attr: Tango.Wattribute
        """
        if not self.scu:
            raise RuntimeError("Connection to the DSC has not completed.")
        tango_attr_name = tango_attr.get_name()
        self.logger.debug("Reading attribute %s from the OPCUA server.", tango_attr_name)
        if tango_attr_name not in self.tango_opcua_attr_mapper:
            raise RuntimeError(
                f"Attribute name [{tango_attr_name}] is not known to the component manager"
            )
        try:
            opcua_mapped_val = self.tango_opcua_attr_mapper[tango_attr_name]
            opcua_node_val = self.scu.attributes[opcua_mapped_val].value
            return str(opcua_node_val)
        except Exception:
            self.logger.exception("Could not read attribute")
            raise


def main() -> None:
    """For local testing."""
    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler(sys.stdout))

    cm = OPCUAComponentManager(
        "127.0.0.1", 4840, "/dish-structure/server/", "http://skao.int/DS_ICD/", logger
    )
    cm.start_communicating()
    time.sleep(2)
    cm.stop_communicating()


if __name__ == "__main__":
    main()
