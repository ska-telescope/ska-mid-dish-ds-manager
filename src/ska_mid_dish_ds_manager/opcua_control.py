"""Module to take care of the OPCUA connection."""

import asyncio
from typing import Union

from asyncua import Node
from ska_mid_dish_steering_control import SteeringControlUnit
from ska_mid_dish_steering_control.sculib import create_ro_attribute


class OPCUAControl(SteeringControlUnit):
    """Subclassing SteeringControlUnit to add additional functionality.

    Specifically extending the nodes that SCU exposed. In addition to what is exposed under
    PLC_PRG, we also need `ApplicationState` and `ServerStatus`
    """

    async def get_first_node_by_browse_name(
        self, parent_node: Node, browse_name: str
    ) -> Union[Node, None]:
        """Return the first match of a browse name.

        We skip PLC_PRG since it is already included and quite large.

        :param parent_node: The parent node to traverse
        :type parent_node: Node
        :param browse_name: The browse name to search for
        :type browse_name: str
        :return: The found node, if any
        :rtype: Node
        """
        node_browse_qual_name = await parent_node.read_browse_name()
        if browse_name == node_browse_qual_name.Name:
            return parent_node

        children = await parent_node.get_children()
        for child_node in children:
            node_browse_qual_name = await child_node.read_browse_name()
            if node_browse_qual_name.Name == "PLC_PRG":
                continue
            result = await self.get_first_node_by_browse_name(child_node, browse_name)
            if result:
                return result
        return None

    @staticmethod
    async def node_to_dot_notation(node: Node) -> str:
        """Take a node and create its dot notated name.

        We remove "Root" and "Objects" from the path.

        :param node: The node
        :type node: Node
        :return: The name
        :rtype: str
        """
        # Keiths PLC has the application node with a "+" infront of Application so we use the
        # get_first_node_by_browse_name method to find the node ApplicationState and return the
        # same dot notation for any variants.
        if "ApplicationState" in str(node.nodeid.Identifier):
            return "Logic.Application.ApplicationState"

        nodes = await node.get_path()
        node_names = []
        for tree_node in nodes:
            browse_qual_name = await tree_node.read_browse_name()
            node_names.append(browse_qual_name.Name)
        return ".".join(node_names[2:])

    def connect_and_setup(self) -> None:
        """Override connect_and_setup to do more.

        During `connect_and_setup` the `PLC_LRG` nodes are exposed.
        We go and add "ApplicationState", "CurrentMode, and "ServerStatus" to the nodes list.
        """
        super().connect_and_setup()
        root_node = self._client.get_root_node()

        # Load up extra nodes
        for node_browse_name in [
            "ApplicationState",
            "CurrentMode",
            "ServerStatus",
        ]:
            coro = self.get_first_node_by_browse_name(root_node, node_browse_name)
            node = asyncio.run_coroutine_threadsafe(coro, self.event_loop).result()
            if node:
                coro = self.node_to_dot_notation(node)
                dotted_name = asyncio.run_coroutine_threadsafe(coro, self.event_loop).result()

                node_class = (
                    asyncio.run_coroutine_threadsafe(node.read_node_class(), self.event_loop)
                    .result()
                    .value
                )

                if node_class == 2:  # attribute node
                    self._attributes[dotted_name] = create_ro_attribute(
                        node, self.event_loop, dotted_name
                    )
                self._nodes[dotted_name] = (node, node_class)
                self._nodes_reversed[node] = dotted_name


if __name__ == "__main__":
    scu = OPCUAControl(
        "127.0.0.1", 4840, endpoint="/dish-structure/server/", namespace="http://skao.int/DS_ICD/"
    )
    scu.connect_and_setup()
