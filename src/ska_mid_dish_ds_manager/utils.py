"""Shared methods in package."""

# pylint: disable=invalid-name
import datetime
import time
from dataclasses import dataclass
from typing import Any

from astropy.time import Time
from asyncua import Node, ua
from tango import InfoIt

from ska_mid_dish_ds_manager.models.constants import (
    MAX_AZIMUTH,
    MAX_ELEVATION,
    MAX_ELEVATION_SCIENCE,
    MIN_AZIMUTH,
    MIN_ELEVATION,
    MIN_ELEVATION_SCIENCE,
)

SKA_EPOCH = "1999-12-31T23:59:28Z"


def get_tai_timestamp_from_unix_s(unix_s: float) -> float:
    """
    Calculate atomic time in seconds from unix time in seconds.

    :param unix_s: Unix time in seconds

    :return: atomic time (tai) in seconds
    """
    unix_time = Time(unix_s, format="unix")
    ska_epoch_tai = Time(SKA_EPOCH, scale="utc").unix_tai
    return unix_time.unix_tai - ska_epoch_tai


def get_tai_timestamp_from_datetime(datetime_obj: datetime.datetime) -> float:
    """Convert a datetime object into a TAI timestamp."""
    source_timestamp_unix = datetime_obj.timestamp()
    source_timestamp_tai = Time(source_timestamp_unix, format="unix").unix_tai
    ska_epoch_tai = Time(SKA_EPOCH, scale="utc").unix_tai

    return source_timestamp_tai - ska_epoch_tai


def get_current_tai_timestamp() -> float:
    """Get the current time as a TAI timestamp."""
    return get_tai_timestamp_from_unix_s(time.time())


def is_point_within_dish_limits(pos_az: float, pos_el: float, science_limits: bool) -> bool:
    """Check if the given point is within the dishes limits."""
    az_param_valid = MAX_AZIMUTH >= pos_az >= MIN_AZIMUTH

    el_lim_max = MAX_ELEVATION_SCIENCE if science_limits else MAX_ELEVATION
    el_lim_min = MIN_ELEVATION_SCIENCE if science_limits else MIN_ELEVATION
    el_param_valid = el_lim_max >= pos_el >= el_lim_min

    return az_param_valid and el_param_valid


def are_points_within_dish_limits(points: list[tuple[float, float]], science_limits: bool) -> bool:
    """Check if the given points are within the dishes limits."""
    for pos_az, pos_el in points:
        if not is_point_within_dish_limits(pos_az, pos_el, science_limits):
            return False
    return True


def generate_tango_name(opcua_node_name: str) -> str:
    """Generate a tango attribute name.

    :param opcua_node_name: OPCUA Node
    :type opcua_node_name: Node
    :return: generated name
    :rtype: str
    """
    list_of_pos_names = opcua_node_name.split(".")
    # There are 2 attributes for p_Set & p_Act under different parent nodes
    if list_of_pos_names[-1] in ["p_Set", "p_Act"]:
        return list_of_pos_names[0] + "_" + list_of_pos_names[-1]

    return list_of_pos_names[-1]


class BaseInfoIt(InfoIt):
    """Update Tango InfoIt to not truncate the result."""

    @staticmethod
    def _LogIt__compact_elem(v: Any, **_: Any) -> str:
        """Just return the item as is."""
        v = repr(v)
        return v


@dataclass
class SubscriptionEvent:
    """Convert the event to a dataclass for better ergonomics."""

    name: str
    node: Node
    value: Any
    source_timestamp: datetime.datetime
    server_timestamp: float
    data: ua.DataChangeNotification
