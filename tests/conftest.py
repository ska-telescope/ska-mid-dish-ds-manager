"""Contains pytest fixtures for other tests setup."""

# pylint: disable=redefined-outer-name


from typing import Any

import pytest
import tango

from tests.utils import EventStore


def pytest_addoption(parser: Any) -> None:
    """Pytest options."""
    default_ds_host = "ds-opcua-server-simulator-001-svc"
    default_ds_port = 4840
    default_ds_endpoint = "/dish-structure/server/"
    default_ds_opcua_namespace = "http://skao.int/DS_ICD/"

    parser.addoption(
        "--ds-host",
        action="store",
        default=default_ds_host,
        help="Host address for the DS OPC-UA server",
    )
    parser.addoption(
        "--ds-port",
        action="store",
        type=int,
        default=default_ds_port,
        help="Port for the DS OPC-UA server",
    )
    parser.addoption(
        "--ds-endpoint",
        action="store",
        default=default_ds_endpoint,
        help="Endpoint path for the DS OPC-UA server",
    )
    parser.addoption(
        "--ds-opcua-namespace",
        action="store",
        default=default_ds_opcua_namespace,
        help="Namespace for the DS OPC-UA server",
    )


@pytest.fixture
def ds_host(request: Any) -> str:
    """Fixture for the DS host."""
    return request.config.getoption("--ds-host")


@pytest.fixture
def ds_port(request: Any) -> int:
    """Fixture for the DS port."""
    return request.config.getoption("--ds-port")


@pytest.fixture
def ds_endpoint(request: Any) -> str:
    """Fixture for the DS endpoint."""
    return request.config.getoption("--ds-endpoint")


@pytest.fixture
def ds_opcua_namespace(request: Any) -> str:
    """Fixture for the DS OPC-UA namespace."""
    return request.config.getoption("--ds-opcua-namespace")


@pytest.fixture
def event_store_class() -> type[EventStore]:
    """Fixture for storing events."""
    return EventStore


@pytest.fixture
def ds_manager_device_fqdn() -> str:
    """Return the ds manager tango fqdn."""
    return "mid-dish/ds-manager/SKA001"


@pytest.fixture
def ds_manager_proxy(ds_manager_device_fqdn: str) -> tango.DeviceProxy:
    """Return a tango proxy to the ds manager."""
    return tango.DeviceProxy(ds_manager_device_fqdn)
