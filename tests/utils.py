# pylint: disable=too-few-public-methods
"""General utils for test devices."""

import queue
import time
from math import isclose
from typing import Any, Callable, List, Optional, Tuple

import numpy as np
import pytest
import tango


class EventStore:
    """Store events with useful functionality."""

    def __init__(self) -> None:
        """Init Store."""
        self._queue: queue.Queue = queue.Queue()

    def push_event(self, event: tango.EventData) -> None:
        """Store the event.

        :param event: Tango event
        :type event: tango.EventData
        """
        self._queue.put(event)

    def wait_for_value(self, value: Any, timeout: int = 3, approximate: bool = False) -> bool:
        """Wait for a value to arrive.

        Wait `timeout` seconds for each fetch.

        :param value: The value to check for
        :type value: Any
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If None are found
        :return: True if found
        :rtype: bool
        """
        try:
            events = []
            while True:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if self._is_desired_event(event, value, approximate):
                    return True
        except queue.Empty as err:
            ev_vals = self.extract_event_values(events)
            raise RuntimeError(f"Never got an event with value [{value}] got [{ev_vals}]") from err

    def wait_for_condition(self, condition: Callable, timeout: int = 3) -> bool:
        """Wait for a generic condition.

        Wait `timeout` seconds for each fetch.

        :param value: The value to check for
        :type value: Any
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If None are found
        :return: True if found
        :rtype: bool
        """
        try:
            events = []
            start_time = time.time()
            while time.time() - start_time < timeout:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if condition(event.attr_value.value):
                    return True
            raise TimeoutError("Timeout exceeded.")
        except (TimeoutError, queue.Empty) as err:
            ev_vals = self.extract_event_values(events)
            raise RuntimeError(f"Never got an event that meets condition got [{ev_vals}]") from err

    def wait_for_event_condition(self, condition: Callable, timeout: int = 3) -> bool:
        """Wait for a generic condition with an event object.

        Wait `timeout` seconds for each fetch.

        :param condition: The condition callable
        :type value: Callable
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If None are found
        :return: True if found
        :rtype: bool
        """
        try:
            events = []
            start_time = time.time()
            while time.time() - start_time < timeout:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if condition(event):
                    return True
            raise TimeoutError("Timeout exceeded.")
        except (TimeoutError, queue.Empty) as err:
            raise RuntimeError(f"Never got an event that meets condition got [{events}]") from err

    def wait_for_array_indices_match(
        self, array: List[Any], match_indexes: List[int], timeout: int = 3
    ) -> bool:
        """Wait for an array to match certain indices.

        Wait `timeout` seconds for each fetch.

        :param array: Array to match against
        :type value: List[Any]
        :param match_indexes: the indexes concerned with matching
        :type timeout: int, optional
        :raises RuntimeError: If None are found
        :return: True if found
        :rtype: bool
        """
        try:
            events = []
            while True:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if len(event.attr_value.value) < len(match_indexes):
                    raise RuntimeError("Attribute list cannot be smaller than indexes of concern.")
                if all(event.attr_value.value[i] == array[i] for i in match_indexes):
                    return True
        except queue.Empty as err:
            ev_vals = self.extract_event_values(events)
            raise RuntimeError(
                f"Never got an event with subarray [{[array[i] for i in match_indexes]}] "
                f"got [{ev_vals}]."
            ) from err

    def _is_desired_event(  # pylint: disable=R0911
        self, event: tango.EventData, value: Any, approximate: bool
    ) -> bool:
        """Check if the event matches the value.

        :param event: event received
        :type event: tango.EventData
        :param value: value that is expected from the event data
        :type value: Any
        :return: If the event value matches expected value
        :rtype: bool
        """
        if not event.attr_value:
            return False

        if isinstance(event.attr_value.value, np.ndarray):
            if len(event.attr_value.value) == len(value):
                if (event.attr_value.value == value).all():
                    return True
                if np.isclose(event.attr_value.value, value).all():
                    return True
            return False

        if event.attr_value.value == value:
            return True
        if approximate and isclose(event.attr_value.value, value, rel_tol=1e-6):
            return True

        return False

    def wait_for_command_result(
        self, command_id: str, command_result: Any, timeout: int = 5
    ) -> Optional[bool]:
        """Wait for a long running command result.

        Wait `timeout` seconds for each fetch.

        :param command_id: The long running command ID
        :type command_id: str
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If none are found
        :return: The result of the long running command
        :rtype: str
        """
        try:
            while True:
                event = self._queue.get(timeout=timeout)
                if not event.attr_value:
                    continue
                if not isinstance(event.attr_value.value, tuple):
                    continue
                if len(event.attr_value.value) != 2:
                    continue
                (lrc_id, lrc_result) = event.attr_value.value
                if command_id == lrc_id and command_result == lrc_result:
                    return True
        except queue.Empty as err:
            raise RuntimeError(f"Never got an LRC result from command [{command_id}]") from err

    def wait_for_command_id(self, command_id: str, timeout: int = 5) -> List:
        """Wait for a long running command to complete.

        Wait `timeout` seconds for each fetch.

        :param command_id: The long running command ID
        :type command_id: str
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If none are found
        :return: The result of the long running command
        :rtype: str
        """
        events = []
        try:
            while True:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if not event.attr_value:
                    continue
                if not isinstance(event.attr_value.value, tuple):
                    continue
                if len(event.attr_value.value) != 2:
                    continue
                (lrc_id, _) = event.attr_value.value
                if command_id == lrc_id and event.attr_value.name == "longrunningcommandresult":
                    return events
        except queue.Empty as err:
            event_info = [(event.attr_value.name, event.attr_value.value) for event in events]
            raise RuntimeError(
                f"Never got an LRC result from command [{command_id}],",
                f" but got [{event_info}]",
            ) from err

    def wait_for_progress_update(self, progress_message: str, timeout: int = 5) -> List[Any]:
        """Wait for a long running command progress update.

        Wait `timeout` seconds for each fetch.

        :param progress_message: The progress message to wait for
        :type progress_message: str
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If none are found
        :return: The result of the long running command
        :rtype: str
        """
        events = []
        try:
            while True:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if not event.attr_value:
                    continue
                if not isinstance(event.attr_value.value, tuple):
                    continue
                progress_update = str(event.attr_value.value)
                if (
                    progress_message in progress_update
                    and event.attr_value.name == "longrunningcommandprogress"
                ):
                    return events
        except queue.Empty as err:
            event_info = [(event.attr_value.name, event.attr_value.value) for event in events]
            raise RuntimeError(
                f"Never got a progress update with [{progress_message}],",
                f" but got [{event_info}]",
            ) from err

    @classmethod
    def filter_id_events(
        cls, events: List[tango.EventData], unique_id: str
    ) -> List[tango.EventData]:
        """Filter out only events from unique_id.

        :param events: Events
        :type events: List[tango.EventData]
        :param unique_id: command ID
        :type unique_id: str
        :return: Filtered list of events
        :rtype: List[tango.EventData]
        """
        return [event for event in events if unique_id in str(event.attr_value.value)]

    def wait_for_n_events(self, event_count: int, timeout: int = 5) -> List[Any]:
        """Wait for N number of events.

        Wait `timeout` seconds for each fetch.

        :param event_count: The number of events to wait for
        :type command_id: int
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If none are found
        :return: The result of the long running command
        :rtype: List[Any]
        """
        events: List[Any] = []
        try:
            while len(events) != event_count:
                event = self._queue.get(timeout=timeout)
                events.append(event)
            return events
        except queue.Empty as err:
            raise RuntimeError(
                f"Did not get {event_count} events, ",
                f"got {len(events)} events",
            ) from err

    def clear_queue(self) -> None:
        """Clear out the queue."""
        while not self._queue.empty():
            self._queue.get()

    def get_queue_events(self, timeout: int = 3) -> List[Any]:
        """Get all the events out of the queue."""
        items = []
        try:
            while True:
                items.append(self._queue.get(timeout=timeout))
        except queue.Empty:
            return items

    @classmethod
    def extract_event_values(cls, events: List[tango.EventData]) -> List[Tuple[Any, Any, Any]]:
        """Get the values out of events.

        :param events: List of events
        :type events: List[tango.EventData]
        :return: List of value tuples
        :rtype: List[Tuple]
        """
        event_info = [
            (event.attr_value.name, event.attr_value.value, event.device) for event in events
        ]
        return event_info

    def get_queue_values(self, timeout: int = 3) -> List[Tuple[Any, Any]]:
        """Get the values from the queue."""
        items = []
        try:
            while True:
                event = self._queue.get(timeout=timeout)
                items.append((event.attr_value.name, event.attr_value.value))
        except queue.Empty:
            return items

    @classmethod
    def get_data_from_events(cls, events: List[tango.EventData]) -> List[Tuple]:
        """Retrieve the event info from the events.

        :param events: list of
        :type events: List[tango.EventData]
        """
        return [(event.attr_value.name, event.attr_value.value) for event in events]


class AlwaysContainsList:
    """Mock class to always return for __contains__."""

    def __contains__(self, item: Any) -> bool:
        """Contains method."""
        return True


class MethodCallsStore:
    """Store the calls to this method so it can be awaited."""

    def __init__(self) -> None:
        """Init the class."""
        self._queue: queue.Queue = queue.Queue()

    def __call__(self, *args: tuple, **kwargs: dict) -> None:
        """Store the kwargs used in calls to the MethodCallsStore class.

        :param kwargs: The method parameters
        :type kwargs: dict
        """
        if kwargs:
            self._queue.put(kwargs)
        if args:
            self._queue.put(args)

    def wait_for_kwargs(self, expected_kwargs: dict, timeout: int = 3) -> bool:
        """Wait for a specific dict to arrive.

        :param expected_kwargs: The kwargs we're expecting
        :type expected_kwargs: dict
        :param timeout: How long to wait, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: When the expected value is not fuond
        :return: Whether it was found or not
        :rtype: bool
        """
        try:
            queue_values = []
            while True:
                queue_kwargs = self._queue.get(timeout=timeout)
                queue_values.append(queue_kwargs)
                if queue_kwargs == expected_kwargs:
                    return True
        except queue.Empty as err:
            raise RuntimeError(f"Never got a {expected_kwargs}, but got {queue_values}") from err

    def wait_for_args(self, expected_args: list, timeout: int = 3) -> bool:
        """Wait for a specific arg list to arrive.

        :param expected_args: The kwargs we're expecting
        :type expected_args: dict
        :param timeout: How long to wait, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: When the expected value is not fuond
        :return: Whether it was found or not
        :rtype: bool
        """
        try:
            queue_values = []
            while True:
                queue_args = self._queue.get(timeout=timeout)
                queue_values.append(queue_args)
                if queue_args == expected_args:
                    return True
        except queue.Empty as err:
            raise RuntimeError(f"Never got a {expected_args}, but got {queue_values}") from err

    def clear_queue(self) -> None:
        """Clear out the queue."""
        while not self._queue.empty():
            self._queue.get()


def wait_until_condition_met(condition: Callable, fail_message: str, timeout: int = 10) -> None:
    """
    Wait until the given condition evaluates to True.

    :param condition: A callable that returns a boolean.
    :type condition: Callable
    :param fail_message: Message to display if the condition fails.
    :type fail_message: str
    :param timeout: Maximum time (in seconds) to wait for the condition.
    :type timeout: int
    :return: None
    """
    wait_until = time.time() + timeout
    while time.time() < wait_until:
        if condition():
            return
        time.sleep(1)
    pytest.fail(fail_message)
