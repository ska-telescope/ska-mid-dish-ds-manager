"""Fixtures for running ska-mid-dish-ds-manager acceptance tests."""

# mypy: disable-error-code="union-attr"
# mypy: disable-error-code="arg-type"
import logging
from typing import Any, Generator
from unittest.mock import Mock

import pytest
import tango
from ska_control_model import CommunicationStatus
from ska_tango_base.executor import TaskExecutorComponentManager

from ska_mid_dish_ds_manager.command_manager import CommandManager
from ska_mid_dish_ds_manager.models.dish_enums import DSOperatingMode, IndexerPosition
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager
from ska_mid_dish_ds_manager.opcua_control import OPCUAControl
from tests.utils import MethodCallsStore


@pytest.fixture(autouse=True)
def setup_and_teardown(event_store_class: Any, ds_manager_proxy: tango.DeviceProxy) -> Generator:
    """Reset the tango device to a fresh state before each test."""
    main_event_store = event_store_class()
    if ds_manager_proxy.operatingMode != DSOperatingMode.STANDBY_FP:
        ds_manager_proxy.subscribe_event(
            "operatingmode",
            tango.EventType.CHANGE_EVENT,
            main_event_store,
        )

        ds_manager_proxy.SetStandbyFPMode()
        main_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)

    if ds_manager_proxy.indexerposition != IndexerPosition.B1:
        ds_manager_proxy.subscribe_event(
            "indexerposition",
            tango.EventType.CHANGE_EVENT,
            main_event_store,
        )

        ds_manager_proxy.SetIndexPosition(1)
        main_event_store.wait_for_value(IndexerPosition.B1, timeout=60)

    if ds_manager_proxy.operatingMode != DSOperatingMode.STANDBY_LP:
        ds_manager_proxy.subscribe_event(
            "operatingmode",
            tango.EventType.CHANGE_EVENT,
            main_event_store,
        )

        ds_manager_proxy.SetStandbyLPMode()
        main_event_store.wait_for_value(DSOperatingMode.STANDBY_LP, timeout=6)

    yield


@pytest.fixture(scope="function")
def command_manager(ds_host: str, ds_port: int, ds_endpoint: str, ds_opcua_namespace: str) -> Any:
    """Create an instance of the command manager for reuse."""
    logger = Mock(spec=logging.Logger)

    scu = OPCUAControl(
        host=ds_host,
        port=ds_port,
        endpoint=ds_endpoint,
        namespace=ds_opcua_namespace,
        timeout=1.0,
    )
    scu.connect_and_setup()

    scu.take_authority("LMC")

    task_executor_cm = TaskExecutorComponentManager(logger=logger)
    com_manager = CommandManager(logger, scu, task_executor_cm.submit_task)

    yield com_manager

    com_manager.scu.disconnect_and_cleanup()  # type: ignore[union-attr]


@pytest.fixture(scope="function")
def component_manager_logger(
    ds_host: str, ds_port: int, ds_endpoint: str, ds_opcua_namespace: str
) -> Any:
    """Fixture that sets up a component manager for testing.

    :param ds_host: _description_
    :type ds_host: str
    :param ds_port: _description_
    :type ds_port: int
    :param ds_endpoint: _description_
    :type ds_endpoint: str
    :param ds_opcua_namespace: _description_
    :type ds_opcua_namespace: str
    :return: _description_
    :rtype: Tuple[OPCUAComponentManager, logging.Logger]
    :yield: _description_
    :rtype: Iterator[Tuple[OPCUAComponentManager, logging.Logger]]
    """
    logger = logging.getLogger(__name__)

    comm_state_callback = MethodCallsStore()

    cm = OPCUAComponentManager(
        ds_host,
        ds_port,
        ds_endpoint=ds_endpoint,
        ds_opcua_namespace=ds_opcua_namespace,
        logger=logger,
        communication_state_callback=comm_state_callback,
    )
    cm.start_communicating()

    # Wait for setup to complete

    comm_state_callback.wait_for_args(
        (CommunicationStatus.ESTABLISHED,),
        timeout=120,
    )

    yield cm
    cm.stop_communicating()
    comm_state_callback.wait_for_args(
        (CommunicationStatus.DISABLED,),
        timeout=30,
    )
