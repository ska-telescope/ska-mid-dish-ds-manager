"""Test the connections of DS Manager."""

import pytest
import tango

from ska_mid_dish_ds_manager.models.dish_enums import DSOperatingMode


@pytest.mark.acceptance
@pytest.mark.forked
def test_ds_manager_is_pingable(ds_manager_proxy: tango.DeviceProxy) -> None:
    """Checks if the DS Manager is reachable."""
    ds_manager_proxy.ping()


@pytest.mark.acceptance
@pytest.mark.forked
def test_ds_manager_id_111_in_standby_lp() -> None:
    """Checks if the DS Manager for ID 111 is in STANDBY_LP mode."""
    ds_device_fqdn = "mid-dish/ds-manager/SKA111"
    dp = tango.DeviceProxy(ds_device_fqdn)
    assert dp.operatingMode == DSOperatingMode.STANDBY_LP
