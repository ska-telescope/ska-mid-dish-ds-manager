"""Test StandbyLP."""

from typing import Any

import pytest
import tango

from ska_mid_dish_ds_manager.models.dish_enums import (
    DSCState,
    DSOperatingMode,
    DSPowerState,
    PointingState,
)


@pytest.mark.acceptance
@pytest.mark.forked
def test_standby_lp_transition(
    event_store_class: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test transition to Standby_LP."""
    operating_mode_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_event_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)

    ds_manager_proxy.SetStandbyLPMode()
    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_LP, timeout=6)

    assert ds_manager_proxy.dscState == DSCState.STANDBY
    assert ds_manager_proxy.powerState == DSPowerState.LOW_POWER
    assert ds_manager_proxy.operatingMode == DSOperatingMode.STANDBY_LP


@pytest.mark.acceptance
@pytest.mark.forked
def test_standby_lp_transition_from_stow_mode(
    event_store_class: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test transition to Standby_LP."""
    operating_mode_event_store = event_store_class()
    pointing_state_event_store = event_store_class()
    progress_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "pointingstate",
        tango.EventType.CHANGE_EVENT,
        pointing_state_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "longRunningCommandProgress",
        tango.EventType.CHANGE_EVENT,
        progress_event_store,
    )

    achieved_pointing = ds_manager_proxy.achievedPointing

    # If the current pointing elevation is far from the stow position,
    # then use Slew with a higher speed to get there faster
    if achieved_pointing[2] < 80:
        # az_pos, el_pos, speed_az, speed_el
        ds_manager_proxy.Slew([achieved_pointing[1], 80])

        pointing_state_event_store.wait_for_value(PointingState.SLEW, timeout=60)
        pointing_state_event_store.wait_for_value(PointingState.READY, timeout=60)

    ds_manager_proxy.Stow()
    operating_mode_event_store.wait_for_value(DSOperatingMode.STOW, timeout=60)

    ds_manager_proxy.SetStandbyLPMode()
    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_LP, timeout=6)

    progress_msg = "Path [Management.Commands.Stow] called with args [(True,)] from [Stow] command"
    progress_event_store.wait_for_progress_update(progress_msg, timeout=5)

    assert ds_manager_proxy.dscState == DSCState.STANDBY
    assert ds_manager_proxy.powerState == DSPowerState.LOW_POWER
    assert ds_manager_proxy.operatingMode == DSOperatingMode.STANDBY_LP
