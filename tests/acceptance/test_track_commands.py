"""Test Track and TrackStop commands."""

from random import uniform
from typing import Any

import pytest
import tango

from ska_mid_dish_ds_manager.models.constants import (
    MAX_AZIMUTH,
    MAX_ELEVATION_SCIENCE,
    MIN_AZIMUTH,
    MIN_ELEVATION_SCIENCE,
)
from ska_mid_dish_ds_manager.models.dish_enums import (
    DSCState,
    DSOperatingMode,
    DSPowerState,
    PointingState,
    TrackTableLoadMode,
)
from ska_mid_dish_ds_manager.utils import get_current_tai_timestamp

LONG_MOVE_TIMEOUT = 180
POINTING_TOLERANCE_DEG = 0.01


def convert_degrees_to_arcsec(angle_deg: float) -> float:
    """
    Convert angle in degrees to arcsecs.

    :param angle_deg: angle in degrees
    :return: angle in arcsecs
    """
    return angle_deg * 3600


@pytest.fixture
def check_and_move_into_valid_region(
    event_store_class: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Check if the dish is in a valid region for tracking. If not, slew to valid region."""
    main_event_store = event_store_class()
    ds_manager_proxy.subscribe_event(
        "pointingstate",
        tango.EventType.CHANGE_EVENT,
        main_event_store,
    )

    ds_manager_proxy.TrackStop()

    TRACKING_TOL_DEG = 20
    min_abs_azimuth_tracking = MIN_AZIMUTH + TRACKING_TOL_DEG
    max_abs_azimuth_tracking = MAX_AZIMUTH - TRACKING_TOL_DEG
    max_abs_elevation_tracking = MAX_ELEVATION_SCIENCE - TRACKING_TOL_DEG
    achieved_pointing = ds_manager_proxy.achievedPointing

    not_in_azimuth_tracking_region = abs(achieved_pointing[1]) > abs(max_abs_azimuth_tracking)
    above_elevation_tracking_limit = achieved_pointing[2] > max_abs_elevation_tracking
    below_elevation_tracking_limit = achieved_pointing[2] < (
        MIN_ELEVATION_SCIENCE + TRACKING_TOL_DEG
    )
    not_in_elevation_tracking = above_elevation_tracking_limit or below_elevation_tracking_limit
    if not_in_azimuth_tracking_region or not_in_elevation_tracking:
        ds_manager_proxy.Slew([min_abs_azimuth_tracking, max_abs_elevation_tracking])
        main_event_store.wait_for_value(PointingState.SLEW, timeout=10)
        main_event_store.wait_for_value(PointingState.READY, timeout=LONG_MOVE_TIMEOUT)


@pytest.mark.acceptance
@pytest.mark.forked
def test_track_and_trackstop(
    check_and_move_into_valid_region: Any,
    event_store_class: Any,
    ds_manager_proxy: tango.DeviceProxy,
) -> None:
    """Test Track and TrackStop commands."""
    main_event_store = event_store_class()
    achieved_pointing_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        main_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "pointingstate",
        tango.EventType.CHANGE_EVENT,
        main_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "achievedPointing",
        tango.EventType.CHANGE_EVENT,
        achieved_pointing_event_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    main_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)

    # According to the command fan-out:
    # https://confluence.skatelescope.org/pages/viewpage.action?pageId=188656205
    # Set point mode doesn't make any change to DS besides activating the axis which already
    # happens in SetStandbyFPMode. Therefore, SetPointMode only changes the operatingMode
    # attribute value. We're implementing the testing to follow the requirement pending
    # follow up on the test procedure.
    # TODO: Follow up
    ds_manager_proxy.SetPointMode()
    main_event_store.wait_for_value(DSOperatingMode.POINT, timeout=60)

    current_pointing = ds_manager_proxy.achievedPointing
    current_az = current_pointing[1]
    current_el = current_pointing[2]

    current_time_tai_s = get_current_tai_timestamp()

    # Directions to move values
    az_dir = 1 if current_az < 250 else -1
    el_dir = 1 if current_el < 80 else -1

    track_table = [
        TrackTableLoadMode.NEW,
        5,  # Number of entries
        current_time_tai_s + 5,
        current_az + 1 * az_dir,
        current_el + 1 * el_dir,
        current_time_tai_s + 7,
        current_az + 2 * az_dir,
        current_el + 2 * el_dir,
        current_time_tai_s + 9,
        current_az + 3 * az_dir,
        current_el + 3 * el_dir,
        current_time_tai_s + 11,
        current_az + 4 * az_dir,
        current_el + 4 * el_dir,
        current_time_tai_s + 13,
        current_az + 5 * az_dir,
        current_el + 5 * el_dir,
    ]
    final_table_entry = track_table[-3:]

    ds_manager_proxy.TrackLoadTable(track_table)

    ds_manager_proxy.Track()
    main_event_store.wait_for_value(PointingState.SLEW, timeout=60)
    main_event_store.wait_for_value(PointingState.TRACK, timeout=60)

    assert ds_manager_proxy.operatingMode == DSOperatingMode.POINT
    assert ds_manager_proxy.powerState == DSPowerState.FULL_POWER
    assert ds_manager_proxy.pointingState == PointingState.TRACK

    main_event_store.clear_queue()

    def check_final_points_reached(value: Any) -> bool:
        return (
            abs(value[1] - final_table_entry[1]) < POINTING_TOLERANCE_DEG
            and abs(value[2] - final_table_entry[2]) < POINTING_TOLERANCE_DEG
        )

    achieved_pointing_event_store.clear_queue()
    achieved_pointing_event_store.wait_for_condition(check_final_points_reached, timeout=120)

    ds_manager_proxy.TrackStop()
    main_event_store.wait_for_value(PointingState.READY, timeout=60)

    assert ds_manager_proxy.dscState == DSCState.STANDSTILL
    assert ds_manager_proxy.powerState == DSPowerState.FULL_POWER
    assert ds_manager_proxy.operatingMode == DSOperatingMode.POINT
    assert ds_manager_proxy.pointingState == PointingState.READY


@pytest.mark.acceptance
@pytest.mark.forked
def test_track_and_append(
    check_and_move_into_valid_region: Any,
    event_store_class: Any,
    ds_manager_proxy: tango.DeviceProxy,
) -> None:
    """Test TrackLoadTable with Append while Tracking."""
    main_event_store = event_store_class()
    achieved_pointing_event_store = event_store_class()

    ON_SOURCE_THRESHOLD_ARCSEC = convert_degrees_to_arcsec(POINTING_TOLERANCE_DEG)
    THRESHOLD_TIME_PERIOD = 0.1
    ds_manager_proxy.configureTargetLock = [ON_SOURCE_THRESHOLD_ARCSEC, THRESHOLD_TIME_PERIOD]

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        main_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "pointingstate",
        tango.EventType.CHANGE_EVENT,
        main_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "achievedPointing",
        tango.EventType.CHANGE_EVENT,
        achieved_pointing_event_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    main_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)

    ds_manager_proxy.SetPointMode()
    main_event_store.wait_for_value(DSOperatingMode.POINT, timeout=60)

    current_pointing = ds_manager_proxy.achievedPointing
    current_az = current_pointing[1]
    current_el = current_pointing[2]

    current_time_tai_s = get_current_tai_timestamp()

    # Directions to move values
    az_dir = 1 if current_az < 250 else -1
    el_dir = 1 if current_el < 80 else -1

    track_table = [
        TrackTableLoadMode.NEW,
        5,  # Number of entries
        current_time_tai_s + 5,
        current_az + 2 * az_dir,
        current_el + 2 * el_dir,
        current_time_tai_s + 7,
        current_az + 3 * az_dir,
        current_el + 3 * el_dir,
        current_time_tai_s + 9,
        current_az + 4 * az_dir,
        current_el + 4 * el_dir,
        current_time_tai_s + 11,
        current_az + 5 * az_dir,
        current_el + 5 * el_dir,
        current_time_tai_s + 13,
        current_az + 6 * az_dir,
        current_el + 6 * el_dir,
    ]

    ds_manager_proxy.TrackLoadTable(track_table)

    ds_manager_proxy.Track()
    main_event_store.wait_for_value(PointingState.SLEW, timeout=60)
    # TODO flesh out the achievedTargetLock tests a bit
    # assert ds_manager_proxy.achievedTargetLock is False
    main_event_store.wait_for_value(PointingState.TRACK, timeout=60)

    assert ds_manager_proxy.operatingMode == DSOperatingMode.POINT
    assert ds_manager_proxy.powerState == DSPowerState.FULL_POWER
    assert ds_manager_proxy.pointingState == PointingState.TRACK

    main_event_store.clear_queue()

    track_table_additional = [
        TrackTableLoadMode.APPEND,
        5,  # Number of entries
        current_time_tai_s + 15,
        current_az + 6 * az_dir,
        current_el + 6 * el_dir,
        current_time_tai_s + 17,
        current_az + 7 * az_dir,
        current_el + 7 * el_dir,
        current_time_tai_s + 19,
        current_az + 8 * az_dir,
        current_el + 8 * el_dir,
        current_time_tai_s + 21,
        current_az + 9 * az_dir,
        current_el + 9 * el_dir,
        current_time_tai_s + 23,
        current_az + 10 * az_dir,
        current_el + 10 * el_dir,
    ]
    ds_manager_proxy.TrackLoadTable(track_table_additional)

    final_table_entry = track_table_additional[-3:]

    def check_final_points_reached(value: Any) -> bool:
        return (
            abs(value[1] - final_table_entry[1]) < POINTING_TOLERANCE_DEG
            and abs(value[2] - final_table_entry[2]) < POINTING_TOLERANCE_DEG
        )

    achieved_pointing_event_store.clear_queue()
    achieved_pointing_event_store.wait_for_condition(check_final_points_reached, timeout=60)

    ds_manager_proxy.TrackStop()
    main_event_store.wait_for_value(PointingState.READY, timeout=30)

    assert ds_manager_proxy.dscState == DSCState.STANDSTILL
    assert ds_manager_proxy.powerState == DSPowerState.FULL_POWER
    assert ds_manager_proxy.operatingMode == DSOperatingMode.POINT
    assert ds_manager_proxy.pointingState == PointingState.READY

    assert ds_manager_proxy.achievedTargetLock is True


@pytest.mark.acceptance
@pytest.mark.forked
def test_track_load_static_offset(
    event_store_class: Any,
    ds_manager_proxy: tango.DeviceProxy,
) -> None:
    """Test TrackLoadStaticOff command."""
    static_offset_el_event_store = event_store_class()
    static_offset_xel_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "actStaticOffsetValueEl",
        tango.EventType.CHANGE_EVENT,
        static_offset_el_event_store,
    )
    ds_manager_proxy.subscribe_event(
        "actStaticOffsetValueXel",
        tango.EventType.CHANGE_EVENT,
        static_offset_xel_event_store,
    )

    new_el_offset = uniform(0, 1) * 3600
    new_xel_offset = uniform(0, 1) * 3600

    ds_manager_proxy.TrackLoadStaticOff([new_xel_offset, new_el_offset])

    static_offset_el_event_store.wait_for_value(new_el_offset, timeout=4, approximate=True)
    static_offset_xel_event_store.wait_for_value(new_xel_offset, timeout=4, approximate=True)


@pytest.mark.acceptance
@pytest.mark.forked
def test_track_load_fill_capacity(
    check_and_move_into_valid_region: Any,
    event_store_class: Any,
    ds_manager_proxy: tango.DeviceProxy,
) -> None:
    """Test filling the track table."""
    main_event_store = event_store_class()
    achieved_pointing_event_store = event_store_class()
    current_index_event_store = event_store_class()
    end_index_event_store = event_store_class()

    ON_SOURCE_THRESHOLD_ARCSEC = convert_degrees_to_arcsec(POINTING_TOLERANCE_DEG)
    THRESHOLD_TIME_PERIOD = 0.1
    ds_manager_proxy.configureTargetLock = [ON_SOURCE_THRESHOLD_ARCSEC, THRESHOLD_TIME_PERIOD]

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        main_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "pointingstate",
        tango.EventType.CHANGE_EVENT,
        main_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "achievedPointing",
        tango.EventType.CHANGE_EVENT,
        achieved_pointing_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "trackTableCurrentIndex",
        tango.EventType.CHANGE_EVENT,
        current_index_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "trackTableEndIndex",
        tango.EventType.CHANGE_EVENT,
        end_index_event_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    main_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)

    ds_manager_proxy.SetPointMode()
    main_event_store.wait_for_value(DSOperatingMode.POINT, timeout=60)

    current_pointing = ds_manager_proxy.achievedPointing
    current_az = current_pointing[1]
    current_el = current_pointing[2]

    current_time_tai_s = get_current_tai_timestamp()

    # build NEW track table with constant azimuth and elevation
    table_entries = 50
    lead_time_s = 10
    block_duration = 10
    point_separation_s = block_duration / table_entries
    track_table = [TrackTableLoadMode.NEW, table_entries]
    track_table_timestamp = current_time_tai_s + lead_time_s
    for _ in range(table_entries):
        row = [track_table_timestamp, current_az, current_el]
        track_table.extend(row)
        track_table_timestamp += point_separation_s
    # load NEW track table
    ds_manager_proxy.TrackLoadTable(track_table)

    current_index_event_store.wait_for_value(0, timeout=10)
    expected_end_index = table_entries - 1
    end_index_event_store.wait_for_value(expected_end_index, timeout=10)

    current_index_event_store.clear_queue()
    end_index_event_store.clear_queue()

    # build and APPEND to track table to fill buffer
    track_table_maximum_points = 10000
    load_blocks = int(track_table_maximum_points / table_entries)
    # need 1 less than load_blocks because first block sent with NEW
    for _ in range(load_blocks - 1):
        # use timestamp from last block as reference for start of new block
        track_table_timestamp = track_table[-3] + point_separation_s
        # start new track table construction
        track_table = [TrackTableLoadMode.APPEND, table_entries]
        for _ in range(table_entries):
            row = [track_table_timestamp, current_az, current_el]
            track_table.extend(row)
            track_table_timestamp += point_separation_s
        # load track table with APPEND
        ds_manager_proxy.TrackLoadTable(track_table)
        # check that the end index updates as expected
        expected_end_index += table_entries
        end_index_event_store.wait_for_value(expected_end_index)

    # start tracking and confirm indexes move accordingly
    ds_manager_proxy.Track()
    # wait for some index updates to confirm tracking
    current_index_event_store.wait_for_n_events(100, timeout=20)
    assert ds_manager_proxy.trackTableCurrentIndex > 0
    ds_manager_proxy.TrackStop()
