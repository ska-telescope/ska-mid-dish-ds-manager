"""Test indexer position."""

from typing import Any

import pytest
import tango

from ska_mid_dish_ds_manager.models.dish_enums import DSOperatingMode, IndexerPosition


@pytest.mark.acceptance
@pytest.mark.forked
def test_indexer_position_changes(
    event_store_class: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test if indexer position changes."""
    operating_mode_event_store = event_store_class()
    indexer_position_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "indexerposition",
        tango.EventType.CHANGE_EVENT,
        indexer_position_event_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_FP)

    ds_manager_proxy.SetIndexPosition(2)

    indexer_position_event_store.wait_for_value(IndexerPosition.MOVING, timeout=60)
    indexer_position_event_store.wait_for_value(IndexerPosition.B2, timeout=60)

    assert ds_manager_proxy.indexerposition == IndexerPosition.B2
