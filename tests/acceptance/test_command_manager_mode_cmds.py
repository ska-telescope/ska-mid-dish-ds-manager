# mypy: disable-error-code="union-attr"
"""Module containing mode tests for command manager ."""
import queue
from unittest.mock import MagicMock, Mock

import pytest
from ska_control_model import TaskStatus

from ska_mid_dish_ds_manager.command_manager import CommandManager
from ska_mid_dish_ds_manager.models.dish_enums import DSCState
from tests.utils import MethodCallsStore


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_stow(
    command_manager: CommandManager,
) -> None:
    """Test that the stow() propagate to the PLC."""
    # Make sure first that the antenna is not stowed.
    dsc_state = command_manager.scu.attributes["Management.Status.DscState"].value
    assert dsc_state != DSCState.STOWED.value

    # DSCState informs us about the movement state of the antenna.
    attr_monit_queue: queue.Queue = queue.Queue()
    command_manager.scu.subscribe("Management.Status.DscState", 100, attr_monit_queue)

    # When you subscribe, you immediate get data from the PLC,
    # Therefore clearing the queue ensures we only have data in
    # the queue after calling stow().
    dsc_state = attr_monit_queue.get(timeout=3)
    assert dsc_state["value"] == DSCState.STANDBY.value

    # Mock out task callback
    task_callback_store = MethodCallsStore()
    command_manager.stow(task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs({"status": TaskStatus.IN_PROGRESS})
    task_callback_store.wait_for_kwargs({"status": TaskStatus.COMPLETED, "result": "Stow Called"})

    # When the stow is called from the antenna moves this way:
    # Standby -> Slew -> Stowed
    dsc_state_data_event = attr_monit_queue.get(timeout=30)
    new_dsc_state_val = dsc_state_data_event["value"]

    assert (
        new_dsc_state_val == DSCState.SLEW.value
    ), f"Expected {DSCState.SLEW.value}, got {new_dsc_state_val}"

    dsc_state_data_event = attr_monit_queue.get(timeout=100)
    new_dsc_state_val = dsc_state_data_event["value"]
    assert (
        new_dsc_state_val == DSCState.STOWED.value
    ), f"Expected {DSCState.STOWED.value}, got {new_dsc_state_val}"

    # When stow is complete, the DSCState goes to stowed.
    # This check verifies the PLC Current DSCState
    new_dsc_state = command_manager.scu.attributes["Management.Status.DscState"].value
    assert (
        new_dsc_state == DSCState.STOWED.value
    ), f"Expected {DSCState.STOWED.value}, got {new_dsc_state}"


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_set_standby_lp(
    command_manager: CommandManager,
) -> None:
    """Test that the set_standby_lp() propagate to the PLC."""
    # LowpowerActive opcua node informs us whether the dish is in LP or FP
    attr_monit_queue: queue.Queue = queue.Queue()
    task_callback_store = MethodCallsStore()
    command_manager.scu.subscribe("Management.PowerStatus.LowPowerActive", 100, attr_monit_queue)
    # The antenna starts at low power
    low_power_state = attr_monit_queue.get(timeout=3)
    assert low_power_state["value"] is True

    # force it to full power first
    command_manager.set_standby_fp_mode(
        {"dscpowerlimitkw": 15.0}, task_callback=task_callback_store
    )
    low_power_state = attr_monit_queue.get(timeout=60)
    assert low_power_state["value"] is False

    # Now invoke the standby_lp and test it
    gen_component_state = {"dscpowerlimitkw": 10.0}
    command_manager.set_standby_lp_mode(gen_component_state, task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs(
        {
            "status": TaskStatus.IN_PROGRESS,
            "progress": "Low Power Mode called using DSC Power Limit: 10.0kW",
        }
    )
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "SetStandbyLPMode Called"}
    )
    low_power_state = attr_monit_queue.get(timeout=30)
    assert low_power_state["value"] is True
    # When standby_lp is complete, the DSCState goes to standby.
    new_dsc_state = command_manager.scu.attributes["Management.Status.DscState"].value
    assert new_dsc_state == DSCState.STANDBY.value
    assert command_manager.scu.attributes["Management.PowerStatus.LowPowerActive"].value is True


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_set_standby_fp(
    command_manager: CommandManager,
) -> None:
    """Test that the set_standby_fp() propagate to the PLC."""
    # LowpowerActive opcua node informs us whether the dish is in LP or FP
    attr_monit_queue: queue.Queue = queue.Queue()
    command_manager.scu.subscribe("Management.PowerStatus.LowPowerActive", 100, attr_monit_queue)
    # Make sure first that the antenna is not on full power
    low_power_state = attr_monit_queue.get(timeout=3)
    assert low_power_state["value"] is True

    task_callback_store = MethodCallsStore()
    gen_component_state = {"dscpowerlimitkw": 10.0}
    command_manager.set_standby_fp_mode(gen_component_state, task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs(
        {
            "status": TaskStatus.IN_PROGRESS,
            "progress": "Full Power Mode called using DSC Power Limit: 10.0kW",
        }
    )
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "SetStandbyFPMode Called"}
    )
    # When standby_fp is complete, the LowPowerActive goes to False.
    low_power_state = attr_monit_queue.get(timeout=30)
    assert low_power_state["value"] is False
    new_dsc_state = command_manager.scu.attributes["Management.Status.DscState"].value
    assert (
        new_dsc_state == DSCState.STANDBY.value
    ), f"Expected {DSCState.STANDBY.value}, got {new_dsc_state}"
    assert command_manager.scu.attributes["Management.PowerStatus.LowPowerActive"].value is False


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_set_point_mode(
    command_manager: CommandManager,
) -> None:
    """Test that the set_point_mode() propagate to the PLC."""
    # When you subscribe, you immediate get data from the PLC,
    # Therefore clearing the queue ensures we only have data in
    # the queue after calling point().
    attr_monit_queue: queue.Queue = queue.Queue()
    command_manager.scu.subscribe("Management.Status.DscState", 100, attr_monit_queue)
    dsc_state = attr_monit_queue.get(timeout=3)
    assert dsc_state["value"] == DSCState.STANDBY.value

    # Set to full power
    command_manager.scu.subscribe("Management.PowerStatus.LowPowerActive", 100, attr_monit_queue)
    dsc_state = attr_monit_queue.get(timeout=3)
    assert dsc_state["value"]

    command_manager.set_power_mode(Mock(), [False, 10.0])

    dsc_state = attr_monit_queue.get(timeout=3)
    assert not dsc_state["value"]

    # Set point mode
    task_callback_store = MethodCallsStore()

    command_manager.set_point_mode(task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs({"status": TaskStatus.IN_PROGRESS})
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "SetPointMode Called"}
    )
    command_manager.scu.subscribe("Management.Status.DscState", 100, attr_monit_queue)
    dsc_state = attr_monit_queue.get(timeout=30)
    # PLC has Activating state, this 'if' is so that this test works with Karoo sim which doesn't
    # have the Activating state
    if dsc_state["value"] == DSCState.ACTIVATING.value:
        dsc_state = attr_monit_queue.get(timeout=30)
    assert dsc_state["value"] == DSCState.STANDSTILL.value
    new_dsc_state = command_manager.scu.attributes["Management.Status.DscState"].value
    assert new_dsc_state == DSCState.STANDSTILL.value


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_set_power_mode(
    command_manager: CommandManager,
) -> None:
    """Test that the set_power_mode() propagate to the PLC."""
    attr_monit_queue: queue.Queue = queue.Queue()
    command_manager.scu.subscribe("Management.PowerStatus.LowPowerActive", 100, attr_monit_queue)
    # Make sure first that the dish is on low power
    low_power_state = attr_monit_queue.get(timeout=3)
    assert low_power_state["value"] is True

    # Then go, ahead and call set_power_mode
    task_callback_store = MethodCallsStore()
    # Mockout the _update_component_state since we do start the tango device.
    mock_update_comp_state_update = MagicMock(return_value=None)
    command_manager.set_power_mode(
        mock_update_comp_state_update, [0.0, 15.0], task_callback=task_callback_store
    )

    task_callback_store.wait_for_kwargs({"status": TaskStatus.IN_PROGRESS})
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "SetPowerMode Called"}
    )
    low_power_state = attr_monit_queue.get(timeout=30)
    assert low_power_state["value"] is False
    low_power_active = command_manager.scu.attributes[
        "Management.PowerStatus.LowPowerActive"
    ].value
    assert low_power_active is False


@pytest.mark.acceptance
@pytest.mark.forked
@pytest.mark.skip("set_maintenance_mode not implemented")
def test_command_manager_set_maintenance_mode(
    command_manager: CommandManager,
) -> None:
    """Test that the set_maintenance_mode() propagate to the PLC."""
    task_callback_store = MethodCallsStore()

    command_manager.set_maintenance_mode(task_callback=task_callback_store)

    # set_maintenance_mode logic is not implemented yet.
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "SetMaintenanceMode Called"}
    )
    assert False, "TODO Add the check"
