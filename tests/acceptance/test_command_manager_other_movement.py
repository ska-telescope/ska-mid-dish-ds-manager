# mypy: disable-error-code="union-attr"
"""Module containing movement tests for command manager ."""
import queue
from unittest.mock import Mock

import pytest
from ska_control_model import TaskStatus

from ska_mid_dish_ds_manager.command_manager import CommandManager
from ska_mid_dish_ds_manager.models.dish_enums import DSCState, IndexerPosition
from tests.utils import MethodCallsStore


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_set_index_position(
    command_manager: CommandManager,
) -> None:
    """Test that the set_index_position() propagate to the PLC."""
    task_callback_store = MethodCallsStore()

    attr_monit_queue: queue.Queue = queue.Queue()
    # Set DSC to full power
    command_manager.scu.subscribe("Management.PowerStatus.LowPowerActive", 100, attr_monit_queue)
    dsc_state = attr_monit_queue.get(timeout=3)
    assert dsc_state["value"]

    command_manager.set_power_mode(Mock(), [False, 10.0])

    dsc_state = attr_monit_queue.get(timeout=3)
    assert not dsc_state["value"]

    command_manager.scu.subscribe("Management.Status.FiPos", 100, attr_monit_queue)
    # Make sure first that the Fipos is not on B1
    fi_pos = attr_monit_queue.get(timeout=3)
    assert fi_pos["value"] == 1  # ua.BandType.Band_1

    command_manager.set_index_position(IndexerPosition.B2, task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs({"status": TaskStatus.QUEUED})
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "SetIndexPosition Called"}
    )
    # Moves to unknown when the feed indexer is not in position, only on the PLC
    fi_pos = attr_monit_queue.get(timeout=30)
    if fi_pos["value"] == 8:  # ua.BandType.Unknown
        fi_pos = attr_monit_queue.get(timeout=60)
    # When set_index_position is complete, the FiPos should be in requested band.
    assert fi_pos["value"] == 2  # ua.BandType.Band_2


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_track_load_static_off(
    command_manager: CommandManager,
) -> None:
    """Test that the set_index_position() propagate to the PLC."""
    task_callback_store = MethodCallsStore()
    command_manager.track_load_static_off([1.0, 2.0], task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs({"status": TaskStatus.QUEUED})
    task_callback_store.wait_for_kwargs({"status": TaskStatus.IN_PROGRESS})
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "TrackLoadStaticOff Called"}
    )

    new_el_offset = command_manager.scu.attributes["Tracking.Status.act_statOffset_value_El"].value
    assert new_el_offset == 2.0
    new_xel_offset = command_manager.scu.attributes[
        "Tracking.Status.act_statOffset_value_Xel"
    ].value
    assert new_xel_offset == 1.0


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_slew(
    command_manager: CommandManager,
) -> None:
    """Test that the slew() propagate to the PLC."""
    # Ensure that the intial DSCState is not SLEW
    attr_monit_queue: queue.Queue = queue.Queue()
    command_manager.scu.subscribe("Management.Status.DscState", 100, attr_monit_queue)
    dsc_state = attr_monit_queue.get(timeout=3)
    assert dsc_state["value"] == DSCState.STANDBY.value

    # Now call slew
    task_callback_store = MethodCallsStore()
    gen_comp_state = {"dscpowerlimitkw": 10.0}
    command_manager.slew(gen_comp_state, [45, 70], 1.0, 1.0, task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs({"status": TaskStatus.QUEUED})

    task_callback_store.wait_for_kwargs(
        {
            "status": TaskStatus.IN_PROGRESS,
            "progress": "Slew called with Azimuth speed: 1.0 deg/s, "
            "Elevation speed: 1.0 deg/s and DSC Power Limit: 10.0kW",
        }
    )
    task_callback_store.wait_for_kwargs({"status": TaskStatus.COMPLETED, "result": "Slew Called"})
    # When slewing the PLC DSCState goes to standstill first and then slew
    dsc_state = attr_monit_queue.get(timeout=30)
    assert dsc_state["value"] == DSCState.STANDSTILL.value

    dsc_state = attr_monit_queue.get(timeout=60)
    assert dsc_state["value"] == DSCState.SLEW.value
    new_dsc_state = command_manager.scu.attributes["Management.Status.DscState"].value
    assert new_dsc_state == DSCState.SLEW.value
    assert command_manager.scu.attributes["Management.PowerStatus.LowPowerActive"].value is False

    dsc_state = attr_monit_queue.get(timeout=60)
    assert dsc_state["value"] == DSCState.STANDSTILL.value
