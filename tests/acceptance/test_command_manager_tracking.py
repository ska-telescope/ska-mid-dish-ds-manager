# mypy: disable-error-code="union-attr"
"""Module containing tracking tests for command manager ."""
import queue

import pytest
from ska_control_model import ResultCode, TaskStatus

from ska_mid_dish_ds_manager.command_manager import CommandManager
from ska_mid_dish_ds_manager.models.dish_enums import (
    DSCState,
    TrackInterpolationMode,
    TrackTableLoadMode,
)
from ska_mid_dish_ds_manager.utils import get_current_tai_timestamp
from tests.utils import MethodCallsStore


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_trackload_table(
    command_manager: CommandManager,
) -> None:
    """Test that the track_load_table() propagate to the PLC."""
    current_az = command_manager.scu.attributes["Azimuth.Status.p_Act"].value
    current_el = command_manager.scu.attributes["Elevation.Status.p_Act"].value
    # Now set the track table
    current_time_tai_s = get_current_tai_timestamp()

    # Directions to move values
    az_dir = 1 if current_az < 250 else -1
    el_dir = 1 if current_el < 80 else -1

    track_table = [
        TrackTableLoadMode.NEW,
        5,  # Number of entries
        current_time_tai_s + 5,
        current_az + 1 * az_dir,
        current_el + 1 * el_dir,
        current_time_tai_s + 7,
        current_az + 2 * az_dir,
        current_el + 2 * el_dir,
        current_time_tai_s + 9,
        current_az + 3 * az_dir,
        current_el + 3 * el_dir,
        current_time_tai_s + 11,
        current_az + 4 * az_dir,
        current_el + 4 * el_dir,
        current_time_tai_s + 13,
        current_az + 5 * az_dir,
        current_el + 5 * el_dir,
    ]

    result_code, msg = command_manager.track_load_table(track_table)
    assert result_code == ResultCode.OK, msg
    assert msg == "TrackLoadTable successfully triggered the DS Controller"


@pytest.mark.acceptance
@pytest.mark.forked
def test_command_manager_track_start_and_stop(
    command_manager: CommandManager,
) -> None:
    """Test that the track() and trackstop propagate to the PLC."""
    current_az = command_manager.scu.attributes["Azimuth.Status.p_Act"].value
    current_el = command_manager.scu.attributes["Elevation.Status.p_Act"].value
    # Now set the track table
    current_time_tai_s = get_current_tai_timestamp()

    # Directions to move values
    az_dir = 1 if current_az < 250 else -1
    el_dir = 1 if current_el < 80 else -1

    track_table = [
        TrackTableLoadMode.NEW,
        5,  # Number of entries
        current_time_tai_s + 5,
        current_az + 1 * az_dir,
        current_el + 1 * el_dir,
        current_time_tai_s + 7,
        current_az + 2 * az_dir,
        current_el + 2 * el_dir,
        current_time_tai_s + 9,
        current_az + 3 * az_dir,
        current_el + 3 * el_dir,
        current_time_tai_s + 11,
        current_az + 4 * az_dir,
        current_el + 4 * el_dir,
        current_time_tai_s + 13,
        current_az + 5 * az_dir,
        current_el + 5 * el_dir,
    ]

    command_manager.track_load_table(track_table)

    # Ensure that the intial DSCState is not TRACK
    attr_monit_queue: queue.Queue = queue.Queue()
    command_manager.scu.subscribe("Management.Status.DscState", 100, attr_monit_queue)
    dsc_state = attr_monit_queue.get(timeout=3)
    assert dsc_state["value"] in (DSCState.STANDBY.value, DSCState.STANDSTILL.value)

    # Set the dish to standbyFP
    task_callback_store = MethodCallsStore()
    another_mon_queue: queue.Queue = queue.Queue()
    command_manager.scu.subscribe("Management.PowerStatus.LowPowerActive", 100, another_mon_queue)

    command_manager.set_standby_fp_mode(
        {"dscpowerlimitkw": 15.0}, task_callback=task_callback_store
    )
    another_mon_queue.get(timeout=3)

    low_power = another_mon_queue.get(timeout=60)
    assert low_power["value"] is False

    # Now invoke track
    command_manager.track(TrackInterpolationMode.NEWTON, task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs({"status": TaskStatus.QUEUED})
    task_callback_store.wait_for_kwargs({"status": TaskStatus.IN_PROGRESS})
    task_callback_store.wait_for_kwargs({"status": TaskStatus.COMPLETED, "result": "Track Called"})

    dsc_state = attr_monit_queue.get(timeout=100)
    assert dsc_state["value"] == DSCState.STANDSTILL.value

    # Invoke Track Stop
    task_callback_store = MethodCallsStore()
    command_manager.track_stop(task_callback=task_callback_store)
    task_callback_store.wait_for_kwargs({"status": TaskStatus.QUEUED})
    task_callback_store.wait_for_kwargs({"status": TaskStatus.IN_PROGRESS})
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "TrackStop Called"}
    )

    dsc_state = command_manager.scu.attributes["Management.Status.DscState"].value
    assert dsc_state == DSCState.STANDSTILL.value
