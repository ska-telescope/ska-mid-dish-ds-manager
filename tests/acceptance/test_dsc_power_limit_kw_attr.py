"""Test dscPowerLimitkW attribute."""

import pytest
import tango


@pytest.mark.acceptance
@pytest.mark.forked
def test_dscpowerlimitkw_correct(ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test dscPowerLimitKw attribute (Correct)."""
    power_limit = 12.5
    ds_manager_proxy.write_attribute("dscPowerLimitkW", power_limit)
    assert power_limit == ds_manager_proxy.read_attribute("dscPowerLimitkW").value


@pytest.mark.acceptance
@pytest.mark.forked
def test_dscpowerlimitkw_incorrect(ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test dscPowerLimitKw attribute (Incorrect)."""
    power_limit = 7.99
    with pytest.raises(tango.DevFailed):
        ds_manager_proxy.write_attribute("dscPowerLimitkW", power_limit)
        current_power_value = ds_manager_proxy.read_attribute("dscPowerLimitkW").value
        assert current_power_value != power_limit
