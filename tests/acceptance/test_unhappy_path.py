"""Test dish unhappy path."""

from collections.abc import Generator
from typing import Any

import pytest
import tango
from asyncua.sync import Client
from ska_control_model import ResultCode


@pytest.fixture
def reset_track_table_on_opcua_server(ds_manager_proxy: tango.DeviceProxy) -> Generator:
    """
    Reset track table to enable exception trigger.

    manual test run output:
    https://gitlab.com/ska-telescope/ska-mid-dish-ds-manager/-/merge_requests/25
    """
    ds_sim_fqdn = ds_manager_proxy.get_property("DSCFqdn")
    ds_sim_fqdn = ds_sim_fqdn.get("DSCFqdn")[0]

    client = Client(url=ds_sim_fqdn)
    client.connect()
    client.load_data_type_definitions()

    ds_opc_ua_namespace = ds_manager_proxy.get_property("DSOPCUANamespace")
    ds_opc_ua_namespace = ds_opc_ua_namespace.get("DSOPCUANamespace")[0]

    idx = client.get_namespace_index(uri=ds_opc_ua_namespace)

    # Reading Tracking Node
    plc_prg = client.nodes.root.get_child(
        [
            "0:Objects",
            f"{idx}:Logic",
            f"{idx}:Application",
            f"{idx}:PLC_PRG",
        ]
    )

    tracking = plc_prg.get_child([f"{idx}:Tracking"])
    # Intentionally raise an error from DS opc ua device.
    tracking.call_method(f"{idx}:TrackLoadTable", 0, 0, None)
    yield
    tracking.call_method(f"{idx}:TrackLoadTable", 0, 0, [10.0, 20.0, 30.0])


@pytest.mark.acceptance
@pytest.mark.forked
@pytest.mark.xfail(
    reason="Not able to connect to the OPCUA service from the test runner pod in k8s-tests"
)
def test_ds_handles_unhappy_path_in_command_execution(
    event_store_class: Any,
    ds_manager_proxy: tango.DeviceProxy,
    reset_track_table_on_opcua_server: Generator,
) -> None:
    """Test unhappy path using Track."""
    status_event_store = event_store_class()
    progress_event_store = event_store_class()
    result_event_store = event_store_class()

    # Tango subcription to LRC Progress
    ds_manager_proxy.subscribe_event(
        "longRunningCommandProgress",
        tango.EventType.CHANGE_EVENT,
        progress_event_store,
    )

    # Tango subcription to LRC Result
    ds_manager_proxy.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        result_event_store,
    )

    # Tango subcription to LRC Status
    ds_manager_proxy.subscribe_event(
        "longRunningCommandStatus",
        tango.EventType.CHANGE_EVENT,
        status_event_store,
    )

    # Calling track
    [[_], [unique_id]] = ds_manager_proxy.Track()

    result_event_store.wait_for_command_id(unique_id, timeout=5)

    progress_msg = "TrackStart method called with args"
    progress_event_store.wait_for_progress_update(progress_msg, timeout=5)

    status_event_store = status_event_store.get_queue_values(timeout=5)
    # Filter out event values
    status_event_store = [event_value[1] for event_value in status_event_store]
    # Unpack the list of tuples into a single list.
    status_event_store = [evt_val for tpl in status_event_store for evt_val in tpl]
    # Join events into one string
    joined_event_store = ", ".join([evnts for evnts in status_event_store])

    assert f"{unique_id}, FAILED" in joined_event_store


@pytest.mark.acceptance
@pytest.mark.forked
def test_slew_command_handler_validation_checks(ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test slew function rejects incorrect input."""
    [[result_code], [message]] = ds_manager_proxy.Slew([60.0, 50.0, 30.0])
    assert result_code == ResultCode.REJECTED
    assert message == "Expected array of length 2 ([pos_az, pos_el]) but got array of length 3."
