"""Tests for the intial states of DSManager tango device."""

from typing import Any

import pytest
import tango
from ska_control_model import HealthState


@pytest.mark.acceptance
@pytest.mark.forked
@pytest.mark.parametrize(
    "attr_name,attr_state",
    [
        ("healthState", HealthState.OK),
        ("State", tango.DevState.RUNNING),
    ],
)
def test_ds_attr_states_after_init(
    attr_name: str, attr_state: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test the DS Manager attribute states after init is completed."""
    ds_attr = ds_manager_proxy.read_attribute(attr_name)
    assert ds_attr.value == attr_state
