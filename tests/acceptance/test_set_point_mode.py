"""Test Point Mode."""

from typing import Any

import pytest
import tango

from ska_mid_dish_ds_manager.models.dish_enums import DSCState, DSOperatingMode, DSPowerState


@pytest.mark.acceptance
@pytest.mark.forked
def test_set_point_mode(event_store_class: Any, ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test transition to POINT Mode."""
    operating_mode_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_event_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)

    ds_manager_proxy.SetPointMode()
    operating_mode_event_store.wait_for_value(DSOperatingMode.POINT, timeout=6)

    assert ds_manager_proxy.operatingMode == DSOperatingMode.POINT
    assert ds_manager_proxy.powerState == DSPowerState.FULL_POWER
    assert ds_manager_proxy.dscState == DSCState.STANDSTILL
