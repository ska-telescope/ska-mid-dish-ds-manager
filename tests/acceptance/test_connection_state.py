"""Test connection state attribute."""

from typing import Any

import pytest
import tango
from ska_control_model import CommunicationStatus


@pytest.mark.acceptance
@pytest.mark.forked
def test_connection_state_attr(
    event_store_class: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test if the connectionstate gets updated."""
    state_event_store = event_store_class()
    ds_manager_proxy.subscribe_event(
        "connectionState",
        tango.EventType.ARCHIVE_EVENT,
        state_event_store,
    )

    state_event_store.wait_for_value(CommunicationStatus.ESTABLISHED, timeout=30)
    assert True
