"""Test Slew."""

from typing import Any

import pytest
import tango

from ska_mid_dish_ds_manager.models.constants import (
    DEFAULT_AZIMUTH_SPEED_DPS,
    DEFAULT_ELEVATION_SPEED_DPS,
)
from ska_mid_dish_ds_manager.models.dish_enums import (
    DSCState,
    DSOperatingMode,
    DSPowerState,
    PointingState,
)


@pytest.mark.acceptance
@pytest.mark.forked
def test_slew(event_store_class: Any, ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test slew."""
    operating_mode_event_store = event_store_class()
    pointing_state_event_store = event_store_class()
    power_state_event_store = event_store_class()
    achieved_pointing_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "pointingstate",
        tango.EventType.CHANGE_EVENT,
        pointing_state_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "powerstate",
        tango.EventType.CHANGE_EVENT,
        power_state_event_store,
    )

    ds_manager_proxy.subscribe_event(
        "achievedPointing",
        tango.EventType.CHANGE_EVENT,
        achieved_pointing_event_store,
    )

    # check default speeds
    assert ds_manager_proxy.azimuthSpeed == DEFAULT_AZIMUTH_SPEED_DPS
    assert ds_manager_proxy.elevationSpeed == DEFAULT_ELEVATION_SPEED_DPS

    achieved_pointing = ds_manager_proxy.achievedPointing

    target_az_pos = achieved_pointing[1] + (5 if achieved_pointing[1] < 250 else -5)
    target_el_pos = achieved_pointing[2] + (5 if achieved_pointing[2] < 80 else -5)

    # az_pos, el_pos, speed_az, speed_el
    ds_manager_proxy.Slew([target_az_pos, target_el_pos])

    pointing_state_event_store.wait_for_value(PointingState.SLEW, timeout=10)
    pointing_state_event_store.wait_for_value(PointingState.READY, timeout=30)

    achieved_pointing_events = achieved_pointing_event_store.get_queue_events()

    assert len(achieved_pointing_events) > 0

    final_pointing_event = achieved_pointing_events[-1].attr_value.value

    assert final_pointing_event[1] == pytest.approx(target_az_pos)
    assert final_pointing_event[2] == pytest.approx(target_el_pos)

    assert ds_manager_proxy.dscState == DSCState.STANDSTILL
    assert ds_manager_proxy.powerState == DSPowerState.FULL_POWER
    assert ds_manager_proxy.operatingMode == DSOperatingMode.POINT
    assert ds_manager_proxy.pointingstate == PointingState.READY
