"""Test SetPowerMode."""

from typing import Any

import pytest
import tango
from ska_control_model import ResultCode

from ska_mid_dish_ds_manager.models.dish_enums import (
    DSCState,
    DSOperatingMode,
    DSPowerState,
    PointingState,
)


@pytest.mark.acceptance
@pytest.mark.forked
def test_set_power_mode_false(event_store_class: Any, ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test SetPowerMode with False."""
    operating_mode_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_event_store,
    )

    # Param [0.0, 14.7] will become False and 14.7kW
    ds_manager_proxy.SetPowerMode([0.0, 14.7])

    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)
    assert ds_manager_proxy.powerState == DSPowerState.FULL_POWER
    assert ds_manager_proxy.pointingstate == PointingState.READY
    assert ds_manager_proxy.operatingMode == DSOperatingMode.STANDBY_FP
    assert ds_manager_proxy.dscState == DSCState.STANDBY
    assert ds_manager_proxy.read_attribute("dscPowerLimitkW").value == 14.7


@pytest.mark.acceptance
@pytest.mark.forked
def test_set_power_mode_true(event_store_class: Any, ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test SetPowerMode with True."""
    operating_mode_event_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_event_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_FP, timeout=6)

    # Param [1.0, 17.0] will become True and 17.0kW
    ds_manager_proxy.SetPowerMode([1.0, 17.0])

    operating_mode_event_store.wait_for_value(DSOperatingMode.STANDBY_LP, timeout=6)
    assert ds_manager_proxy.powerState == DSPowerState.LOW_POWER
    assert ds_manager_proxy.pointingstate == PointingState.READY
    assert ds_manager_proxy.operatingMode == DSOperatingMode.STANDBY_LP
    assert ds_manager_proxy.dscState == DSCState.STANDBY
    assert ds_manager_proxy.read_attribute("dscPowerLimitkW").value == 17.0


@pytest.mark.acceptance
@pytest.mark.forked
@pytest.mark.parametrize(
    "power_limit,power_setting",
    [
        (5.5, 1.0),
        (50.0, 1.0),
        (4.6, 0.0),
        (100.0, 0.0),
    ],
)
def test_set_power_mode_incorrect_power_limit(
    power_limit: float, power_setting: float, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test SetPowerMode incorrect power limit used."""
    ds_manager_proxy.SetStandbyFPMode()
    power_limit_before = ds_manager_proxy.read_attribute("dscPowerLimitkW").value
    [[result_code], [message]] = ds_manager_proxy.SetPowerMode([power_setting, power_limit])
    assert result_code == ResultCode.REJECTED
    assert message == (
        f"Invalid dsc power limit: {power_limit}. Expected a value in this range" " [10.0, 20.0]."
    )
    assert ds_manager_proxy.read_attribute("dscPowerLimitkW").value == power_limit_before
