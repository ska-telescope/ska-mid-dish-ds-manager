"""Test achievedTargetLock and configureTargetLock."""

from typing import Any

import numpy as np
import pytest
import tango

from ska_mid_dish_ds_manager.models.dish_enums import DSOperatingMode


@pytest.mark.acceptance
@pytest.mark.forked
def test_configure_and_achieved_target_lock_attribute(
    event_store_class: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test the configureTargetLock attribute."""
    operating_mode_store = event_store_class()
    configure_target_lock_store = event_store_class()

    ds_manager_proxy.subscribe_event(
        "operatingmode",
        tango.EventType.CHANGE_EVENT,
        operating_mode_store,
    )

    ds_manager_proxy.subscribe_event(
        "configuretargetlock",
        tango.EventType.CHANGE_EVENT,
        configure_target_lock_store,
    )

    ds_manager_proxy.SetStandbyFPMode()
    operating_mode_store.wait_for_value(DSOperatingMode.STANDBY_FP)

    # change in configureTargetLock triggers appropriate UA methods
    # to DS simulators and stores the new value for the tango attribute.
    # TODO: configureTargetLock startup value is an empty list, should display defaults from DSC
    ds_manager_proxy.configureTargetLock = [3.3, 8.8]
    configure_target_lock_store.wait_for_value([3.3, 8.8], timeout=60)
    assert np.all(ds_manager_proxy.configureTargetLock == [3.3, 8.8])

    # TODO To be discussed:
    # Currently the achievedTargetLock is updated ONLY when the dish moves
    # * Re-calculate achievedTargetLock when the configured threshold changes *
    # * achievedTargetLock needs to be set to False when onSourceDev > onSourceThreshold, currently
    #   once it is set to True it will always be True *
    assert ds_manager_proxy.achievedTargetLock is True
