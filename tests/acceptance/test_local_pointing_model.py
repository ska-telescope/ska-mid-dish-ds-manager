"""Test Static Pointing Model."""

import random
from typing import Any

import pytest
import tango

from ska_mid_dish_ds_manager.models.constants import BAND_POINTING_MODEL_PARAMS_LENGTH


@pytest.mark.acceptance
@pytest.mark.forked
@pytest.mark.parametrize(
    "tango_attribute",
    [
        ("band0PointingModelParams"),
        ("band1PointingModelParams"),
        ("band2PointingModelParams"),
        ("band3PointingModelParams"),
        ("band4PointingModelParams"),
        ("band5aPointingModelParams"),
        ("band5bPointingModelParams"),
    ],
)
def test_read_band_static_pointing_model_parameters(
    tango_attribute: str, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """Test BandN Static Pointing Model Parameters."""
    band_pointing_model_params = ds_manager_proxy.read_attribute(tango_attribute).value

    assert len(band_pointing_model_params) == BAND_POINTING_MODEL_PARAMS_LENGTH
    assert band_pointing_model_params.dtype.name == "float64"  # type: ignore


@pytest.mark.acceptance
@pytest.mark.forked
@pytest.mark.parametrize(
    "tango_attribute",
    [
        ("band0PointingModelParams"),
        ("band1PointingModelParams"),
        ("band2PointingModelParams"),
        ("band3PointingModelParams"),
        ("band4PointingModelParams"),
        ("band5aPointingModelParams"),
        ("band5bPointingModelParams"),
    ],
)
def test_invalid_write_bands_static_pointing_model_parameters(
    tango_attribute: str,
    ds_manager_proxy: tango.DeviceProxy,
) -> None:
    """Test Band Static Pointing Model Parameters."""
    invalid_write_values = []
    abphi_index = 10
    invalid_coeff = random.randint(0, 17)  # make a random coeff invalid
    for i in range(BAND_POINTING_MODEL_PARAMS_LENGTH):
        if i == invalid_coeff:
            invalid_write_values.append(random.uniform(10000, 10001))
        elif i == abphi_index:  # abphi in between [0,360]
            invalid_write_values.append(random.uniform(0, 360))
        else:
            invalid_write_values.append(random.uniform(-2000, 2000))

    with pytest.raises(tango.DevFailed):
        ds_manager_proxy.write_attribute(tango_attribute, invalid_write_values)


@pytest.mark.acceptance
@pytest.mark.forked
@pytest.mark.parametrize(
    "tango_attribute",
    [
        ("band0PointingModelParams"),
        ("band1PointingModelParams"),
        ("band2PointingModelParams"),
        ("band3PointingModelParams"),
        ("band4PointingModelParams"),
        ("band5aPointingModelParams"),
        ("band5bPointingModelParams"),
    ],
)
def test_valid_write_bands_static_pointing_model_parameters(
    tango_attribute: str,
    ds_manager_proxy: tango.DeviceProxy,
    event_store_class: Any,
) -> None:
    """Test Band Static Pointing Model Parameters."""
    write_values = []
    abphi_index = 10

    for i in range(BAND_POINTING_MODEL_PARAMS_LENGTH):
        if i == abphi_index:  # abphi is the only one with unique range
            write_values.append(random.uniform(0, 360))
        else:
            write_values.append(random.uniform(-2000, 2000))
    ds_manager_proxy.write_attribute(tango_attribute, write_values)
    model_event_store = event_store_class()
    ds_manager_proxy.subscribe_event(
        tango_attribute, tango.EventType.CHANGE_EVENT, model_event_store
    )
    model_event_store.wait_for_value(write_values, timeout=7)
