"""Test to ensure DS Manager and DSC build state structures are populated."""

import json

import pytest
import tango

from ska_mid_dish_ds_manager import release
from ska_mid_dish_ds_manager.DSManager import DSManager


@pytest.mark.acceptance
@pytest.mark.forked
def test_ds_and_dsc_build_state(ds_manager_proxy: tango.DeviceProxy) -> None:
    """Test DS buildState attribute fields populated."""
    build_state_attr = ds_manager_proxy.read_attribute("buildState")
    dm_buildstate_dict = json.loads(build_state_attr.value)

    # Verify that DS manager buildState Data is populated as expected
    assert dm_buildstate_dict["device"] == release.name
    assert dm_buildstate_dict["ds_manager_version"] == release.get_ds_manager_release_version()
    assert dm_buildstate_dict["dsc_address"] == DSManager.opcua_server_addr

    # Verify that DSC buildState fields are populated
    dsc_buildstate_dict = dm_buildstate_dict["dsc_build_state"]

    assert dsc_buildstate_dict["device"] == "Dish Structure Controller"
    assert dsc_buildstate_dict["dish_id"]
    assert dsc_buildstate_dict["dsc_serial_number"]
    assert dsc_buildstate_dict["dsc_software_version"]
    assert dsc_buildstate_dict["dsc_icd_version"]
