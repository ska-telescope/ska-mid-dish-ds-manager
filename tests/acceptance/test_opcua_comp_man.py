"""Test the component manager methods."""

from unittest.mock import MagicMock

import pytest

from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager


@pytest.mark.acceptance
@pytest.mark.forked
def test_node_read_method(component_manager_logger: OPCUAComponentManager) -> None:
    """Test the read method used for dynamic attr reads."""
    # mock out the tango Wattibute class
    Wattribute = MagicMock()
    # Creat an instance of the  Wattribute class
    mock_instance = Wattribute()

    # Add mock methods with return values
    mock_instance.get_name.return_value = "dsc_dsc_state"

    cm = component_manager_logger
    assert cm.scu

    # Force the dsc_dsc_state to exist in the opcua_attrbute_mapper
    # since the read_node_value uses it and the dynamic attributes are
    # not setup from a tango layer.
    cm.tango_opcua_attr_mapper["dsc_dsc_state"] = "Management.Status.DscState"

    assert isinstance(int(cm.read_node_value("", mock_instance)), int)
