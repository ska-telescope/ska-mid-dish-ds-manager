"""Acceptance test checking that datachange events from the simulator are being received."""

import logging
from typing import Any
from unittest.mock import Mock

import pytest

from ska_mid_dish_ds_manager.opcua_control import OPCUAControl
from ska_mid_dish_ds_manager.subscription_manager import SubscriptionManager
from tests.utils import EventStore


@pytest.mark.acceptance
@pytest.mark.forked
def test_subscription_manager_monitored_attributes(
    event_store_class: type[EventStore],
    ds_host: str,
    ds_port: int,
    ds_endpoint: str,
    ds_opcua_namespace: str,
) -> None:
    """Test that subscriptions to all the monitored attributes work."""
    event_store: EventStore = event_store_class()
    logger = Mock(spec=logging.Logger)

    scu = OPCUAControl(
        host=ds_host,
        port=ds_port,
        endpoint=ds_endpoint,
        namespace=ds_opcua_namespace,
        timeout=1.0,
    )
    scu.connect_and_setup()

    sub_manager = SubscriptionManager(logger)

    try:
        # Subscribe to all monitored nodes
        sub_manager.setup_subscriptions(scu, event_store._queue)

        # Test with the LowPowerActive node that datachange events come through
        low_power_active_key = "Management.PowerStatus.LowPowerActive"

        assert low_power_active_key in scu.attributes

        target_power_mode = not scu.attributes[low_power_active_key].value

        scu.take_authority("LMC")
        scu.commands["Management.Commands.SetPowerMode"](target_power_mode, 10.0)

        def cond(event: Any) -> bool:
            return event["name"] == low_power_active_key and event["value"] == target_power_mode

        event_store.wait_for_event_condition(cond, timeout=5)
    finally:
        scu.unsubscribe_all()
