"""Test Static Pointing Model."""

from typing import Any

import pytest
import tango


@pytest.mark.acceptance
@pytest.mark.forked
def test_track_load_static_off(
    ds_manager_proxy: tango.DeviceProxy, event_store_class: Any
) -> None:
    """Test TrackLoadStaticOff command."""
    write_values = [20.1, 0.5]

    ds_manager_proxy.TrackLoadStaticOff(write_values)

    model_event_store = event_store_class()
    ds_manager_proxy.subscribe_event(
        "actStaticOffsetValueXel", tango.EventType.CHANGE_EVENT, model_event_store
    )
    model_event_store.wait_for_value(write_values[0], timeout=7)

    ds_manager_proxy.subscribe_event(
        "actStaticOffsetValueEl", tango.EventType.CHANGE_EVENT, model_event_store
    )
    model_event_store.wait_for_value(write_values[1], timeout=7)
