"""Test the component manager."""

import time
from functools import partial
from queue import Empty, Queue
from typing import Any, Optional
from unittest.mock import MagicMock

import pytest
from ska_control_model import CommunicationStatus, TaskStatus

from ska_mid_dish_ds_manager.models.dish_enums import DSPowerState
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager
from ska_mid_dish_ds_manager.opcua_control import OPCUAControl
from tests.utils import EventStore, MethodCallsStore


def func_arg_to_queue(*args: Any, **kwargs: Any) -> None:
    """Store function params in a queue."""
    tracking_queue = kwargs.pop("tracking_queue")
    for arg in args:
        tracking_queue.put(arg)
    for key, val in kwargs.items():
        tracking_queue.put((key, val))


@pytest.mark.acceptance
@pytest.mark.forked
def test_scu(
    ds_host: str, ds_port: int, ds_endpoint: str, ds_opcua_namespace: str
) -> Optional[None]:
    """Check the setup."""
    scu_instance = OPCUAControl(
        host=ds_host,
        port=ds_port,
        endpoint=ds_endpoint,
        namespace=ds_opcua_namespace,
    )
    scu_instance.connect_and_setup()
    assert scu_instance.is_connected()
    assert scu_instance.get_attribute_list()
    assert scu_instance.attributes["Management.Status.DscState"].value > 0
    assert scu_instance.attributes["Logic.Application.ApplicationState"].value > 0
    assert scu_instance.attributes["System.CurrentMode"].value > 0
    assert scu_instance.attributes["Server.ServerStatus"].value.BuildInfo.BuildDate
    assert scu_instance.attributes["Server.ServerStatus"].value.CurrentTime

    scu_instance.disconnect_and_cleanup()
    assert not scu_instance.is_connected()


@pytest.mark.acceptance
@pytest.mark.forked
def test_comm_state(ds_host: str, ds_port: int, ds_endpoint: str, ds_opcua_namespace: str) -> None:
    """Check the communication status is called."""
    logger = MagicMock()
    cm = OPCUAComponentManager(
        ds_host,
        ds_port,
        ds_endpoint,
        ds_opcua_namespace,
        logger,
    )
    tracking_queue: Queue[Any] = Queue()
    cm._update_communication_state = partial(  # type: ignore
        func_arg_to_queue, tracking_queue=tracking_queue
    )
    cm.start_communicating()
    # Wait for comms update, note that on the PLC this takes a while so this timeout will need
    # to be increased
    val = tracking_queue.get(timeout=10)
    assert val == CommunicationStatus.NOT_ESTABLISHED
    val = tracking_queue.get(timeout=180)
    assert val == CommunicationStatus.ESTABLISHED
    cm.stop_communicating()
    val = tracking_queue.get(timeout=20)
    assert val == CommunicationStatus.DISABLED


@pytest.mark.acceptance
@pytest.mark.forked
def test_sub(ds_host: str, ds_port: int, ds_endpoint: str, ds_opcua_namespace: str) -> None:
    """Check that subscriptions come through via component state update."""
    logger = MagicMock()
    cm = OPCUAComponentManager(
        ds_host,
        ds_port,
        ds_endpoint,
        ds_opcua_namespace,
        logger,
    )

    tracking_queue: Queue = Queue()
    cm._update_communication_state = partial(  # type: ignore
        func_arg_to_queue, tracking_queue=tracking_queue
    )
    cm._update_component_state = partial(  # type: ignore
        func_arg_to_queue, tracking_queue=tracking_queue
    )

    cm.start_communicating()  # Wait for comms update
    # Note: On the PLC this takes a while, so the timeout might need to be increased.
    tracking_queue.get(timeout=120)

    waiting_for_attribute_update = {}
    for node_group in cm.subscription_manager.monitored_node_groups:
        for mon_node in node_group.nodes.values():
            waiting_for_attribute_update[mon_node.component_state_name] = True

    timeout = 5
    start_time = time.time()

    try:
        while any(waiting_for_attribute_update.values()) and time.time() - start_time < timeout:
            try:
                update_key, _ = tracking_queue.get(timeout=0.1)

                if update_key in waiting_for_attribute_update:
                    waiting_for_attribute_update[update_key] = False
            except Empty:
                pass

        if any(waiting_for_attribute_update.values()):
            attributes_missing_updates = [
                key for key, value in waiting_for_attribute_update.items() if value
            ]
            assert 0, f"Never got attribute updates for {attributes_missing_updates}."
    finally:
        cm.stop_communicating()


@pytest.mark.acceptance
@pytest.mark.forked
def test_running_command(
    event_store_class: type[EventStore],
    ds_host: str,
    ds_port: int,
    ds_endpoint: str,
    ds_opcua_namespace: str,
) -> None:
    """Check that subscriptions come through via component state update."""
    logger = MagicMock()
    cm = OPCUAComponentManager(
        ds_host,
        ds_port,
        ds_endpoint,
        ds_opcua_namespace,
        logger,
    )
    event_store = event_store_class()
    cm._update_communication_state = partial(  # type: ignore
        func_arg_to_queue, tracking_queue=event_store._queue
    )
    cm._update_component_state = partial(  # type: ignore
        func_arg_to_queue, tracking_queue=event_store._queue
    )

    cm.start_communicating()
    # Wait for comms update, note that on the PLC this takes a while so this timeout will need
    # to be increased
    # 2 events, one for NOT_ESTABLISHED and one for ESTABLISHED
    event_store.wait_for_n_events(2, timeout=150)

    # Mock out task callback
    task_callback_store = MethodCallsStore()

    cm.set_power_mode([False, 10.0], task_callback=task_callback_store)

    task_callback_store.wait_for_kwargs(
        {
            "status": TaskStatus.IN_PROGRESS,
        }
    )
    task_callback_store.wait_for_kwargs(
        {"status": TaskStatus.COMPLETED, "result": "SetPowerMode Called"}
    )

    def cond(event: Any) -> bool:
        return event == ("powerstate", DSPowerState.FULL_POWER)

    event_store.wait_for_event_condition(cond, timeout=10)
