"""Test the dynamic attributes of DS Manager."""

from typing import Any

import pytest
import tango

from ska_mid_dish_ds_manager.models.dish_enums import DSCState
from tests.utils import wait_until_condition_met


@pytest.mark.acceptance
@pytest.mark.forked
def test_ds_manager_dynamic_attribute_updates_correct(
    event_store_class: Any, ds_manager_proxy: tango.DeviceProxy
) -> None:
    """
    Test if a DS Manager dynamic attribute gets updated correctly.

    This test ensures that the dynamically exposed attribute `dsc_dsc_state` updates correctly
    when the DS Manager transitions between states.
    """
    event_store = event_store_class()

    ds_manager_proxy.ExposeDynamicDSCAttributes()

    ds_manager_proxy.subscribe_event(
        "dscState",
        tango.EventType.ARCHIVE_EVENT,
        event_store,
    )

    # Note: Dynamic attributes return string types only.
    # `dscState` and `dsc_dsc_state` track the same thing but have different data types.

    def dyn_attr_ready() -> bool:
        """
        Check if the attribute 'dsc_dsc_state' exists in the attribute list of ds_manager_proxy.

        :return: True if 'dsc_dsc_state' is in the attribute list, False otherwise.
        :rtype: bool
        """
        return "dsc_dsc_state" in ds_manager_proxy.get_attribute_list()

    wait_until_condition_met(dyn_attr_ready, "Dynamic attributes were not set up properly.")

    assert ds_manager_proxy.dsc_dsc_state == "1"

    ds_manager_proxy.SetStandbyFPMode()
    ds_manager_proxy.SetPointMode()
    event_store.wait_for_value(DSCState.STANDSTILL, timeout=6)

    assert ds_manager_proxy.dsc_dsc_state == "7"


@pytest.mark.acceptance
@pytest.mark.forked
def test_ds_manager_dynamic_attribute_setup_completes_without_error(
    ds_manager_proxy: tango.DeviceProxy,
) -> None:
    """Test if all DS Manager dynamic attributes are readable."""
    ds_manager_proxy.ExposeDynamicDSCAttributes()
    attr_list = ds_manager_proxy.get_attribute_list()
    for attr in attr_list:
        if attr.startswith("dsc_"):
            attr_val = ds_manager_proxy.read_attribute(attr)
            # None of the attributes in opcua server are expected to return an empty value.
            assert attr_val != ""
