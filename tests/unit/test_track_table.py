# mypy: disable-error-code="union-attr"
"""Tests opcua component_manager."""

import logging
from typing import Any, List
from unittest.mock import Mock, patch

import pytest
from ska_control_model import ResultCode
from tango.test_context import DeviceTestContext

from ska_mid_dish_ds_manager.DSManager import DSManager
from ska_mid_dish_ds_manager.models.constants import (
    MAX_AZIMUTH,
    MAX_ELEVATION_SCIENCE,
    MIN_AZIMUTH,
    MIN_ELEVATION_SCIENCE,
)
from ska_mid_dish_ds_manager.models.dish_enums import DscCmdAuthType, TrackTableLoadMode
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager
from ska_mid_dish_ds_manager.utils import get_current_tai_timestamp

LOGGER = logging.getLogger(__name__)


# pylint:disable=attribute-defined-outside-init
@pytest.mark.unit
@pytest.mark.forked
class TestTrackLoadTable:
    """Contains tests of the OPCUADeviceComponentManager."""

    def setup_method(self) -> None:
        """Set up context."""
        with patch(
            "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
        ) as patched_opcua_control:
            patched_opcua_control.return_value = Mock()

            self.tango_context = DeviceTestContext(DSManager)
            self.tango_context.start()
            self.device_proxy = self.tango_context.device

            class_instance = DSManager.instances.get(self.device_proxy.name())
            self.opcua_device_cm: OPCUAComponentManager = class_instance.component_manager
            self.scu_mock = Mock()
            mock_command = Mock()
            mock_command.return_value = [Mock(), Mock()]
            self.scu_mock.commands = {
                "Tracking.Commands.TrackLoadTable": mock_command,
                "Tracking.Commands.TrackStart": mock_command,
            }
            mock_attribute = Mock()
            mock_attribute.value = DscCmdAuthType.LMC
            self.scu_mock.attributes = {"CommandArbiter.Status.DscCmdAuthority": mock_attribute}
            self.opcua_device_cm.command_manager.scu = self.scu_mock

    def teardown_method(self) -> None:
        """Tear down context."""
        self.tango_context.stop()

    @pytest.mark.parametrize(
        ("table,expected_msg"),
        [
            ([], "Length of argument (0) is not as expected (>4)."),
            ([TrackTableLoadMode.NEW, 1, 120], "Length of argument (3) is not as expected (>4)."),
            ([TrackTableLoadMode.NEW, 1], "Length of argument (2) is not as expected (>4)."),
            (
                [TrackTableLoadMode.NEW, 2, 120, 30, 50],
                "Sequence length (2) does not match number of sequences in table (1)",
            ),
            (
                [TrackTableLoadMode.NEW, 1, 120, 30, 50, 20],
                "Length of table (4) is not a multiple"
                " of 3 (timestamp, azimuth coordinate, elevation coordinate) as expected.",
            ),
            (
                [TrackTableLoadMode.NEW, 1, 120, 30, 50, 20, 25, 55],
                "Sequence length (1) does not match number of sequences in table (2)",
            ),
        ],
    )
    def test_program_track_table_incorrect_values(
        self, table: List[Any], expected_msg: str
    ) -> None:
        """Verify the incorrect values for program track tables raise an error."""
        [[result_code], [message]] = self.device_proxy.TrackLoadTable(table)
        assert result_code == ResultCode.REJECTED
        assert message == expected_msg

    def test_program_track_table_correct_values(self) -> None:
        """Verify the correct values for program track tables."""
        table_values = [
            [TrackTableLoadMode.NEW, 1, 120, 30, 50],
            [TrackTableLoadMode.NEW, 2, 120, 30, 50, 100, 50, 60],
        ]
        for table in table_values:
            self.device_proxy.TrackLoadTable(table)

    def test_track_limits(self) -> None:
        """Verify incorrect values for program track table raise errors."""
        azimuth_limits = [MIN_AZIMUTH, MAX_AZIMUTH]
        track_elevation_limits = [MIN_ELEVATION_SCIENCE, MAX_ELEVATION_SCIENCE]

        out_of_bound_azimuth = [azimuth_limits[0] - 1, azimuth_limits[1] + 1]
        out_of_bound_track_elevation = [
            track_elevation_limits[0] - 1,
            track_elevation_limits[1] + 1,
        ]

        def test_parameter_out_of_range(
            azimuth_values: list[float], elevation_values: list[float]
        ) -> None:
            current_time = get_current_tai_timestamp()
            for azimuth in azimuth_values:
                for elevation in elevation_values:
                    # Try with new LoadModeType
                    track_table = [
                        TrackTableLoadMode.NEW,
                        1,  # Number of entries
                        current_time + 1,
                        azimuth,
                        elevation,
                    ]
                    [[result_code], [_]] = self.device_proxy.TrackLoadTable(track_table)
                    assert result_code == ResultCode.REJECTED

                    # Try with append LoadModeType
                    track_table = [
                        TrackTableLoadMode.APPEND,
                        1,  # Number of entries
                        current_time + 1,
                        azimuth,
                        elevation,
                    ]
                    [[result_code], [_]] = self.device_proxy.TrackLoadTable(track_table)
                    assert result_code == ResultCode.REJECTED

        test_parameter_out_of_range(out_of_bound_azimuth, track_elevation_limits)
        test_parameter_out_of_range(azimuth_limits, out_of_bound_track_elevation)
        test_parameter_out_of_range(out_of_bound_azimuth, out_of_bound_track_elevation)
