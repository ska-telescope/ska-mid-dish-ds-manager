# pylint: disable=protected-access,attribute-defined-outside-init,too-few-public-methods
"""Tests opcua component_manager."""

import dataclasses
import json
import logging
import time
from typing import Any
from unittest.mock import Mock

import pytest

from ska_mid_dish_ds_manager import release
from ska_mid_dish_ds_manager.models.data_classes import (
    DscBuildStateDataclass,
    DsmBuildStateDataclass,
)
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager
from ska_mid_dish_ds_manager.opcua_control import OPCUAControl
from ska_mid_dish_ds_manager.subscription_manager import MonitoredNode, MonitoredNodeGroup

LOGGER = logging.getLogger(__name__)


def wait_for_calls(mock: Mock, call_count: int, timeout: float = 5.0) -> None:
    """Check and wait for mock function calls."""
    start_time = time.time()
    while time.time() - start_time < timeout:
        if mock.call_count >= call_count:  # Check if it has been called
            return
        time.sleep(0.1)  # Small delay to avoid busy-waiting
    raise TimeoutError(
        f"Mock was not called enough times ({call_count}) before the timeout,"
        f" called ({mock.call_count}) time(s)."
    )


@pytest.mark.unit
class TestSCUOPCUAComponentManager:
    """Contains tests of the SCUlib variant of the OPCUAComponentManager."""

    def setup_method(self) -> None:
        """Set up context."""
        self.logger = Mock(spec=logging.Logger)
        self.component_manager = OPCUAComponentManager(
            "opcua.server.address", 4840, "/some/endpoint", "http://skao.int/DS_ICD/", self.logger
        )

    def test_setup_subscriptions(self) -> None:
        """
        Verify behaviour of setup_subscriptions command.

        :param component_manager: the component manager under test
        :param subscribe_mock: a dictionary of mocks.
        """
        with pytest.raises(RuntimeError):
            self.component_manager.setup_subscriptions()

        self.component_manager.scu = Mock()
        subscribe_mock = Mock()
        self.component_manager.scu.subscribe = subscribe_mock

        # Set the available nodes so we can test how the setup_subscriptions method behaves
        # no available nodes
        self.component_manager.scu.nodes = []

        self.component_manager.setup_subscriptions()
        assert subscribe_mock.call_count == 0

        # only one available node, from the static group
        self.component_manager.scu.nodes = [
            "Management.Status.DscState",
        ]

        self.component_manager.setup_subscriptions()
        assert subscribe_mock.call_count == 1

        # one available node from each group
        self.component_manager.scu.nodes = [
            "Management.Status.DscState",
            "Pointing.Status.CurrentPointing",
        ]

        subscribe_mock.reset_mock()
        self.component_manager.setup_subscriptions()
        assert subscribe_mock.call_count == 2

    def test_event_consumer(self) -> None:
        """Verify the behaviour of the event_comsumer command in the component manager."""
        # start the event consumer thread but don't attempt connection
        self.component_manager._attempt_connection = Mock()  # type: ignore

        event_handler_cb_mock = Mock()
        self.component_manager.event_handler_cb = event_handler_cb_mock  # type: ignore

        self.component_manager.start_communicating()
        assert (
            self.component_manager._consuming_thread
            and self.component_manager._consuming_thread.is_alive()
        )

        test_opcua_control = OPCUAControl(
            host="opcua/server/address",
            port=1234,
            endpoint="some/endpoint",
            namespace="namespace",
            timeout=1.0,
        )
        self.component_manager._events_queue.put(test_opcua_control)
        wait_for_calls(event_handler_cb_mock, 1)

        event_handler_cb_mock.reset_mock()

        mock_item = Mock()
        self.component_manager._events_queue.put(mock_item)

        wait_for_calls(event_handler_cb_mock, 1)

        event_handler_cb_mock.assert_called_with(sub_event=mock_item)

        self.component_manager.scu = None
        self.component_manager.stop_communicating()

        assert self.component_manager._connect_thread is None
        assert self.component_manager._consuming_thread is None
        assert self.component_manager.scu is None

    def test_event_handler_cb(self) -> None:
        """

        Verify the behaviour of events handled from the subscriptions events.

        :param component_manager: the component manager under test
        :param mock_side_effect: tuple of calculate_updates mock
        """
        # start the event consumer thread but don't attempt connection
        self.component_manager._attempt_connection = Mock()  # type: ignore
        self.component_manager.start_communicating()

        mock_side_effect = Mock()
        mock_side_effect.return_value = {"someattribute": 123}

        mock_update_component_state = Mock()
        self.component_manager._update_component_state = (  # type: ignore
            mock_update_component_state
        )
        self.component_manager.subscription_manager.monitored_node_groups = [
            MonitoredNodeGroup(
                100,
                {
                    "Management.Status.DscState": MonitoredNode(
                        node_path="Management.Status.DscState",
                        component_state_name="dscstate",
                        convert_to_component_state_val=lambda sub_event: sub_event.value,
                        calculate_updates=(mock_side_effect,),
                    ),
                },
            )
        ]

        self.component_manager._events_queue.put(
            {
                "name": "Management.Status.DscState",
                "node": Mock(),
                "value": 111,
                "source_timestamp": Mock(),
                "server_timestamp": Mock(),
                "data": Mock(),
            }
        )

        wait_for_calls(mock_update_component_state, 2, timeout=5)

        mock_update_component_state.assert_any_call(dscstate=111)
        mock_update_component_state.assert_any_call(someattribute=123)

        self.component_manager.stop_communicating()

    def test_update_build_state_info(self) -> None:
        """
        Test the `_update_build_state_info` method.

        :param component_manager: the component manager under test
        :param mock_update_component_state: mock method
        """
        self.component_manager._attempt_connection = Mock()  # type: ignore
        self.component_manager.scu = Mock()  # type: ignore

        mock_update_component_state = Mock()
        self.component_manager._update_component_state = (  # type: ignore
            mock_update_component_state
        )

        class Attribute:
            """Attribute class."""

            def __init__(self, value: Any) -> None:
                self.value: Any = value

        self.component_manager.scu.attributes = {
            "Management.NamePlate.DishId": Attribute("1"),
            "Management.NamePlate.DishStructureSerialNo": Attribute("2"),
            "Management.NamePlate.DscSoftwareVersion": Attribute("3"),
            "Management.NamePlate.IcdVersion": Attribute("4"),
        }

        self.component_manager._update_build_state_info()

        expected_dsc_build_state = DscBuildStateDataclass(
            "Dish Structure Controller",
            "1",
            "2",
            "3",
            "4",
        )
        expected_dm_build_state = DsmBuildStateDataclass(
            release.name,
            release.get_ds_manager_release_version(),
            f"opc.tcp://{self.component_manager.ds_host}:"
            + f"{self.component_manager.ds_port}{self.component_manager.ds_endpoint}",
            expected_dsc_build_state,
        )
        expected_dsc_builds_state_json: str = json.dumps(
            dataclasses.asdict(expected_dm_build_state), indent=4
        )

        mock_update_component_state.assert_called_once_with(
            buildstate=expected_dsc_builds_state_json
        )
