# pylint: disable=protected-access,unused-argument
"""Test the operating mode calculations."""
import logging
from unittest.mock import Mock

import pytest

from ska_mid_dish_ds_manager.models.dish_enums import DSCState, DSOperatingMode, DSPowerState
from ska_mid_dish_ds_manager.subscription_manager import SubscriptionManager
from ska_mid_dish_ds_manager.utils import SubscriptionEvent
from tests.utils import AlwaysContainsList


# pylint:disable=attribute-defined-outside-init,protected-access
@pytest.mark.unit
@pytest.mark.forked
class TestOperatingMode:
    """Contains Operating Mode tests."""

    def setup_method(self) -> None:
        """Test set up for Subscription Manager."""
        self.logger = Mock(spec=logging.Logger)
        self.manager = SubscriptionManager(self.logger)
        self.manager.build_monitored_nodes(AlwaysContainsList())

        assert self.manager.monitored_node_groups is not None
        assert isinstance(self.manager.monitored_node_groups, list)

    @pytest.mark.parametrize(
        "dsc_state,power_state,hhpconnected,expected_result",
        [
            (DSCState.ACTIVATING, None, False, DSOperatingMode.UNKNOWN),
            (DSCState.DEACTIVATING, None, False, DSOperatingMode.UNKNOWN),
            (DSCState.STARTUP, None, False, DSOperatingMode.STARTUP),
            (DSCState.STOWED, None, False, DSOperatingMode.STOW),
            (DSCState.LOCKED, None, False, DSOperatingMode.ESTOP),
            (DSCState.LOCKED_STOWED, None, False, DSOperatingMode.ESTOP),
            (DSCState.STANDSTILL, None, False, DSOperatingMode.POINT),
            (DSCState.STOPPING, None, False, DSOperatingMode.POINT),
            (DSCState.SLEW, None, False, DSOperatingMode.POINT),
            (DSCState.TRACK, None, False, DSOperatingMode.POINT),
            (DSCState.TRACK, None, True, DSOperatingMode.MAINTENANCE),
            (DSCState.JOG, None, True, DSOperatingMode.MAINTENANCE),
            (DSCState.JOG, None, False, DSOperatingMode.MAINTENANCE),
            (
                DSCState.STANDBY,
                DSPowerState.FULL_POWER,
                False,
                DSOperatingMode.STANDBY_FP,
            ),
            (
                DSCState.STANDBY,
                DSPowerState.LOW_POWER,
                False,
                DSOperatingMode.STANDBY_LP,
            ),
            (
                DSCState.STANDBY,
                DSPowerState.OFF,
                False,
                DSOperatingMode.UNKNOWN,
            ),
            (
                DSCState.STANDBY,
                DSPowerState.UNKNOWN,
                False,
                DSOperatingMode.UNKNOWN,
            ),
            (
                DSCState.STANDBY,
                DSPowerState.UPS,
                False,
                DSOperatingMode.UNKNOWN,
            ),
        ],
    )
    def test_operating_mode_calculation(
        self,
        dsc_state: DSCState,
        power_state: DSPowerState,
        hhpconnected: bool,
        expected_result: DSOperatingMode,
    ) -> None:
        """Test the operating mode options."""
        event = Mock(spec=SubscriptionEvent)

        component_state = {
            "dscstate": dsc_state,
            "powerstate": power_state,
            "hhpconnected": hhpconnected,
        }

        operating_mode = self.manager._compute_operating_mode(event, component_state)
        assert operating_mode["operatingmode"] == expected_result
