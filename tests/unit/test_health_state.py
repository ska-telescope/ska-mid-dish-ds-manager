"""Test HealthState."""

import logging
from unittest.mock import Mock

import pytest  # type: ignore
from ska_control_model import HealthState

from ska_mid_dish_ds_manager.models.dish_enums import ApplicationStateType, CurrentModeType
from ska_mid_dish_ds_manager.subscription_manager import SubscriptionManager
from ska_mid_dish_ds_manager.utils import SubscriptionEvent
from tests.utils import AlwaysContainsList


# pylint:disable=attribute-defined-outside-init,protected-access
@pytest.mark.unit
@pytest.mark.forked
class TestHealthState:
    """Contains HealthState tests triggered by Application State or Current Mode changes."""

    def setup_method(self) -> None:
        """Test set up for Subscription Manager."""
        self.logger = Mock(spec=logging.Logger)
        self.manager = SubscriptionManager(self.logger)
        self.manager.build_monitored_nodes(AlwaysContainsList())

        assert self.manager.monitored_node_groups is not None
        assert isinstance(self.manager.monitored_node_groups, list)

    def test_health_state_cb_current_mode_change(
        self,
    ) -> None:
        """Tests HealthState changes based on Current Mode changes."""
        component_state = {
            "currentmode": None,
            "applicationstate": ApplicationStateType.Run,
        }
        event = Mock(spec=SubscriptionEvent)
        updates = self.manager._compute_health_state(event, component_state)
        assert updates["healthstate"] == HealthState.FAILED
        component_state["currentmode"] = CurrentModeType.SYSTEMMODE_BB  # type: ignore
        updates = self.manager._compute_health_state(event, component_state)
        assert updates["healthstate"] == HealthState.OK

    def test_health_state_cb_app_state_change(
        self,
    ) -> None:
        """Tests HealthState changes based on Application States changes."""
        component_state = {
            "currentmode": CurrentModeType.SYSTEMMODE_BB,
            "applicationstate": None,
        }
        event = Mock(spec=SubscriptionEvent)
        updates = self.manager._compute_health_state(event, component_state)
        assert updates["healthstate"] == HealthState.FAILED
        component_state["applicationstate"] = ApplicationStateType.Run  # type: ignore
        updates = self.manager._compute_health_state(event, component_state)
        assert updates["healthstate"] == HealthState.OK
