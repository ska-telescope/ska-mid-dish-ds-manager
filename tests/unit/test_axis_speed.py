"""Test axis speed."""

from typing import Type
from unittest.mock import Mock, patch

import pytest
import tango
from tango.test_context import DeviceTestContext

from ska_mid_dish_ds_manager.DSManager import DSManager
from ska_mid_dish_ds_manager.models.constants import (
    DEFAULT_AZIMUTH_SPEED_DPS,
    DEFAULT_ELEVATION_SPEED_DPS,
)
from tests.utils import EventStore


# pylint:disable=attribute-defined-outside-init
@pytest.mark.unit
@pytest.mark.forked
class TestAxisSpeed:
    """Test axis speed."""

    def setup_method(self) -> None:
        """Set up context."""
        with patch(
            "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
        ) as patched_opcua_control:
            patched_opcua_control.return_value = Mock()

            self.tango_context = DeviceTestContext(DSManager)
            self.tango_context.start()
            self.device_proxy = self.tango_context.device

    def teardown_method(self) -> None:
        """Tear down context."""
        self.tango_context.stop()

    def test_elevation(self, event_store_class: Type[EventStore]) -> None:
        """Verify that elevation is updated."""
        assert self.device_proxy.elevationSpeed == DEFAULT_ELEVATION_SPEED_DPS

        event_store = event_store_class()
        self.device_proxy.subscribe_event(
            "elevationSpeed",
            tango.EventType.ARCHIVE_EVENT,
            event_store,
        )
        assert event_store.wait_for_value(DEFAULT_ELEVATION_SPEED_DPS)
        self.device_proxy.elevationSpeed = 0.65
        assert event_store.wait_for_value(0.65)
        assert self.device_proxy.elevationSpeed == 0.65

    def test_azimuth(self, event_store_class: Type[EventStore]) -> None:
        """Verify that azimuth is updated."""
        assert self.device_proxy.azimuthSpeed == DEFAULT_AZIMUTH_SPEED_DPS

        event_store = event_store_class()
        self.device_proxy.subscribe_event(
            "azimuthSpeed",
            tango.EventType.ARCHIVE_EVENT,
            event_store,
        )
        assert event_store.wait_for_value(DEFAULT_AZIMUTH_SPEED_DPS)
        self.device_proxy.azimuthSpeed = 0.77
        assert event_store.wait_for_value(0.77)
        assert self.device_proxy.azimuthSpeed == 0.77
