"""Test client can subscribe to archive events."""

from typing import Type
from unittest.mock import Mock, patch

import pytest
import tango
from tango.test_context import DeviceTestContext

from ska_mid_dish_ds_manager.DSManager import DSManager
from tests.utils import EventStore


# pylint:disable=attribute-defined-outside-init
@pytest.mark.unit
@pytest.mark.forked
@pytest.mark.parametrize(
    ("attribute"),
    [
        ("trackProgramMode"),
    ],
)
class TestArchiveEvent:
    """Test client can subscribe and receive archive events."""

    def setup_method(self) -> None:
        """Set up context."""
        with patch(
            "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
        ) as patched_opcua_control:
            patched_opcua_control.return_value = Mock()

            self.tango_context = DeviceTestContext(DSManager)
            self.tango_context.start()
            self.device_proxy = self.tango_context.device

    def teardown_method(self) -> None:
        """Tear down context."""
        self.tango_context.stop()

    def test_client_receives_archive_event(
        self, event_store_class: Type[EventStore], attribute: str
    ) -> None:
        """Verify archive events get pushed to the event store."""
        event_store = event_store_class()

        self.device_proxy.subscribe_event(
            attribute,
            tango.EventType.ARCHIVE_EVENT,
            event_store,
        )

        assert event_store.get_queue_events()
