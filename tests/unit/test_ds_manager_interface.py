"""Unit tests for verifying the ds manager interface."""

from unittest.mock import Mock, patch

import pytest
from tango.test_context import DeviceTestContext

from ska_mid_dish_ds_manager.DSManager import DSManager


# pylint:disable=attribute-defined-outside-init
@pytest.mark.unit
@pytest.mark.forked
class TestDSManagerInterface:
    """Tests for Interface."""

    def setup_method(self) -> None:
        """Set up context."""
        with patch(
            "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
        ) as patched_opcua_control:
            patched_opcua_control.return_value = Mock()

            self.tango_context = DeviceTestContext(DSManager)
            self.tango_context.start()
            self.device_proxy = self.tango_context.device

    def teardown_method(self) -> None:
        """Tear down context."""
        self.tango_context.stop()

    def test_command_list(self) -> None:
        """Command list test."""
        command_list = [
            "SetStandbyLPMode",
            "SetStandbyFPMode",
            "SetPointMode",
            "Stow",
            "SetMaintenanceMode",
            "SetIndexPosition",
            "Slew",
            "Track",
            "TrackStop",
            "TrackLoadStaticOff",
            "TrackLoadTable",
        ]

        device_command_list = self.device_proxy.get_command_list()
        for command in command_list:
            assert command in device_command_list

    def test_attribute_list(self) -> None:
        """Attribute list test."""
        attribute_list = [
            "operatingMode",
            "indexerPosition",
            "pointingState",
        ]

        device_attribute_list = self.device_proxy.get_attribute_list()
        for attribute in attribute_list:
            assert attribute in device_attribute_list
