"""Tests ds manager component manager commands using invalid input."""

import pytest
from ska_control_model import TaskStatus

from ska_mid_dish_ds_manager.models.constants import BAND_POINTING_MODEL_PARAMS_LENGTH
from ska_mid_dish_ds_manager.models.dish_enums import BandType
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager


@pytest.mark.unit
def test_update_pointing_model_params_with_invalid_input(
    ds_component_manager: OPCUAComponentManager,
) -> None:
    """
    Verify behaviour of update_pointing_model_params command using invalid input.

    :param component_manager: the component manager under test
    :param callbacks: a dictionary of mocks, passed as callbacks to
        the command tracker under test
    """
    success, message = ds_component_manager.update_pointing_model_params(
        BandType.B2,
        [33.0, 10.0, 0.0],
    )
    assert success is TaskStatus.FAILED
    assert message == f"Expected {BAND_POINTING_MODEL_PARAMS_LENGTH} arguments but got 3 arg(s)."


@pytest.mark.unit
def test_update_threshold_levels_with_invalid_input(
    ds_component_manager: OPCUAComponentManager,
) -> None:
    """
    Verify behaviour of update_threshold_levels command using invalid input.

    :param component_manager: the component manager under test
    :param callbacks: a dictionary of mocks, passed as callbacks to
        the command tracker under test
    """
    success, message = ds_component_manager.update_threshold_levels([8.0])
    assert success is TaskStatus.FAILED
    assert (
        message == "Expected 2 arguments (on_source_threshold, threshold_time_period) "
        "but got 1 arg(s)."
    )


@pytest.mark.unit
def test_slew_with_invalid_input(
    ds_component_manager: OPCUAComponentManager,
) -> None:
    """
    Verify behaviour of slew command using invalid input.

    :param component_manager: the component manager under test
    :param callbacks: a dictionary of mocks, passed as callbacks to
        the command tracker under test
    """
    result_code, message = ds_component_manager.slew([3.0, 7.0, 2.0], 2.0, 1.0)
    assert message == "Expected array of length 2 ([pos_az, pos_el]) but got array of length 3."
    assert result_code == TaskStatus.REJECTED
