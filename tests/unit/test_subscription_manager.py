# pylint: disable=protected-access,attribute-defined-outside-init

"""Test the subscription manager class."""

import logging
from typing import Any
from unittest.mock import Mock

import asyncua
import pytest
from asyncua import ua
from ska_control_model import HealthState

from ska_mid_dish_ds_manager.models.dish_enums import (
    ApplicationStateType,
    CurrentModeType,
    DSCState,
    DSOperatingMode,
    DSPowerState,
    IndexerPosition,
    PointingState,
)
from ska_mid_dish_ds_manager.subscription_manager import SubscriptionManager
from ska_mid_dish_ds_manager.utils import SubscriptionEvent
from tests.utils import AlwaysContainsList


# type: ignore
@pytest.mark.unit
@pytest.mark.forked
class TestSubscriptionManager:
    """Contains tests for the Subscription Manager Class."""

    def setup_method(self) -> None:
        """Test set up for Subscription Manager."""
        self.logger = Mock(spec=logging.Logger)
        self.manager = SubscriptionManager(self.logger)
        self.manager.build_monitored_nodes(AlwaysContainsList())

        assert self.manager.monitored_node_groups is not None
        assert isinstance(self.manager.monitored_node_groups, list)

    def test_compute_band_type_to_indexer_position(self) -> None:
        """Test mapping of band type to indexer position."""
        event = Mock(spec=SubscriptionEvent)
        component_state: dict = {}
        # # Define a mock BandType enum with the expected values for testing purposes
        asyncua.ua.BandType = Mock()
        asyncua.ua.BandType.Band_1 = 0
        asyncua.ua.BandType.Band_2 = 1
        asyncua.ua.BandType.Band_3 = 2
        asyncua.ua.BandType.Band_4 = 3
        asyncua.ua.BandType.Band_5a = 4
        asyncua.ua.BandType.Band_5b = 5
        asyncua.ua.BandType.Band_6 = 6
        asyncua.ua.BandType.Optical = 7
        # Test known band type
        event.value = ua.BandType.Band_1
        updates = self.manager._compute_band_type_to_indexer_position(event, component_state)
        assert updates["indexerposition"] == IndexerPosition.B1
        # Test unknown band type
        event.value = ua.BandType.Band_6
        updates = self.manager._compute_band_type_to_indexer_position(event, component_state)
        assert updates["indexerposition"] == IndexerPosition.UNKNOWN

    def test_compute_health_state(self) -> None:
        """Test calculation of health_state based on component states."""
        component_state = {
            "currentmode": CurrentModeType.SYSTEMMODE_BB,
            "applicationstate": ApplicationStateType.Run,
        }
        event = Mock(spec=SubscriptionEvent)
        updates = self.manager._compute_health_state(event, component_state)
        assert updates["healthstate"] == HealthState.OK
        # Test for non-OK health state
        component_state["currentmode"] = None  # type: ignore
        updates = self.manager._compute_health_state(event, component_state)
        assert updates["healthstate"] == HealthState.FAILED

    def test_compute_pointing_updates(self) -> None:
        """Test updates to currentpointing and achievedpointing."""
        component_state = {"currentpointing": [0, 1, 2, 3, 4, 5, 6]}
        event = Mock(spec=SubscriptionEvent)
        event.value = [0, 10, 20, 0, 0, 0, 0]
        updates = self.manager._compute_pointing_updates(event, component_state)
        assert updates["currentpointing"] == [0, 10, 20, 0, 0, 0, 0]
        assert updates["achievedpointing"] == [0, 10, 20]
        # Test with no change in pointing
        component_state["currentpointing"] = [0, 10, 20, 0, 0, 0, 0]
        updates = self.manager._compute_pointing_updates(event, component_state)
        assert "currentpointing" not in updates  # No updates if values are the same

    def test_compute_pointing_state(self) -> None:
        """Test calculation of pointingstate based on dscstate."""
        component_state = {"dscstate": DSCState.SLEW}
        event = Mock(spec=SubscriptionEvent)
        updates = self.manager._compute_pointing_state(event, component_state)
        assert updates["pointingstate"] == PointingState.SLEW
        # Test unknown pointing state
        component_state["dscstate"] = None  # type: ignore
        updates = self.manager._compute_pointing_state(event, component_state)
        assert updates["pointingstate"] == PointingState.UNKNOWN

    def test_compute_operating_mode(self) -> None:
        """Test computation of operating mode based on component states."""
        event = Mock(spec=SubscriptionEvent)
        # Test hhpconnected condition
        component_state: dict[str, Any] = {"hhpconnected": True}
        updates = self.manager._compute_operating_mode(event, component_state)
        assert updates["operatingmode"] == DSOperatingMode.MAINTENANCE
        # Test standby with powerstate variations
        component_state = {
            "hhpconnected": False,
            "dscstate": DSCState.STANDBY,
            "powerstate": DSPowerState.LOW_POWER,
        }
        updates = self.manager._compute_operating_mode(event, component_state)
        assert updates["operatingmode"] == DSOperatingMode.STANDBY_LP
        component_state["powerstate"] = DSPowerState.FULL_POWER
        updates = self.manager._compute_operating_mode(event, component_state)
        assert updates["operatingmode"] == DSOperatingMode.STANDBY_FP

    def test_compute_configure_target_lock(self) -> None:
        """Test computation of operating mode based on component states."""
        event = Mock(spec=SubscriptionEvent)
        component_state: dict[str, Any] = {}
        updates = self.manager._compute_configure_target_lock(event, component_state)
        assert updates["configuretargetlock"] == [0.0, 0.0]
        component_state = {
            "onsourcethreshold": 1.0,
            "thresholdtimeperiod": 2.0,
        }
        updates = self.manager._compute_configure_target_lock(event, component_state)
        assert updates["configuretargetlock"] == [1.0, 2.0]

    def test_node_conversion_methods(self) -> None:
        """Test node conversion methods for correct Enum/type conversions."""
        # Example for `Management.Status.DscState` node with DSCState enum conversion
        event = Mock(spec=SubscriptionEvent)
        event.value = DSCState.STANDSTILL.value
        node = self.manager.get_monitored_node("Management.Status.DscState")
        assert node
        assert node.convert_to_component_state_val is not None
        converted_value = node.convert_to_component_state_val(event)
        assert converted_value == DSCState.STANDSTILL

    def test_side_effect_functions_called(self) -> None:
        """Test that side-effect functions are called as expected."""
        # Mock the side-effect functions
        mock_operating_mode = Mock()
        mock_pointing_state = Mock()
        # Patch the SubscriptionManager functions directly in the calculate_updates tuple
        node = self.manager.get_monitored_node("Management.Status.DscState")
        assert node

        node.calculate_updates = (mock_pointing_state, mock_operating_mode)
        # Create a mock event and component state
        event = Mock(spec=SubscriptionEvent)
        event.value = DSCState.SLEW.value
        component_state = {"dscstate": DSCState.SLEW}
        # Manually trigger the side-effect functions
        for func in node.calculate_updates:
            func(event, component_state)
        # Assert that the mocked functions were called
        mock_operating_mode.assert_called_once_with(event, component_state)
        mock_pointing_state.assert_called_once_with(event, component_state)
