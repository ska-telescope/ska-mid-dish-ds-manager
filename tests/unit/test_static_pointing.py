# pylint: disable=protected-access

"""Test static pointing models."""
import logging
import unittest
from unittest.mock import Mock

import pytest

from ska_mid_dish_ds_manager.models.dish_enums import BandType
from ska_mid_dish_ds_manager.subscription_manager import (  # type: ignore
    MonitoredNode,
    SubscriptionManager,
)
from tests.utils import AlwaysContainsList


class TestStaticPointingModel(unittest.TestCase):
    """Test Static Pointing Model."""

    def setUp(self) -> None:
        """Test set up for Subscription Manager."""
        self.logger = Mock(spec=logging.Logger)
        self.manager = SubscriptionManager(self.logger)
        self.manager.build_monitored_nodes(AlwaysContainsList())

    @pytest.mark.unit
    @pytest.mark.forked
    def test_static_pointing_model_node_paths(self) -> None:
        """Tests subscribed node paths creates correct entries for each band and coefficient."""
        # Generate expected keys based on the mocked band and coefficient lists
        expected_keys = {
            f"Pointing.StaticPmParameters.StaticPmParameters[{pm_param_int}].{coeff}"
            for pm_param_int in self.manager._dsc_static_pointing_model_bands
            for coeff in self.manager._dsc_static_pointing_model_params_order
        }

        # 144 = 8 * 18 = num_bands * num_parameters_per_band
        self.assertEqual(len(expected_keys), 144)

        # Check if all expected keys are in node_paths and each entry is a MonitoredNode instance
        for key in expected_keys:
            found_node = self.manager.get_monitored_node(key)
            assert found_node is not None
            self.assertIsInstance(found_node, MonitoredNode)

        # Verify specific structure and format of a few sample entries
        for pm_param_int in self.manager._dsc_static_pointing_model_bands:
            bandtype_index = pm_param_int
            band_param = BandType(bandtype_index)

            if band_param == BandType.OPTICAL:
                band_param_str = "0"
            else:
                band_param_str = band_param.name.lower()[1:]

            band_comp_state_name = f"band{band_param_str}pointingmodelparams"
            for coeff in self.manager._dsc_static_pointing_model_params_order:
                key = f"Pointing.StaticPmParameters.StaticPmParameters[{pm_param_int}].{coeff}"

                # Check MonitoredNode values
                node = self.manager.get_monitored_node(key)

                assert node
                self.assertEqual(node.node_path, key)
                self.assertEqual(node.component_state_name, band_comp_state_name)
                self.assertIsNone(node.convert_to_component_state_val)

                # Ensure the correct partial function is set in calculate_updates
                self.assertEqual(len(node.calculate_updates), 1)
                self.assertEqual(node.calculate_updates[0].func, self.manager._compute_band_update)
                self.assertEqual(
                    node.calculate_updates[0].args,
                    (band_comp_state_name, coeff),
                )
