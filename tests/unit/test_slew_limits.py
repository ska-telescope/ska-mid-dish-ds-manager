# mypy: disable-error-code="union-attr"
"""Tests slew validation checks."""

import logging
from unittest.mock import Mock, patch

import pytest
import tango
from ska_control_model import ResultCode
from tango.test_context import DeviceTestContext

from ska_mid_dish_ds_manager.DSManager import DSManager
from ska_mid_dish_ds_manager.models.constants import (
    MAX_AZIMUTH,
    MAX_AZIMUTH_SPEED_DPS,
    MAX_ELEVATION,
    MAX_ELEVATION_SPEED_DPS,
    MIN_AZIMUTH,
    MIN_ELEVATION,
)
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager

LOGGER = logging.getLogger(__name__)


# pylint:disable=attribute-defined-outside-init
@pytest.mark.unit
@pytest.mark.forked
class TestTrackLoadTable:
    """Contains tests of the OPCUADeviceComponentManager."""

    def setup_method(self) -> None:
        """Set up context."""
        with patch(
            "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
        ) as patched_opcua_control:
            patched_opcua_control.return_value = Mock()

            self.tango_context = DeviceTestContext(DSManager)
            self.tango_context.start()
            self.device_proxy = self.tango_context.device

            class_instance = DSManager.instances.get(self.device_proxy.name())
            self.opcua_device_cm: OPCUAComponentManager = class_instance.component_manager
            self.scu_mock = Mock()
            self.scu_mock.commands = {"Tracking.Commands.TrackStart": Mock()}
            self.opcua_device_cm.command_manager.scu = self.scu_mock

    def teardown_method(self) -> None:
        """Tear down context."""
        self.tango_context.stop()

    def test_slew_limits(self) -> None:
        """Verify incorrect values for slew parameters raise errors."""
        azimuth_limits = [MIN_AZIMUTH, MAX_AZIMUTH]
        elevation_limits = [MIN_ELEVATION, MAX_ELEVATION]

        out_of_bound_azimuth_speeds = [-100, 0, MAX_AZIMUTH_SPEED_DPS + 1]
        out_of_bound_elevation_speeds = [-100, 0, MAX_ELEVATION_SPEED_DPS + 1]
        out_of_bound_azimuth = [azimuth_limits[0] - 1, azimuth_limits[1] + 1]
        out_of_bound_elevation = [elevation_limits[0] - 1, elevation_limits[1] + 1]

        # Test set speeds outside of limits
        for speed in out_of_bound_azimuth_speeds:
            with pytest.raises(tango.DevFailed):
                self.device_proxy.azimuthSpeed = speed

        for speed in out_of_bound_elevation_speeds:
            with pytest.raises(tango.DevFailed):
                self.device_proxy.elevationSpeed = speed

        # Test slew outside of axis limits
        def test_parameter_out_of_range(
            azimuth_values: list[float], elevation_values: list[float]
        ) -> None:
            for azimuth in azimuth_values:
                for elevation in elevation_values:
                    [[result_code], [_]] = self.device_proxy.Slew([azimuth, elevation])
                    assert result_code == ResultCode.REJECTED

        test_parameter_out_of_range(out_of_bound_azimuth, elevation_limits)
        test_parameter_out_of_range(azimuth_limits, out_of_bound_elevation)
        test_parameter_out_of_range(out_of_bound_azimuth, out_of_bound_elevation)

        # Test slew within limits
        def test_slew_and_then_wait(
            azimuth: float,
            elevation: float,
            speed_az: float = MAX_AZIMUTH_SPEED_DPS,
            speed_el: float = MAX_ELEVATION_SPEED_DPS,
        ) -> None:
            self.device_proxy.azimuthSpeed = speed_az
            self.device_proxy.elevationSpeed = speed_el
            self.device_proxy.Slew([azimuth, elevation])

        # some combinations of valid parameters
        test_slew_and_then_wait(
            azimuth_limits[0], elevation_limits[0], speed_az=MAX_AZIMUTH_SPEED_DPS / 2
        )
        test_slew_and_then_wait(azimuth_limits[0] + 1, elevation_limits[0] + 1)
        test_slew_and_then_wait(
            azimuth_limits[1], elevation_limits[1], speed_el=MAX_ELEVATION_SPEED_DPS / 2
        )
        test_slew_and_then_wait(azimuth_limits[1] - 1, elevation_limits[1] - 1)
