# mypy: disable-error-code="union-attr"
"""Tests ds manager component manager commands using invalid input."""

from unittest.mock import Mock, patch

import pytest
from ska_control_model import ResultCode
from tango.test_context import DeviceTestContext

from ska_mid_dish_ds_manager.DSManager import DSManager
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager


# pylint:disable=attribute-defined-outside-init
@pytest.mark.unit
@pytest.mark.forked
class TestSetPowerMode:
    """Contains tests for SetPowerMode."""

    def setup_method(self) -> None:
        """Set up context."""
        with patch(
            "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
        ) as patched_opcua_control:
            patched_opcua_control.return_value = Mock()

            self.tango_context = DeviceTestContext(DSManager)
            self.tango_context.start()
            self.device_proxy = self.tango_context.device

            class_instance = DSManager.instances.get(self.device_proxy.name())
            self.opcua_device_cm: OPCUAComponentManager = class_instance.component_manager
            self.scu_mock = Mock()
            self.scu_mock.commands = {"Management.Commands.SetPowerMode": Mock()}
            self.opcua_device_cm.command_manager.scu = self.scu_mock

    def teardown_method(self) -> None:
        """Tear down context."""
        self.tango_context.stop()

    def test_set_power_mode_argument_length(self) -> None:
        """
        Verify behaviour of set_power_mode command using invalid input.

        :param component_manager: the component manager under test
        :param callbacks: a dictionary of mocks, passed as callbacks to
            the command tracker under test
        """
        [[result_code], [message]] = self.device_proxy.SetPowerMode([3.0, 5.5, 2.0])
        assert result_code == ResultCode.REJECTED
        assert (
            message
            == "Expected 2 arguments [(0.0 or 1.0) and limit(kW)] with 1.0 representing True "
            "and 0.0 representing False. Got 3 arguments."
        )

    def test_set_power_mode_with_invalid_boolean_number(self) -> None:
        """
        Verify behaviour of set_power_mode command using invalid input.

        :param component_manager: the component manager under test
        :param callbacks: a dictionary of mocks, passed as callbacks to
            the command tracker under test
        """
        [[result_code], [message]] = self.device_proxy.SetPowerMode([7.7, 15.5])
        assert result_code == ResultCode.REJECTED
        assert message == "Invalid boolean number: 7.7. Expected 0.0 or 1.0."
