# mypy: disable-error-code="union-attr"
"""Tests track start ."""

import logging
from unittest.mock import Mock, patch

import pytest
from ska_control_model import TaskStatus
from tango.test_context import DeviceTestContext

from ska_mid_dish_ds_manager.DSManager import DSManager
from ska_mid_dish_ds_manager.models.dish_enums import DscCmdAuthType, TrackInterpolationMode
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager
from ska_mid_dish_ds_manager.utils import get_current_tai_timestamp

LOGGER = logging.getLogger(__name__)


# pylint:disable=attribute-defined-outside-init, protected-access
@pytest.mark.unit
@pytest.mark.forked
class TestTrack:
    """Contains tests of the OPCUADeviceComponentManager."""

    def setup_method(self) -> None:
        """Set up context."""
        with patch(
            "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
        ) as patched_opcua_control:
            patched_opcua_control.return_value = Mock()

            self.tango_context = DeviceTestContext(DSManager)
            self.tango_context.start()
            self.device_proxy = self.tango_context.device

            class_instance = DSManager.instances.get(self.device_proxy.name())
            self.opcua_device_cm: OPCUAComponentManager = class_instance.component_manager
            self.scu_mock = Mock()
            mock_command = Mock()
            mock_command.return_value = [Mock(), Mock()]
            self.scu_mock.commands = {"Tracking.Commands.TrackStart": mock_command}
            mock_attribute = Mock()
            mock_attribute.value = DscCmdAuthType.LMC
            self.scu_mock.attributes = {"CommandArbiter.Status.DscCmdAuthority": mock_attribute}
            self.opcua_device_cm.command_manager.scu = self.scu_mock

    def teardown_method(self) -> None:
        """Tear down context."""
        self.tango_context.stop()

    @patch("ska_mid_dish_ds_manager.command_manager.ua")
    def test_track_opcua_method_call(self, mock_ua: Mock) -> None:
        """Verify that the Track method calls TrackStart with the correct parameters."""
        mocked_cb = Mock()

        # Mock the UInt16 casting method to return the same value so we don't receive Mocks
        mock_ua.UInt16.side_effect = lambda x: x
        mock_ua.UaError = Exception

        tai_time_now = get_current_tai_timestamp()
        self.opcua_device_cm.command_manager._track(TrackInterpolationMode.SPLINE, mocked_cb)

        assert self.scu_mock.commands["Tracking.Commands.TrackStart"].called
        call_args = self.scu_mock.commands["Tracking.Commands.TrackStart"].call_args[0]
        assert isinstance(call_args[1], float)

        time_difference = call_args[1] - tai_time_now
        assert time_difference > 1

    @patch("ska_mid_dish_ds_manager.command_manager.CommandManager.track")
    def test_interpolation_mode(self, patched_cm_track_method: Mock) -> None:
        """Test interpolation mode."""
        patched_cm_track_method.return_value = (TaskStatus.COMPLETED, "Done")
        self.device_proxy.Track()
        assert patched_cm_track_method.called
        assert patched_cm_track_method.call_args[0][0] == TrackInterpolationMode.SPLINE
