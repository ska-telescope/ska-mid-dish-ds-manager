"""Contains pytest fixtures for unit test setups."""

# pylint: disable=redefined-outer-name

import logging
from typing import Any
from unittest.mock import Mock, patch

import pytest

from ska_mid_dish_ds_manager.command_manager import CommandManager
from ska_mid_dish_ds_manager.opcua_component_manager import OPCUAComponentManager

LOGGER = logging.getLogger(__name__)


@pytest.fixture()
def callbacks() -> dict:
    """Return a dictionary of callbacks."""
    return {
        "comm_state_cb": Mock(),
        "comp_state_cb": Mock(),
    }


@pytest.fixture
def ds_component_manager(callbacks: dict) -> Any:
    """Return ds opcua component manager."""
    with patch(
        "ska_mid_dish_ds_manager.opcua_component_manager.OPCUAControl"
    ) as patched_opcua_control:
        patched_opcua_control.return_value = Mock()
        opcua_cm = OPCUAComponentManager(
            "opcua/server/address",
            4840,
            "/some/endpoint",
            "http://skao.int/DS_ICD/",
            Mock(),
            communication_state_callback=callbacks["comm_state_cb"],
            component_state_callback=callbacks["comp_state_cb"],
        )
        opcua_cm.command_manager = CommandManager(Mock(), scu=Mock(), submit_task=Mock())

        yield opcua_cm
